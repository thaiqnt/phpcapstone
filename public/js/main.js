/*
 *
 * Copyright 2018 Thai Tran Ngoc Quoc
 * Load some preconfiguration items
 */

// Update a count in a cart
function cartcount(){
    $.ajax({
        url: 'action.php',
        method: 'POST',
        data: {cartcount:1},
        success: function(data){
            if (data.match(/^[0-9]+$/)) {
              $('.checkoutitems').html(data);
            } else {
              $('.checkoutitems').html('0');
            }
        }
    })
}

function searchtour(searchtext) {
    text = $(searchtext).val();
    $.ajax({
        url: "action.php",
        method: "POST",
        data: {getTour:1, keyword:text},
        success: function(data){
            $('#getTour').html(data);
        }
    })
}

function searchcruise(searchtext) {
    text = $(searchtext).val();
    $.ajax({
        url: "action.php",
        method: "POST",
        data: {getCruise:1, keyword:text},
        success: function(data){
            $('#getCruise').html(data);
        }
    })
}

/*

var blacklistkeyword    = [ '“', '”', '’', '"', '–', '"', '–', '-', 'xor', 'XOR'];
var blacklistreplacement = [ '',  '',  '',  '',  '',  '', ' ', '/',   ' ', ' '];

function removeBlackListKeywords(searchitem) {
  for (var i=0; i<blacklistkeyword.length; i++) {
    searchitem.value = searchitem.value.replace(new RegExp(blacklistkeyword[i], 'g'), blacklistreplacement[i]);
  }
}

function removeInputBlackListKeywords() {
  var elements = document.getElementsByTagName("input");
  for (var i = 0; i < elements.length; i++) {
    removeBlackListKeywords(elements[i]);
    console.log(elements[i].value);
  }
  elements = document.getElementsByTagName("textarea");
  for (var i = 0; i < elements.length; i++) {
    removeBlackListKeywords(elements[i]);
    console.log(elements[i].value);
  }
}

var isOnSalsaHosting = true;

function checkMaxLengthInSalsaHost(elementname) {
  element = document.getElementById(elementname);
  if (isOnSalsaHosting && element.value.length > 368) {
    alert(elementname+' field length is over 368 characters and will cause a bug on Salsa server hosting!!!');
  }
}
*/


$(document).ready(function() {
  cartcount();
});

