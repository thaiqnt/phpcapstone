<?php
/**
 * Action Page
 *
 * @author      Thai Tran Ngoc Quoc
 * @copyright   Copyright (c) 2018 - All rights reserved
 */

// ------------------------------------------------------------------------

// Retrieve the configuration
require('../includes/config.php');

if (!isset($_SESSION[USERINFO])) {
  die('The user has logged out.');
}

// Get csrf key from session
$csrfkey = $_SESSION['csrf'];

require(VIETNAMTOUR_CONFIG_PATH.'helper.php');
require(MODEL_CONFIG_PATH.'database.php');
require(MODEL_CONFIG_PATH.'tour.db.php');
require(MODEL_CONFIG_PATH.'invoice.db.php');

// Loading a validator class
use classes\utility\validator;

// ------------------------------------------------------------------------

/**
 * Modify the difference of a pax in Cart and in database
 *
 * @return  int: the desired pax number, ussually zero
 */
function adjustIncorrectPax($availpax) {
  // This situation ussually happens when a user had added a tour but had not checkout and another user checked out the tour
  if ($availpax < 0) {
    // This available pax is not correct, just set it to zero in order not let anyone add it to cart
    // Tell user that someone took the tour
    return 0;
  }
  
  return $availpax;
}

// getTour function
if (isset($_POST['getTour'])){
  // Get all valid tours information
  if (isset($_POST['keyword'])){
    $tours = retrieveTours(1, getSanitizedValue('keyword'));
  } else {
    $tours = retrieveTours(1);
  }
  $tourcontent = '';
  
  if (count($tours)>0) {
    $tourcontent .= <<<EOT
      <!-- the treasure section is beginning with this vt-most-treasure -->
      <div class="vt-most-treasure">
        <ul class="vt-most-treasure">
EOT;
    foreach ($tours as $tour) {
    $tourcontent .= <<<EOT
          <li class="vt-most-treasure">
            <a href="#" class="vt-treasure-inner">
              <article class="vt-treasure-inner">
                <h1 class="vt-treasure-inner title doModal" data-id="{$tour['id']}">{$tour['title']} / {$tour['duration']} / \${$tour['price']}</h1>
                <div style="background-image:url(images/{$tour['image']})" class="vt-treasure-inner img doModal" data-id="{$tour['id']}">
                </div>
                <p class="vt-treasure-inner excerpt doModal" data-id="{$tour['id']}">
                {$tour['desc']}
                </p>
              </article>
            </a>
          </li>
EOT;
    }
    $tourcontent .= <<<EOT
        </ul>
      </div>
EOT;
  } else {
    $tourcontent = '<div class="alert alert-danger"><strong>Error&nbsp;:&nbsp;</strong>No tour has been found.</div>';
  }
  
  echo $tourcontent;
}

// ------------------------------------------------------------------------

// tourDetail function
if (isset($_POST['tourDetail']) && isset($_POST['tourid'])) {
  $tourid = $_POST['tourid'];
  // Is valid id?
  if (isnumeric($tourid)) {
    // Get the tour info
    $tour = retrieveTourInfo($tourid);
    if (count($tour)>0) {
    $availpax = $tour['availpax'];
    
    // Deduct available pax if users already added their paxes
    if (!empty($_SESSION["cartitem"])) {
        if (in_array($tourid, array_keys($_SESSION["cartitem"]))) {
            if(empty($_SESSION["cartitem"][$tourid]["pax"])) {
                $_SESSION["cartitem"][$tourid]["pax"] = 0;
            }
            $availpax -= $_SESSION["cartitem"][$tourid]["pax"];
            $availpax = adjustIncorrectPax($availpax);
        }
    }
      
    $keywords = substr($tour['keywords'], 0, 60);
    $desc = substr($tour['desc'], 0, 410);
    
    $tourcontent = <<<EOT
            <div class='row'>
                <div class='col-md-6 pull-right'>
                    <img src='images/{$tour['image']}' alt='{$tour['title']}' style='width:300px;height:200px;'>
                    <div class='row'> <div class='col-md-12'><strong>Description:</strong><h5 class='text-muted'>$desc</h5></div></div>
                    <div class='row'> <div class='col-md-12'><strong>Keywords:</strong><h4 class='text-muted'>$keywords</h4></div></div>
                </div>
                <div class='col-md-6'>
                    <div class='row'> <div class='col-md-12'><h2>{$tour['title']}</h2></div></div>
                    <div class='row'> <div class='col-md-12'><strong>Duration:</strong><h3 class='text-primary'>{$tour['duration']}</h3></div></div>
                    <div class='row'> <div class='col-md-12'><strong>Start Date:</strong><h3 class='text-success'>{$tour['startdat']}</h3></div></div>
                    <div class='row'> <div class='col-md-12'><strong>Price:</strong><h3 class='text-danger'>\${$tour['price']}</h3></div></div>
                    <div class='row'> 
                        <div class='col-md-3'><strong>Pax:</strong></div>
                        <div class='col-md-3'><input class='form-control' type='text' id='pax' value='1'></input></div>
                        <div class='col-md-3'><strong>Avail Pax:</strong></div>
                        <div class='col-md-3'><input disabled class='form-control' type='text' id='availpax' value='$availpax'></input></div>
                    </div>
                    <br/>
                    <button data-id='{$tour['id']}' class='addCart btn btn-danger'>Add to Cart</button>
                </div>
            </div>
EOT;
    echo $tourcontent;
    }
    
  }

}
// ------------------------------------------------------------------------

// getAvailPax function
if (isset($_POST['getAvailPax']) && isset($_POST['tourid'])) {
  $tourid = $_POST['tourid'];
  // Get the tour info
  $tour = retrieveTourInfo($tourid);    

  if (count($tour)>0) {
    $availpax = $tour['availpax'];
    // Is there any tour pax in cart?
    if (isset($_SESSION["cartitem"][$tourid]["pax"])) {
      // the available pax after deduction
      $availpax -= $_SESSION["cartitem"][$tourid]["pax"];
      $availpax = adjustIncorrectPax($availpax);
    }
    // Here is the available pax in a tour
    echo $availpax;
  }
}


// ------------------------------------------------------------------------

// addCart function
if (isset($_POST['addCart']) && isset($_POST['tourid']) && isset($_POST['pax'])) {
  $tourid = $_POST['tourid'];
  $pax = $_POST['pax'];
  // Is valid id?
  if (isnumeric($tourid) && isnumeric($pax)) {
    $pax = intval($pax);
    
    if ($pax > 0) {
      // Get the tour info
      $tour = retrieveTourInfo($tourid);    
    
      if (count($tour)>0) {
        // Reach PAX limit?
        $availpax = $tour['availpax'];
        if ($pax > $availpax) {
          echo "Error: The ordered PAX ($pax) &gt; the maximum ($availpax)";
        } else {
          // Setup a brand new cart item
          $cartItems = array('title' => $tour['title'],
              'startdat' => $tour['startdat'],
              'price' => $tour['price'],
              'duration' => $tour['duration'],
              // This field is used when user goes to checkout
              'image' => $tour['image'],
              'pax' => $pax);
          if (empty($_SESSION["cartitem"])) {
              $_SESSION["cartitem"][$tourid] = $cartItems;
              echo "OK! Your Cart has been created and filled with FIRST $pax PAX(es) of tour {$tour['title']}";
          } else {

            if (in_array($tourid, array_keys($_SESSION["cartitem"]))) {
                if (empty($_SESSION["cartitem"][$tourid]["pax"])) {
                    $_SESSION["cartitem"][$tourid]["pax"] = 0;
                }
                $total = $_SESSION["cartitem"][$tourid]["pax"] + $pax;

                // Reach Cart limit?
                if ($total > $availpax) {
                  echo "Error: Ordered PAX total (NEW $pax + LAST {$_SESSION["cartitem"][$tourid]["pax"]}) &gt; the maximum ($availpax)";
                } else {
                  // Update as normally
                  $_SESSION["cartitem"][$tourid]["pax"] += $pax;
                  echo "OK! Your Cart has been received $pax PAX(es) of tour {$tour['title']}";
                }

            } else {

                // This is the first time when this tour is added
                $_SESSION["cartitem"][$tourid] = $cartItems;
                echo "OK! Your Cart has been added FIRST $pax PAX(es) of tour {$tour['title']}";
            }

            // Checkpoint should be here.
          }

        }

      }    
    } else {
      echo "Error: The ordered PAX should NOT be ZERO";
    }

  }
}

// ------------------------------------------------------------------------

// cartcount function
if (isset($_POST['cartcount'])){
  // There is no Cart
  if (empty($_SESSION["cartitem"])) {
    echo 0;
  } else {
    $orderedpax = 0;
    
    foreach ($_SESSION["cartitem"] as $row) {
      if (isset($row['pax'])) {
        $orderedpax += $row['pax'];
      }
    }
    
    echo $orderedpax;
  }
}

// ------------------------------------------------------------------------

// cartcheckout function
if (isset($_POST['cartcheckout'])){
  // There is no Cart
  if (empty($_SESSION["cartitem"])) {
    echo 'There is nothing in your Cart. Please go to the Tour page and keep shopping.';
  } else {
    $tourcontent = '';
    $amounttotal = 0;
    
    foreach ($_SESSION["cartitem"] as $key => $row) {
      $tourid = $key;
      $title = $row['title'];
      $price = $row['price'];
      $total = $price * $row['pax'];
      $amounttotal += $total;
      
      $tourcontent .= <<<EOT
        <div class='row'>
          <div class='col-md-2'><img src='images/{$row['image']}' alt='$title' width='60' height='40'></div>
          <div class='col-md-2 text-info'>$title</div>
          <div class='col-md-2'><input class='form-control price' type='text' size='10' data-id='$tourid' id='price-$tourid' value='$price' disabled></div>
          <div class='col-md-2'><input class='form-control qty' type='text' size='10' data-id='$tourid' id='qty-$tourid' value='{$row['pax']}' disabled></div>
          <div class='col-md-2'><input class='total form-control price' type='text' size='10' data-id='$tourid' id='amt-$tourid' value='$total' disabled></div>
          <div class='col-md-2'><a href='#' data-id='$tourid' class='btn btn-danger remove'><span class='glyphicon glyphicon-trash'></span></a></div>
        </div>
EOT;
    }
    
    $tourcontent .= <<<EOT
        <div class='row'>
          <div class='col-md-8'></div>
          <div class='col-md-4'>
              <b>Sub Total: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$<span id='amounttotal'>$amounttotal</span></b>
          </div>
        </div>
EOT;
    
    echo $tourcontent;
  }

}

// ------------------------------------------------------------------------

// removefromcart function
if (isset($_POST['removefromcart']) && isset($_POST['tourid'])) {
  $tourid = $_POST['tourid'];
  // Everything is OK
  if (!empty($_SESSION["cartitem"]) && isnumeric($tourid)) { 
    $tourid = intval($tourid);
    echo "Tour {$_SESSION["cartitem"][$tourid]['title']} has been removed.";
    unset($_SESSION["cartitem"][$tourid]);
  }
}

// ------------------------------------------------------------------------

function doPaymentCheckOut() {
  // Supply the user payment information
  $paymentinfo = array('billto' => getSanitizedValue('billto'),
    'address' => getSanitizedValue('address'),
    'duedate' => getSanitizedValue('duedate'),
    'phone' => getSanitizedValue('phone'),
    'fax' => getSanitizedValue('fax'),
    'email' => getSanitizedValue('email'),
    'tax' => intval($_POST['tax']),
    'subtotal' => intval($_POST['subtotal']),
    'depositeamt' => intval($_POST['depositeamt'])
    );
  
  $tourcontent = '';
  
  $tourstakenout = orderTour($_SESSION[USERINFO]['id'], $_SESSION["cartitem"], $paymentinfo);
  // Is there any error?
  if (count($tourstakenout)>0) {
    // Is this an error message?
    if (isset($tourstakenout['error'])) {
      // Print it out
      $tourcontent .=  $tourstakenout['error'];
      return $tourcontent;
      
    } else {
      // Populate the booked tour ids
      $tourcontent = '';
      $amounttotal = 0;
      
      // Convert booked ids into a key array
      foreach ($tourstakenout as $row) {
        $bookedtourids[] = $row['id']; 
      }
      
      foreach ($_SESSION["cartitem"] as $key => $row) {
        $tourid = $key;
        $title = $row['title'];
        $price = $row['price'];
        $total = $price * $row['pax'];
        $amounttotal += $total;
        
        // Is this a booked tour?
        if (in_array($key, $bookedtourids)) {
          $tourcontent .= <<<EOT
            <div class='row'>
              <div class='col-md-2'><img src='images/vt_notavailable.jpg' alt='$title' width='60' height='40'></div>
              <div class='col-md-2 text-danger'><strike>$title</strike></div>
              <div class='col-md-2'><strike>$price</strike></div>
              <div class='col-md-2'><strike>{$row['pax']}</strike></div>
              <div class='col-md-2'><strike>$total</strike></div>
              <div class='col-md-2'><a href='#' data-id='$tourid' class='btn btn-danger remove'><span class='glyphicon glyphicon-trash'></span></a></div>
            </div>
EOT;
        } else {
          $tourcontent .= <<<EOT
            <div class='row'>
              <div class='col-md-2'><img src='images/{$row['image']}' alt='$title' width='60' height='40'></div>
              <div class='col-md-2 text-info'>$title</div>
              <div class='col-md-2'><input class='form-control price' type='text' size='10' data-id='$tourid' id='price-$tourid' value='$price' disabled></div>
              <div class='col-md-2'><input class='form-control qty' type='text' size='10' dataid='$tourid' id='qty-$tourid' value='{$row['pax']}' disabled></div>
              <div class='col-md-2'><input class='total form-control price' type='text' size='10' dataid='$tourid' id='amt-$tourid' value='$total' disabled></div>
              <div class='col-md-2'><a href='#' data-id='$tourid' class='btn btn-danger remove'><span class='glyphicon glyphicon-trash'></span></a></div>
            </div>
EOT;
        }
      }

      $tourcontent .= <<<EOT
          <div class='row'>
            <div class='col-md-8'></div>
            <div class='col-md-4'>
                <b>Total: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$$amounttotal</b>
            </div>
          </div>
EOT;

      return $tourcontent;
    }
  } else {
    // Everything is OK
    unset($_SESSION["cartitem"]);
    // Say OK
    return 'OK';
  }  
  
  return $tourcontent;
}


// paymentcheckout function
if (isset($_POST['paymentcheckout']) && isset($_POST['csrf'])) {
  // CSRF Check
  if ($csrfkey == $_POST['csrf']) {
    $validator = new validator();

    $validator->validation_bind('billto', '', 'compulsory/minlength[2]/maxlength[255]');
    $validator->validation_bind('address', '', 'compulsory/minlength[2]/maxlength[255]');
    $validator->validation_bind('duedate', '', 'compulsory/minlength[10]/maxlength[25]');
    $validator->validation_bind('phone', '', 'compulsory/phone');
    $validator->validation_bind('fax', '', 'compulsory/phone');
    $validator->validation_bind('email', '', 'compulsory/minlength[7]/maxlength[255]');
    $validator->validation_bind('tax', '', 'compulsory/minlength[1]/maxlength[11]/numeric');
    $validator->validation_bind('subtotal', '', 'compulsory/minlength[1]/maxlength[100]/numeric');
    $validator->validation_bind('depositeamt', '', 'compulsory/minlength[1]/maxlength[100]/numeric');

    if ($ok = $validator->isValidated()) {
       $tourcontent = doPaymentCheckOut();
    } else {
       $tourcontent = $validator->buildErrorMessages();
    }

    echo $tourcontent;
  }

}

// ------------------------------------------------------------------------

// getCruise function
if (isset($_POST['getCruise'])){
  // Get all valid tours information
  if (isset($_POST['keyword'])){
    $tours = retrieveTours(2, getSanitizedValue('keyword'));
  } else {
    $tours = retrieveTours(2);
  }
  
  $tourcontent = '';
  
  if (count($tours)>0) {
    $tourcontent .= <<<EOT
      <table id="seacruiseinvietnam">
        <caption>
          Vietnam Tour Packages for Sea Cruise Trips
        </caption>
        <tr>
          <td>
            <div id="seacruisegallery" class="animated fadeInLeft">
EOT;
    foreach ($tours as $tour) {
    $tourcontent .= <<<EOT
              <div class="doModal" data-id="{$tour['id']}">
                <img src="images/{$tour['image']}" alt="{$tour['title']}" width="298" height="199" />
              </div>
EOT;
    }
    $tourcontent .= <<<EOT
            </div>
          </td>
        </tr>
      </table>
EOT;
  } else {
    $tourcontent = '<div class="alert alert-danger"><strong>Error&nbsp;:&nbsp;</strong>No tour has been found.</div>';
  }
  
  echo $tourcontent;
}

// ------------------------------------------------------------------------

// getOrderList function
if (isset($_POST['getOrderList']) && isset($_POST['invid'])) {
  $invid = $_POST['invid'];
  if (isnumeric($invid)) {
    // Get all orders belonged to the user
    $orders = retrieveOrdersByInvoice($invid);

    $orderlist = '';

    if (count($orders)>0) {
      foreach ($orders as $order) {

        $orderlist .= <<<EOT
                          <div class="row">
                              <div class="col-md-2">{$order['ctitle']}</div>
                              <div class="col-md-2">{$order['title']}</div>
                              <div class="col-md-2">{$order['startdat']}</div>
                              <div class="col-md-2">{$order['duration']}</div>
                              <div class="col-md-2">{$order['price']}</div>
                              <div class="col-md-2">{$order['pax']}</div>
                          </div>
                          <br/>
EOT;
      }
    } else {
      $orderlist = '<strong>Error&nbsp;:&nbsp;</strong>No invoice has been found.';
    }

    echo $orderlist;
  }

}

// ------------------------------------------------------------------------

// getInvoiceList function
if (isset($_POST['getInvoiceList'])) {
  // Get all invoices belonged to the user
  $invoices = retrieveInvoicesByCustomer($_SESSION[USERINFO]['id']);

  // List out all tour categories
  $invlist = '';

  if (count($invoices)>0) {
    $invdate = '<select id="invdateid" name="invdateid" class="form-control" onchange="fillOrders(this.options[this.selectedIndex].value);
      updateInvoice(this.options[this.selectedIndex]);">';
    
    // Print all of the create date list
    foreach ($invoices as $inv) {
      $invdate .= "<option value=\"{$inv['id']}\" 
        data-subtotal=\"{$inv['subtotal']}\" 
        data-duedate=\"{$inv['duedate']}\" 
        data-depositeamt=\"{$inv['depositeamt']}\" 
        data-tax=\"{$inv['tax']}\" 
        data-billto=\"{$inv['billto']}\" 
        data-address=\"{$inv['address']}\" 
        data-phone=\"{$inv['phone']}\" 
      >{$inv['createdat']}</option>";
    }
    
    $invdate .= '</select>';
    
    $invlist .= <<<EOT
                          <div class="row">
                              <div class="col-md-1"></div>
                              <div class="col-md-2">$invdate</div>
                              <div class="col-md-1" id="total"></div>
                              <div class="col-md-2" id="duedate"></div>
                              <div class="col-md-1"><span id="depositeamt"></span> / <span id="tax"></span> </div>
                              <div class="col-md-1" id="billto"></div>
                              <div class="col-md-1" id="phone"></div>
                              <div class="col-md-2"><textarea disabled id="address" rows="3" class='form-control' /></div>
                              <div class="col-md-1"></div>
                          </div>
                          <br/>
EOT;
    
  } else {
    $invlist = '<strong>Error&nbsp;:&nbsp;</strong>No invoice has been found.';
  }
  
  echo $invlist;

}
?>