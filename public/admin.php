<?php
/**
 * Admin Page where all tasks will be proceeded for backend
 *
 * @author      Thai Tran Ngoc Quoc
 * @copyright   Copyright (c) 2018 - All rights reserved
 */

// ------------------------------------------------------------------------

// Retrieve the configuration
require('../includes/config.php');

if (!isset($_SESSION[USERINFO])) {
  die('The user has logged out.');
}

// Make sure all admininstration functions are for admin user
if ($_SESSION[USERINFO]['id'] != 1) {
  die('This function is NOT available for customer users');
}

require(VIETNAMTOUR_CONFIG_PATH.'helper.php');
require(MODEL_CONFIG_PATH.'database.php');
require(MODEL_CONFIG_PATH.'tour.db.php');

// ------------------------------------------------------------------------


/**
 * getCategory function implementation
 *
 * @return  none
 */
function doGetCategories() {
  // getCategory function
  if (isset($_POST['getCategory'])) {
    // Get all tour categories
    $categories = retrieveCategories();
    // List out all tour categories
    $categorylist = '';

    if (count($categories)>0) {
      $categorylist .= '<select id="categoryid" name="categoryid" class="form-control" onchange="fillTours(this.options[this.selectedIndex].value);">';
      foreach ($categories as $cat) {
        $categorylist .= "<option value=\"{$cat['id']}\">{$cat['title']}</option>";
      }
      $categorylist .= '</select>';
    }

    echo $categorylist;

  }
}
// ------------------------------------------------------------------------

/**
 * getTourList function implementation
 *
 * @return  none
 */
function doGetTourList($isDirectCall = true) {
  // Pass by this code if there is an internal call
  if (($isDirectCall) && !isset($_POST['getTourList'])) return;
  
  if (isset($_POST['categoryid'])) {
    if (isnumeric($_POST['categoryid'])) {
      $categoryid = $_POST['categoryid'];

      $keyword = '';
      if (isset($_POST['keyword'])) {
        $keyword = getSanitizedValue('keyword', FILTER_SANITIZE_STRING);

      }

      // Get all tours including deleted tours
      $tours = retrieveTours($categoryid, $keyword, false);

      $tourlist = '';

      if (count($tours)>0) {
        foreach ($tours as $tour) {
          $deletionbutton = "<a href='#' class='btn btn-danger remove' onclick='removeTour({$tour['id']}, \"{$tour['title']}\");'><span class='glyphicon glyphicon-trash'></span></a>";
          // If a tour is deleted, a button of deletion should not visible on the list
          if ($tour['deleted']) {
            $deletionbutton = '';
          }

          $tourlist .= <<<EOT
                            <div class="row">
                                <div class="col-md-1"></div>
                                <div class="col-md-1"><img src='images/{$tour['image']}' alt='{$tour['title']}' class='tourimg'></div>
                                <div class="col-md-2"><input disabled class='form-control' type='text' value='{$tour['title']}'/></div>
                                <div class="col-md-2"><input disabled class='form-control' type='text' value='{$tour['startdat']}'/></div>
                                <div class="col-md-1"><input disabled class='form-control' type='text' value='{$tour['duration']}'/></div>
                                <div class="col-md-1"><input disabled class='form-control' type='text' value='{$tour['price']}'/></div>
                                <div class="col-md-1"><input disabled class='form-control' type='text' value='{$tour['maxpax']}'/></div>
                                <div class="col-md-1"><input disabled class='form-control' type='text' value='{$tour['availpax']}'/></div>
                                <div class='col-md-2'><a href="?p=tourdetail.php&id={$tour['id']}" class="btn btn-success"><span class="glyphicon glyphicon-edit"></span></a>&nbsp;$deletionbutton</div>
                            </div>
                            <br/>
EOT;
        }
      } else {
        $tourlist = '<strong>Error&nbsp;:&nbsp;</strong>No tour has been found.';
      }

      echo $tourlist;
    }

  }
}
// ------------------------------------------------------------------------

/**
 * removeTour function implementation
 *
 * @return  none
 */
function doRemoveTour() {
  // removeTour function
  if (isset($_POST['removeTour']) && isset($_POST['tourid'])) {
    if (isnumeric($_POST['tourid'])) {
      $tourid = $_POST['tourid'];

      // OK, go for it
      activateTour($tourid, false);

      // Print out the tour list after removing a tour from database
      doGetTourList(false);

    } else {
      $tourlist = '<strong>Error&nbsp;:&nbsp;</strong>No tour has been found.';
    }

    
  }
}
// ------------------------------------------------------------------------

// What function would you like to call?
doGetCategories();
doGetTourList();
doRemoveTour();

// ------------------------------------------------------------------------

?>