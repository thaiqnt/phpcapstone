<?php
/**
 * Home page
 *
 * @author      Thai Tran Ngoc Quoc
 * @copyright   Copyright (c) 2018 - All rights reserved
 */

// ------------------------------------------------------------------------

// Retrieve the configuration
require('../includes/config.php');

/**
 * Redirect to a login page when user goes to a invalid place
 *
 * @return  none
 */
function rejectLink() {
  // There is no valid link
  header("Location: index.php?p=login.php");
  exit();      
}


/**
 * Generate a randomized hash number
 *
 * @return  none
 */
function getRandomHashNumber() {
  // See http://www.php.net/mt_rand for why I choose this function to get a better randomized number
  return md5($_SERVER['REMOTE_ADDR'] . uniqid(mt_rand(), true));
}

/**
 * Check suspicious CSRF and regenerate a new sercure csrfkey if a submission takes place
 *
 * @return  boolean: it is suspicious CSRF or not
 */
function isSuspiciousCSRF() {
  global $csrfkey;
  
  // Only check in the post values
  if ($_SERVER['REQUEST_METHOD'] == 'POST') {

      if (!isset($_POST['csrf']) || $_SESSION['csrf'] !== $_POST['csrf']) {
          // Yes, it is suspicious CSRF
          return true;
      }

    //Always generate a secret key when a submission occurs
    $csrfkey = getRandomHashNumber();
    $_SESSION['csrf'] = $csrfkey;

  }
  
  // Not a suspicious CSRF
  return false;
}

/**
 * Check user authorization
 *
 * @return  none
 */
function checkLogin() {
  global $tourlink;
  global $directLinks;
  global $validLinks;
  global $adminLinks;
  global $currentpage;
  
  $isLoggedin = isset($_SESSION[USERINFO]);

  if ($isLoggedin) {
    // If user is logged in, login and user registration should be invisible
    $tourlink['usersignin'] = '';
    $tourlink['userreg'] = '';
    
    // First registered account is always admin user
    if ($_SESSION[USERINFO]['id'] == 1) {
      global $isAdminUser;
      $isAdminUser = true;
      
      // Admin user has right to access the admin pages
      foreach ($adminLinks as $link) {
        $validLinks[] = $link;
      }
      
      
    }
    
  } else {
    // If user is not logged in, logout, cart and profile are invisible
    $tourlink['usercart'] = '';
    $tourlink['userinvoice'] = '';
    $tourlink['usersignout'] = '';
    $tourlink['userprofile'] = '';
  }
  
  $isSuspiciousCSRF = isSuspiciousCSRF();
  
  // Check direct pages such as login, registration
  if (in_array($currentpage, $directLinks)) {
    
    if ($isSuspiciousCSRF) {
        die('Technical issue: CSRF token is mismatched in primary form (re)submission.');
    }

  } else {
  // Check sensitive pages like tour, invoice
  
    if ($isLoggedin) {
      
      if (in_array($currentpage, $validLinks)) {
        // General CSRF check: all POST submission through index.php will
        // be checked by this statement

        if ($isSuspiciousCSRF) {
            die('Technical issue: CSRF token is mismatched in sensitive form (re)submission.');
        }

      } else {
        // Invalid link request
        rejectLink();
      }
    } else {
      // Not logged request
      rejectLink();
    }
      
  }
}

// Just create csrf token for the first time
if (isset($_SESSION['csrf'])) {
  // Get csrf key back from the session
  $csrfkey = $_SESSION['csrf'];
} else {
  //Generate a secret key
  $csrfkey = getRandomHashNumber();
  $_SESSION['csrf'] = $csrfkey;
}

checkLogin();

$title="Vietnam Tours, Proudly Tour Recommendation for Your Family and Friends";
// There is no CSS and specific content in the main page
$csscontent='';
$regioncontent='';

// Is the current request is available on our site? except itself
if (strpos($currentpage, 'index.php') === 0) {
  // There is no valid page ? Return the main content
  require(VIEW_CONFIG_PATH.'index.tpl.php');
  echo $content;
} else {
  require(CONTROL_CONFIG_PATH.$currentpage);
}

?>