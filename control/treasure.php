<?php
/**
 * Treasure Tour
 *
 * @author      Thai Tran Ngoc Quoc
 * @copyright   Copyright (c) 2018 - All rights reserved
 */

// ------------------------------------------------------------------------

// Setting up for the title and activating the menu link
$title="Vietnam Tours: Booking Vietnam Tours";
$tourmenu['mobiletreasureactive'] = 'active';
$tourmenu['desktoptreasureactive'] = 'nav-trigger-active';

// Get the css for the treasure page
require(VIEW_CONFIG_PATH.'treasure.css.tpl.php');
// Get the treasure page content
require(VIEW_CONFIG_PATH.'treasure.tpl.php');
// Get the main content
require(VIEW_CONFIG_PATH.'index.tpl.php');

// Display the content of the page
echo $content;
?>