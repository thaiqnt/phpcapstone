<?php
/**
 * About us page
 *
 * @author      Thai Tran Ngoc Quoc
 * @copyright   Copyright (c) 2018 - All rights reserved
 */

// ------------------------------------------------------------------------

// Setting up for the title and activating the menu link
$title="Vietnam Tours: About us";
$tourmenu['mobileaboutusactive'] = 'active';
$tourmenu['desktopaboutusactive'] = 'nav-trigger-active';

// Get the css for the about us page
require(VIEW_CONFIG_PATH.'aboutus.css.tpl.php');
// Get about us content
require(VIEW_CONFIG_PATH.'aboutus.tpl.php');
// Get the main content
require(VIEW_CONFIG_PATH.'index.tpl.php');

// Display the content of the page
echo $content;
?>