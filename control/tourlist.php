<?php
/**
 * Tour List Page
 *
 * @author		Thai Tran Ngoc Quoc
 * @copyright	Copyright (c) 2018 - All rights reserved
 */

// ------------------------------------------------------------------------

// Setting up for the title and activating the menu link
$title="Vietnam Tours: Tour List";
$tourmenu['mobileadminactive'] = 'active';
$tourmenu['desktopadminactive'] = 'nav-trigger-active';


// Get the css for the tour detail page
require(VIEW_CONFIG_PATH.'tourlist.css.tpl.php');

// Put all tours info into a hero page and ask user to enter information for update again
require(VIEW_CONFIG_PATH.'tourlist.tpl.php');

// Get the main content with customized hero as customer registration
require(VIEW_CONFIG_PATH.'home.tpl.php');

// Display the content of the page
echo $content;
?>