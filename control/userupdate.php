<?php
/**
 * Update for user
 *
 * @author		Thai Tran Ngoc Quoc
 * @copyright	Copyright (c) 2018 - All rights reserved
 */

// ------------------------------------------------------------------------

// Is user logged in?
if (isset($_SESSION[USERINFO])) {
  $userinfo = $_SESSION[USERINFO];
} else {
  die('This page requires an authenticated login');
}

// Loading a validator class
use classes\utility\validator;

$validator = new validator();

// Setting up for the title and activating the menu link
$title="Vietnam Tours: Customer Update";

// Load main modules which support for customer registration
require(VIETNAMTOUR_CONFIG_PATH.'helper.php');
require(MODEL_CONFIG_PATH.'database.php');
require(VIETNAMTOUR_CONFIG_PATH.'userupdate.cfg.php');
require(MODEL_CONFIG_PATH.'user.db.php');


/*
Requirements:

(1) a general ‘string’ validator that only allows legal characters
(2) a ‘phone’ validator
      These phone numbers are valid format
      123-4567
      123-456-7890
      (123) 456-7890
      0 123 456 7890
      1234567890    
(3) a ‘length’ validator (min and max)
(4) a Canadian postal code validator
(5) a password strength validator. Rules for password which has at least
	minimum length 8
	upper case letters
	lower case letters
	number
	special characters
*/


// We are trying to validate all fields by setting up their validation rules
// the rules are like compulsory, trim both values, min length and max length as mentioned

// Requirement (3)
$validator->validation_bind($user_update_loginname, $user_update[$user_update_loginname][$user_update_label_name], 
	'trim/compulsory/minlength[2]/maxlength[20]');
$validator->validation_bind($user_update_repeatedpass, $user_update[$user_update_repeatedpass][$user_update_label_name], 
	'compulsory');

// Requirement (5)
// in addition to password rules, the password field must match with repeated password 
$validator->validation_bind($user_update_password, $user_update[$user_update_password][$user_update_label_name], 
	"compulsory/password/minlength[8]/maxlength[50]/matches[$user_update_repeatedpass]");
$validator->validation_bind($user_update_firstname, $user_update[$user_update_firstname][$user_update_label_name], 
	'trim/compulsory/minlength[2]/maxlength[50]');
$validator->validation_bind($user_update_lastname, $user_update[$user_update_lastname][$user_update_label_name], 
	'trim/compulsory/minlength[2]/maxlength[50]');
$validator->validation_bind($user_update_street, $user_update[$user_update_street][$user_update_label_name], 
	'trim/compulsory/minlength[2]/maxlength[255]');
$validator->validation_bind($user_update_city, $user_update[$user_update_city][$user_update_label_name], 
	'trim/compulsory/minlength[2]/maxlength[50]');

// Requirement (4)  
$validator->validation_bind($user_update_postalcode, $user_update[$user_update_postalcode][$user_update_label_name], 
	'trim/compulsory/minlength[2]/maxlength[50]/canadianpostal');
$validator->validation_bind($user_update_province, $user_update[$user_update_province][$user_update_label_name], 
	'trim/compulsory/minlength[2]/maxlength[50]');
    
// Requirement (1)  
$validator->validation_bind($user_update_country, $user_update[$user_update_country][$user_update_label_name], 
	'trim/compulsory/minlength[2]/maxlength[50]/alphadashspace');

// Requirement (2)  
$validator->validation_bind($user_update_phone, $user_update[$user_update_phone][$user_update_label_name], 
	'trim/compulsory/phone');
$validator->validation_bind($user_update_email, $user_update[$user_update_email][$user_update_label_name], 
	'trim/compulsory/minlength[5]/maxlength[100]/validemail');
$validator->validation_bind($user_update_deleted, $user_update[$user_update_deleted][$user_update_label_name], 'minlength[1]/maxlength[1]/numeric');

if ($ok = $validator->isValidated()) {
    // It is time for updating customer information
    $result = updateCustomer();
    // Errors have been found here
    if (strlen($result) > 0) {
        $validator->set_other_error('system_error', $result);
        $ok = false;
    } else {
      // Change the updated time to now
      $user_update[$user_update_updatedat][$user_update_postedvalue] = retrieveDBCurrentTime();
      
      // Update user info in session where no passwords are stored 
      $_SESSION[USERINFO][$user_update_loginname] = $user_update[$user_update_loginname][$user_update_postedvalue];
      $_SESSION[USERINFO][$user_update_firstname] = $user_update[$user_update_firstname][$user_update_postedvalue];
      $_SESSION[USERINFO][$user_update_lastname] = $user_update[$user_update_lastname][$user_update_postedvalue];
      $_SESSION[USERINFO][$user_update_street] = $user_update[$user_update_street][$user_update_postedvalue];
      $_SESSION[USERINFO][$user_update_city] = $user_update[$user_update_city][$user_update_postedvalue];
      $_SESSION[USERINFO][$user_update_postalcode] = $user_update[$user_update_postalcode][$user_update_postedvalue];
      $_SESSION[USERINFO][$user_update_province] = $user_update[$user_update_province][$user_update_postedvalue];
      $_SESSION[USERINFO][$user_update_country] = $user_update[$user_update_country][$user_update_postedvalue];
      $_SESSION[USERINFO][$user_update_phone] = $user_update[$user_update_phone][$user_update_postedvalue];
      $_SESSION[USERINFO][$user_update_email] = $user_update[$user_update_email][$user_update_postedvalue];
      $_SESSION[USERINFO][$user_update_deleted] = $user_update[$user_update_deleted][$user_update_postedvalue];
      $_SESSION[USERINFO][$user_update_updatedat] = $user_update[$user_update_updatedat][$user_update_postedvalue];
      
      
    }
    
}


// if the update is complete, we show the result
$user_update[$user_update_deleted]['CHECKED'] = getPostedBoolValue(
    $user_update[$user_update_deleted][$user_update_postedvalue], 'checked', '');

// Get the list of errors and deliver it to the error section to display
$user_update_error_messages = $validator->buildErrorMessages();
// Get the css for the about userupdate page
require(VIEW_CONFIG_PATH.'userupdate.css.tpl.php');

// if the update is complete, we show the result
if ($ok) {
	require(VIEW_CONFIG_PATH.'userupdcomplete.desktop.tpl.php');
    require(VIEW_CONFIG_PATH.'userupdcomplete.mobile.tpl.php');
} else {
	// Put all customer update info into a hero page and ask user to enter information for update again
	require(VIEW_CONFIG_PATH.'userupdate.desktop.tpl.php');
	require(VIEW_CONFIG_PATH.'userupdate.mobile.tpl.php');
}
// Get the main content with customized hero as customer registration
require(VIEW_CONFIG_PATH.'home.tpl.php');

// Display the content of the page
echo $content;
?>