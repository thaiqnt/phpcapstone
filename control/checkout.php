<?php
/**
 * Checkout of Cart
 *
 * @author      Thai Tran Ngoc Quoc
 * @copyright   Copyright (c) 2018 - All rights reserved
 */

// ------------------------------------------------------------------------

// Load main modules which support for checkout 
require(VIETNAMTOUR_CONFIG_PATH.'helper.php');
require(MODEL_CONFIG_PATH.'database.php');
require(MODEL_CONFIG_PATH.'tour.db.php');

// Setting up for the title and activating the menu link
$title="Vietnam Tours: Booking Vietnam Tours";

$paymentinfo = retrievePaymentInfo($_SESSION[USERINFO]['id']);

// Get the css for the treasure page
require(VIEW_CONFIG_PATH.'checkout.css.tpl.php');
// Get the checkout page content
require(VIEW_CONFIG_PATH.'checkout.tpl.php');
// Get the main content
require(VIEW_CONFIG_PATH.'index.tpl.php');

// Display the content of the page
echo $content;
?>