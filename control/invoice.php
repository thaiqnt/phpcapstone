<?php
/**
 * Invoice for user
 *
 * @author		Thai Tran Ngoc Quoc
 * @copyright	Copyright (c) 2018 - All rights reserved
 */

// ------------------------------------------------------------------------

// Setting up for the title and activating the menu link
$title="Vietnam Tours: Invoice for Customer";


// Put all tours info into a hero page and ask user to enter information for update again
require(VIEW_CONFIG_PATH.'invoice.tpl.php');

// Get the main content with customized hero as customer registration
require(VIEW_CONFIG_PATH.'index.tpl.php');

// Display the content of the page
echo $content;
?>