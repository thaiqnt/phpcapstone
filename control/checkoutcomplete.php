<?php
/**
 * Checkout Completion of Cart
 *
 * @author      Thai Tran Ngoc Quoc
 * @copyright   Copyright (c) 2018 - All rights reserved
 */

// ------------------------------------------------------------------------

// Setting up for the title and activating the menu link
$title="Vietnam Tours: Booking Vietnam Tours";

// User name in the session
$username = $_SESSION[USERINFO]['loginname'];
// Generate randomized id
$tranid = rand();

// Get the css for the treasure page
require(VIEW_CONFIG_PATH.'checkout.css.tpl.php');
// Get the checkout page content
require(VIEW_CONFIG_PATH.'checkoutcomplete.tpl.php');
// Get the main content
require(VIEW_CONFIG_PATH.'index.tpl.php');

// Display the content of the page
echo $content;
?>