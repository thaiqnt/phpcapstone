<?php
/**
 * Profile page
 *
 * @author      Thai Tran Ngoc Quoc
 * @copyright   Copyright (c) 2018 - All rights reserved
 */

// ------------------------------------------------------------------------

// Setting up for the title and activating the menu link
$title="Vietnam Tours: Customer Profile";

// Load main modules which support for customer registration
require(VIETNAMTOUR_CONFIG_PATH.'helper.php');
require(MODEL_CONFIG_PATH.'database.php');
require(VIETNAMTOUR_CONFIG_PATH.'profile.cfg.php');

// There is no flash message at all at the beginning
$showUserInfo = '';
$flashMsgInfo[$user_profile_loginname] = '';
$flashMsgInfo[$user_profile_createdat] = '';

if (isset($_SESSION[USERINFO])) {
  $userinfo = $_SESSION[USERINFO];

  if (isset($_SESSION[USERLOGGEDIN])) {
    // Show a flash message that tells users their logins successful
    $showUserInfo = 'showWelcomeBack();';
    $flashMsgInfo[$user_profile_loginname] = $userinfo[$user_profile_loginname];
    $flashMsgInfo[$user_profile_createdat] = $userinfo[$user_profile_createdat];

    unset($_SESSION[USERLOGGEDIN]);
  }


  $user_profile[$user_profile_deleted]['CHECKED'] = getPostedBoolValue(
      $userinfo[$user_profile_deleted], 'checked', '');


  // Get the css for the about us page
  require(VIEW_CONFIG_PATH.'profile.css.tpl.php');

  require(VIEW_CONFIG_PATH.'profile.desktop.tpl.php');
  require(VIEW_CONFIG_PATH.'profile.mobile.tpl.php');


  // Get the main content with customized hero as customer registration
  require(VIEW_CONFIG_PATH.'home.tpl.php');

  // Display the content of the page
  echo $content;

}
?>