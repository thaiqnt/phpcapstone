<?php
/**
 * Login page
 *
 * @author      Thai Tran Ngoc Quoc
 * @copyright   Copyright (c) 2018 - All rights reserved
 */

// ------------------------------------------------------------------------

// Loading a validator class
use classes\utility\validator;

$validator = new validator();

$title="Vietnam Tours, Proudly Tour Recommendation for Your Family and Friends";
// There is no CSS and specific content in the main page
$csscontent='';
$regioncontent='';


// Load main modules which support for customer login
require(VIETNAMTOUR_CONFIG_PATH.'helper.php');
require(MODEL_CONFIG_PATH.'database.php');
require(VIETNAMTOUR_CONFIG_PATH.'login.cfg.php');
require(MODEL_CONFIG_PATH.'user.db.php');


// Do not show any flash message
$showUserInfo = '';
$flashMsgInfo[$user_login_loginname] = '';


$validator->validation_bind($user_login_loginname, $user_login[$user_login_loginname][$user_login_label_name], 
	'trim/compulsory/minlength[2]/maxlength[20]');
$validator->validation_bind($user_login_password, $user_login[$user_login_password][$user_login_label_name], 
	"compulsory/password/minlength[8]/maxlength[50]");

if (isset($_SESSION[USERLOGGEDOUT])) {
        // Show a flash message that tells users their logouts successful
        $showUserInfo = 'showLogout();';
        $flashMsgInfo[$user_login_loginname] = $_SESSION[USERLOGGEDOUT];
        
        // Once only
        unset($_SESSION[USERLOGGEDOUT]);

}

if ($ok = $validator->isValidated()) {
    $userinfo = retrieveLoginInfo();
    // There is no customer information?
    if (count($userinfo)===0) {
        $validator->set_other_error('system_error', 
            "Incorrect Username or Password for account {$user_login[$user_login_loginname][$user_login_postedvalue]}.");
        $ok = false;
    } else {
        // Regenerate id and redirect the page to profile as requirement
        session_regenerate_id();
        
        // Keep user info in the session
        $_SESSION[USERINFO] = $userinfo;

        // Show a flash message indicating user logged in
        $_SESSION[USERLOGGEDIN] = $userinfo[$user_login_loginname];
        
        // Display a administration statistics for admin user
        if ($userinfo['id'] == 1) {
          header("Location: index.php?p=adminstatistics.php");
        } else {
          // Show profile for regular users
          header("Location: index.php?p=profile.php");
        }
        
        exit();

    }
}

// Get the list of errors and deliver it to the error section to display
$user_login_error_messages = $validator->buildErrorMessages();


// Get the css for the about us page
require(VIEW_CONFIG_PATH.'login.css.tpl.php');

require(VIEW_CONFIG_PATH.'login.desktop.tpl.php');
require(VIEW_CONFIG_PATH.'login.mobile.tpl.php');

// Get the main content with customized hero as customer registration
require(VIEW_CONFIG_PATH.'home.tpl.php');

// Display the content of the page
echo $content;


?>