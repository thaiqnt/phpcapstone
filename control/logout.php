<?php
/**
 * Logout page
 *
 * @author      Thai Tran Ngoc Quoc
 * @copyright   Copyright (c) 2018 - All rights reserved
 */

// ------------------------------------------------------------------------

$username = '';

if (isset($_SESSION[USERINFO])) {
  $userinfo = $_SESSION[USERINFO];
  $username = $userinfo['loginname'];
}

session_unset(); //remove all session variables
session_destroy(); // destroy the session 

// Fixing the weird bug in a production host
ob_start();
// Do these things as requirement
session_regenerate_id();

session_start();

if ($username != '') {
  $_SESSION[USERLOGGEDOUT] = $username;

}

header("Location: index.php?p=login.php");
?>