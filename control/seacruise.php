<?php
/**
 * Sea Cruise Tour
 *
 * @author      Thai Tran Ngoc Quoc
 * @copyright   Copyright (c) 2018 - All rights reserved
 */

// ------------------------------------------------------------------------

// Setting up for the title and activating the menu link
$title="Vietnam Tours: Booking International Tours";
$tourmenu['mobileseacruiseactive'] = 'active';
$tourmenu['desktopseacruiseactive'] = 'nav-trigger-active';

// Get the css for seacruise page
require(VIEW_CONFIG_PATH.'seacruise.css.tpl.php');
// Get seacruise content
require(VIEW_CONFIG_PATH.'seacruise.tpl.php');
// Get the main content
require(VIEW_CONFIG_PATH.'index.tpl.php');

// Display the content of the page
echo $content;
?>