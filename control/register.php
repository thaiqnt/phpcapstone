<?php
/**
 * Registration for user
 *
 * @author		Thai Tran Ngoc Quoc
 * @copyright	Copyright (c) 2018 - All rights reserved
 */

// ------------------------------------------------------------------------

// Loading a validator class
use classes\utility\validator;

$validator = new validator();

// Setting up for the title and activating the menu link
$title="Vietnam Tours: Customer Registration";

// Load main modules which support for customer registration
require(VIETNAMTOUR_CONFIG_PATH.'helper.php');
require(MODEL_CONFIG_PATH.'database.php');
require(VIETNAMTOUR_CONFIG_PATH.'userregistration.cfg.php');
require(MODEL_CONFIG_PATH.'user.db.php');


/*
Requirements:

(1) a general ‘string’ validator that only allows legal characters
(2) a ‘phone’ validator
      These phone numbers are valid format
      123-4567
      123-456-7890
      (123) 456-7890
      0 123 456 7890
      1234567890    
(3) a ‘length’ validator (min and max)
(4) a Canadian postal code validator
(5) a password strength validator. Rules for password which has at least
	minimum length 8
	upper case letters
	lower case letters
	number
	special characters
*/


// We are trying to validate all fields by setting up their validation rules
// the rules are like compulsory, trim both values, min length and max length as mentioned

// Requirement (3)
$validator->validation_bind($user_reg_loginname, $user_reg[$user_reg_loginname][$user_reg_label_name], 
	'trim/compulsory/minlength[2]/maxlength[20]');
$validator->validation_bind($user_reg_repeatedpass, $user_reg[$user_reg_repeatedpass][$user_reg_label_name], 
	'compulsory');

// Requirement (5)
// in addition to password rules, the password field must match with repeated password 
$validator->validation_bind($user_reg_password, $user_reg[$user_reg_password][$user_reg_label_name], 
	"compulsory/password/minlength[8]/maxlength[50]/matches[$user_reg_repeatedpass]");
$validator->validation_bind($user_reg_firstname, $user_reg[$user_reg_firstname][$user_reg_label_name], 
	'trim/compulsory/minlength[2]/maxlength[50]');
$validator->validation_bind($user_reg_lastname, $user_reg[$user_reg_lastname][$user_reg_label_name], 
	'trim/compulsory/minlength[2]/maxlength[50]');
$validator->validation_bind($user_reg_street, $user_reg[$user_reg_street][$user_reg_label_name], 
	'trim/compulsory/minlength[2]/maxlength[255]');
$validator->validation_bind($user_reg_city, $user_reg[$user_reg_city][$user_reg_label_name], 
	'trim/compulsory/minlength[2]/maxlength[50]');

// Requirement (4)  
$validator->validation_bind($user_reg_postalcode, $user_reg[$user_reg_postalcode][$user_reg_label_name], 
	'trim/compulsory/minlength[2]/maxlength[50]/canadianpostal');
$validator->validation_bind($user_reg_province, $user_reg[$user_reg_province][$user_reg_label_name], 
	'trim/compulsory/minlength[2]/maxlength[50]');
    
// Requirement (1)  
$validator->validation_bind($user_reg_country, $user_reg[$user_reg_country][$user_reg_label_name], 
	'trim/compulsory/minlength[2]/maxlength[50]/alphadashspace');

// Requirement (2)  
$validator->validation_bind($user_reg_phone, $user_reg[$user_reg_phone][$user_reg_label_name], 
	'trim/compulsory/phone');
$validator->validation_bind($user_reg_email, $user_reg[$user_reg_email][$user_reg_label_name], 
	'trim/compulsory/minlength[5]/maxlength[100]/validemail');
$validator->validation_bind($user_reg_deleted, $user_reg[$user_reg_deleted][$user_reg_label_name], 'minlength[1]/maxlength[1]/numeric');

if ($ok = $validator->isValidated()) {
	// It is time for creating a new customer information
	$result = createNewCustomer();
	// Errors have been found here
	if (strlen($result) > 0) {
		$validator->set_other_error('system_error', $result);
		$ok = false;
	} else {
		// What is the newest id?
		$newestCustomerId = getLastInsertedCustomer();

		// Get the customer information from database
		$regcompleteresult = retrieveCustomer($newestCustomerId);
        // There is no customer information?
        if (count($regcompleteresult)===0) {
            $validator->set_other_error('system_error', 
                "Could not locate the customer information with id $newestCustomerId");
            $ok = false;
        }
	}
}

// if the registration is complete, we show the result
if ($ok) {
	// Determine checked flag for inactive field from the information retrieved from database
	$user_reg[$user_reg_deleted]['CHECKED'] = getPostedBoolValue(
		$regcompleteresult[$user_reg_deleted], 'checked', '');
} else {
	// Determine checked flag for inactive field from the submitted form
	$user_reg[$user_reg_deleted]['CHECKED'] = getPostedBoolValue(
		$user_reg[$user_reg_deleted][$user_reg_postedvalue], 'checked', '');
}

// Get the list of errors and deliver it to the error section to display
$user_registration_error_messages = $validator->buildErrorMessages();
// Get the css for the about us page
require(VIEW_CONFIG_PATH.'userregistration.css.tpl.php');

// if the registration is complete, we show the result
if ($ok) {
	require(VIEW_CONFIG_PATH.'userregcomplete.desktop.tpl.php');
    require(VIEW_CONFIG_PATH.'userregcomplete.mobile.tpl.php');
} else {
	// Put all customer registration into a hero page and ask user to enter information for registration
	require(VIEW_CONFIG_PATH.'userregistration.desktop.tpl.php');
	require(VIEW_CONFIG_PATH.'userregistration.mobile.tpl.php');
}
// Get the main content with customized hero as customer registration
require(VIEW_CONFIG_PATH.'home.tpl.php');

// Display the content of the page
echo $content;
?>