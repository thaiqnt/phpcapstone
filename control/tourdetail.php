<?php
/**
 * Update for tour detail
 *
 * @author		Thai Tran Ngoc Quoc
 * @copyright	Copyright (c) 2018 - All rights reserved
 */

// ------------------------------------------------------------------------

// Loading a validator class
use classes\utility\validator;

$validator = new validator();

// Setting up for the title and activating the menu link
$title="Vietnam Tours: Tour Detail";
$tourmenu['mobileadminactive'] = 'active';
$tourmenu['desktopadminactive'] = 'nav-trigger-active';

// Load main modules which support for customer registration
require(VIETNAMTOUR_CONFIG_PATH.'helper.php');
require(MODEL_CONFIG_PATH.'database.php');
require(VIETNAMTOUR_CONFIG_PATH.'tourdetail.cfg.php');
require(MODEL_CONFIG_PATH.'tour.db.php');

// When update or create a tour is OK, this flag will be true in order to
// prevent any further update.
$isActionComplete = false;

$tour_messages = '';
// Get all categories
$categories = retrieveCategories();

// List all images from folder images
$list = glob("./images/vt*.jpg");
$imagelinks = array();
foreach ($list as $l) {
  $imagelinks[] = substr($l, 9);
}

$tourimglinks = '';

$defaultimage = '';

$size = count($imagelinks);
if ($size>0) {
  // The last image will be chosen
  $defaultimage = $imagelinks[$size-1];
}

// Users like to change something in a tour?
if (count($_POST)>0) {
  $cmd = $tour[$tour_cmd][$tour_postedvalue];
  if ($cmd == 'undelete') {
    // Mark a tour as active
    $tourid = $tour[$tour_id][$tour_postedvalue];
    if (isnumeric($tourid)) {
      activateTour($tourid);
      $tour_messages = "The tour informaiton has been RECOVERED successfully.";
      
      // Update the posted value to remove Undelete button
      $tour[$tour_deleted][$tour_postedvalue] = 0;
    } else {
      $tour_messages = "Something is not CORRECT in submission.";
    }
    
  } else if ($cmd == 'update' || $cmd == 'create') {
    
    $validator->validation_bind($tour_id, '', 'compulsory/minlength[1]/numeric');
    $validator->validation_bind($tour_title, $tour[$tour_title][$tour_label_name], 'compulsory/minlength[2]/maxlength[200]');
    $validator->validation_bind($tour_categoryid, $tour[$tour_categoryid][$tour_label_name], 'compulsory/minlength[1]/numeric');
    $validator->validation_bind($tour_image, $tour[$tour_image][$tour_label_name], 'compulsory/minlength[5]');
    $validator->validation_bind($tour_price, $tour[$tour_price][$tour_label_name], 'compulsory/minlength[1]/maxlength[100]/numeric');
    $validator->validation_bind($tour_duration, $tour[$tour_duration][$tour_label_name], 'compulsory/minlength[1]/maxlength[200]');
    $validator->validation_bind($tour_keywords, $tour[$tour_keywords][$tour_label_name], 'compulsory/minlength[1]/maxlength[255]');
    $validator->validation_bind($tour_startdate, $tour[$tour_startdate][$tour_label_name], 'compulsory/minlength[1]/maxlength[50]');
    $validator->validation_bind($tour_maxpax, $tour[$tour_maxpax][$tour_label_name], 'compulsory/minlength[1]/numeric');
    $validator->validation_bind($tour_availpax, $tour[$tour_availpax][$tour_label_name], 'compulsory/minlength[1]/numeric');
    $validator->validation_bind($tour_desc, $tour[$tour_desc][$tour_label_name], 'compulsory/minlength[1]/maxlength[1000]');

    if ($ok = $validator->isValidated()) {

      $tourid = $tour[$tour_id][$tour_postedvalue];
      
      // Create a new tour?
      if ($tourid == -1) {
       
        $result = createNewTour();
        // Errors have been found here
        if (strlen($result) > 0) {
            $validator->set_other_error('system_error', $result);
            $ok = false;
        } else {
          // Update the posted id
          $tour[$tour_id][$tour_postedvalue] = getLastInsertedId();
        }      
      } else {
        // Update an existed tour
        $result = updateTour();
        // Errors have been found here
        if (strlen($result) > 0) {
            $validator->set_other_error('system_error', $result);
            $ok = false;
        }        
      }

    }

    if ($ok) {
      $tour_messages = ($tourid == -1) ? "The tour information has been CREATED successfully." : "The tour information has been UPDATED successfully.";
      
      // Yes, we've done! No further action here.
      $isActionComplete = true;
    } else {
      // Get the list of errors and deliver it to the error section to display
      $tour_messages = $validator->buildErrorMessages();
    }    
  }
  


} else {
  // Load the tour with specific id
  if (isset($_GET[$tour_id])) {
    $tourid = $_GET[$tour_id];
    
    // Users like to create a new tour?
    if ($tourid == 'new') {
      // Create a dummy data for a new tour
        $tour[$tour_id][$tour_postedvalue] = -1;
        $tour[$tour_categoryid][$tour_postedvalue] = '';
        $tour[$tour_title][$tour_postedvalue] = '';
        $tour[$tour_image][$tour_postedvalue] = $defaultimage;
        $tour[$tour_price][$tour_postedvalue] = '';
        $tour[$tour_duration][$tour_postedvalue] = '';
        $tour[$tour_keywords][$tour_postedvalue] = '';
        $tour[$tour_startdate][$tour_postedvalue] = '';
        $tour[$tour_maxpax][$tour_postedvalue] = '';
        $tour[$tour_availpax][$tour_postedvalue] = '';
        $tour[$tour_desc][$tour_postedvalue] = '';
        $tour[$tour_deleted][$tour_postedvalue] = '';

    } else if (isnumeric($tourid)) {
      // Get all tours including deleted tours
      $tourinfo = retrieveTourInfo($tourid, false);

      if (count($tourinfo) > 0) {
        $tour[$tour_id][$tour_postedvalue] = $tourinfo[$tour_id];
        $tour[$tour_categoryid][$tour_postedvalue] = $tourinfo[$tour_categoryid];
        $tour[$tour_title][$tour_postedvalue] = $tourinfo[$tour_title];
        $tour[$tour_image][$tour_postedvalue] = $tourinfo[$tour_image];
        $tour[$tour_price][$tour_postedvalue] = $tourinfo[$tour_price];
        $tour[$tour_duration][$tour_postedvalue] = $tourinfo[$tour_duration];
        $tour[$tour_keywords][$tour_postedvalue] = $tourinfo[$tour_keywords];
        $tour[$tour_startdate][$tour_postedvalue] = $tourinfo[$tour_startdate];
        $tour[$tour_maxpax][$tour_postedvalue] = $tourinfo[$tour_maxpax];
        $tour[$tour_availpax][$tour_postedvalue] = $tourinfo[$tour_availpax];
        $tour[$tour_desc][$tour_postedvalue] = $tourinfo[$tour_desc];
        $tour[$tour_deleted][$tour_postedvalue] = $tourinfo[$tour_deleted];

      } else {
        // Errors have been found here
          $tour_messages = "User ID $tourid has not been found in database.";
          $ok = false;
      }

    }
  }
}


            
// Get the css for the tour detail page
require(VIEW_CONFIG_PATH.'tourdetail.css.tpl.php');

// Put all tours info into a hero page and ask user to enter information for update again
require(VIEW_CONFIG_PATH.'tourdetail.tpl.php');

// Get the main content with customized hero as customer registration
require(VIEW_CONFIG_PATH.'home.tpl.php');

// Display the content of the page
echo $content;
?>