<?php
/**
 * About us page
 *
 * @author      Thai Tran Ngoc Quoc
 * @copyright   Copyright (c) 2018 - All rights reserved
 */

// ------------------------------------------------------------------------

// Setting up for the title and activating the menu link
$title="Vietnam Tours: Administration Statistics";
$tourmenu['mobileadminactive'] = 'active';
$tourmenu['desktopadminactive'] = 'nav-trigger-active';

require(VIETNAMTOUR_CONFIG_PATH.'helper.php');
require(MODEL_CONFIG_PATH.'database.php');
require(MODEL_CONFIG_PATH.'invoice.db.php');

// retrieve Detail Statistics info
$generalinfo = retrieveGeneralStatistics();

// retrieve General Statistics info
$detailinfo = retrieveDetailStatistics();


// Get the css for the about us page
require(VIEW_CONFIG_PATH.'adminstatistics.css.tpl.php');
// Get about us content
require(VIEW_CONFIG_PATH.'adminstatistics.tpl.php');
// Get the main content
require(VIEW_CONFIG_PATH.'index.tpl.php');

// Display the content of the page
echo $content;
?>