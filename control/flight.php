<?php
/**
 * Flight Booking page
 *
 * @author      Thai Tran Ngoc Quoc
 * @copyright   Copyright (c) 2018 - All rights reserved
 */

// ------------------------------------------------------------------------

// Setting up for the title and activating the menu link
$title="Vietnam Tours: Booking flights";
$tourmenu['mobileflightactive'] = 'active';
$tourmenu['desktopflightactive'] = 'nav-trigger-active';

// Get the css for the flight page
require(VIEW_CONFIG_PATH.'flight.css.tpl.php');
// Get the flight content
require(VIEW_CONFIG_PATH.'flight.tpl.php');
// Get the main content
require(VIEW_CONFIG_PATH.'index.tpl.php');

// Display the content of the page
echo $content;
?>