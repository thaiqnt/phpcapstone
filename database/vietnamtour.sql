-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.2.12-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for vietnamtour
CREATE DATABASE IF NOT EXISTS `vietnamtour` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `vietnamtour`;

-- Dumping structure for table vietnamtour.category
CREATE TABLE IF NOT EXISTS `category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(200) NOT NULL,
  `createdat` datetime NOT NULL DEFAULT current_timestamp(),
  `updatedat` datetime NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Dumping data for table vietnamtour.category: ~3 rows (approximately)
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
REPLACE INTO `category` (`id`, `title`, `createdat`, `updatedat`) VALUES
	(1, 'Treasures', '2018-09-09 18:18:43', '2018-09-09 18:18:43'),
	(2, 'Sea Cruises', '2018-09-09 18:18:43', '2018-09-09 18:18:43'),
	(3, 'Flight', '2018-09-09 18:18:43', '2018-09-09 18:18:43');
/*!40000 ALTER TABLE `category` ENABLE KEYS */;

-- Dumping structure for table vietnamtour.customer
CREATE TABLE IF NOT EXISTS `customer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `loginname` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `street` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `postalcode` varchar(255) NOT NULL,
  `province` varchar(255) NOT NULL,
  `country` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `createdat` datetime NOT NULL DEFAULT current_timestamp(),
  `updatedat` datetime NOT NULL DEFAULT current_timestamp(),
  `deleted` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_login` (`loginname`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table vietnamtour.customer: ~0 rows (approximately)
/*!40000 ALTER TABLE `customer` DISABLE KEYS */;
/*!40000 ALTER TABLE `customer` ENABLE KEYS */;

-- Dumping structure for table vietnamtour.customer_order
CREATE TABLE IF NOT EXISTS `customer_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tranid` int(100) NOT NULL,
  `iid` int(11) NOT NULL,
  `tid` int(11) NOT NULL,
  `pax` int(11) NOT NULL,
  `title` varchar(200) NOT NULL,
  `duration` varchar(200) NOT NULL,
  `price` int(100) NOT NULL,
  `startdat` datetime NOT NULL DEFAULT current_timestamp(),
  `createdat` datetime NOT NULL DEFAULT current_timestamp(),
  `updatedat` datetime NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `invoice_fk_idx` (`iid`),
  KEY `tour_fk_idx` (`tid`),
  CONSTRAINT `invoice1_fk` FOREIGN KEY (`iid`) REFERENCES `invoice` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `tour1_fk` FOREIGN KEY (`tid`) REFERENCES `tour` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table vietnamtour.customer_order: ~0 rows (approximately)
/*!40000 ALTER TABLE `customer_order` DISABLE KEYS */;
/*!40000 ALTER TABLE `customer_order` ENABLE KEYS */;

-- Dumping structure for table vietnamtour.invoice
CREATE TABLE IF NOT EXISTS `invoice` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cid` int(11) NOT NULL,
  `billto` varchar(255) NOT NULL,
  `address` text NOT NULL,
  `phone` varchar(255) NOT NULL,
  `fax` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `tax` float NOT NULL,
  `subtotal` int(100) NOT NULL,
  `depositeamt` int(100) NOT NULL,
  `duedate` datetime NOT NULL DEFAULT current_timestamp(),
  `createdat` datetime NOT NULL DEFAULT current_timestamp(),
  `updatedat` datetime NOT NULL DEFAULT current_timestamp(),
  `deleted` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `customer_fk` (`cid`),
  CONSTRAINT `customer_fk` FOREIGN KEY (`cid`) REFERENCES `customer` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Dumping data for table vietnamtour.invoice: ~0 rows (approximately)
/*!40000 ALTER TABLE `invoice` DISABLE KEYS */;
/*!40000 ALTER TABLE `invoice` ENABLE KEYS */;

-- Dumping structure for table vietnamtour.tour
CREATE TABLE IF NOT EXISTS `tour` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cid` int(11) NOT NULL DEFAULT 1,
  `title` varchar(200) NOT NULL,
  `duration` varchar(200) NOT NULL,
  `price` int(100) NOT NULL,
  `desc` text NOT NULL,
  `image` text NOT NULL,
  `keywords` text NOT NULL,
  `maxpax` int(11) NOT NULL,
  `availpax` int(11) NOT NULL,
  `startdat` datetime NOT NULL DEFAULT current_timestamp(),
  `createdat` datetime NOT NULL DEFAULT current_timestamp(),
  `updatedat` datetime NOT NULL DEFAULT current_timestamp(),
  `deleted` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `category_fk` (`cid`),
  CONSTRAINT `category_fk` FOREIGN KEY (`cid`) REFERENCES `category` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;

-- Dumping data for table vietnamtour.tour: ~20 rows (approximately)
/*!40000 ALTER TABLE `tour` DISABLE KEYS */;
REPLACE INTO `tour` (`id`, `cid`, `title`, `duration`, `price`, `desc`, `image`, `keywords`, `maxpax`, `availpax`, `startdat`, `createdat`, `updatedat`, `deleted`) VALUES
	(1, 1, 'MekongDelta Floating Market', '2 days', 1000, 'Mekong Delta leaves a strong impression on tourists by its green/yellow rice paddies, fruits orchards, Southern folk songs, and delicious regional dishes. What make its unique are dense rivers, canals system, house stilts, and lifestyle of locals on river. Thus, it is quite exceptional and awesome to surround yourself with hectic and vibrant floating markets – one of the noticeable highlights of the Mekong Delta', 'vt_seacruise_img1_MekongDelta_Floating_Market.jpg', 'MekongDelta Floating Market', 10, 10, '2018-10-05 09:35:12', '2018-09-04 21:29:24', '2018-09-06 09:41:31', 0),
	(2, 1, 'VungVieng Fishing Village', '1 day', 1200, 'VungVieng often starts 5 AM and lasts until midday. But, it is better to arrive in it before 9 AM. The main goods here include wholesale fruits, agricultural products and specialties from Chau Thanh District and other neighboring areas. Of course, there is no need for sellers to cry out about their products. As mentioned above, each boat has a long upright pole where the samples of the goods are hung on the top for sale. So, if you want to buy bananas, for instance, just find which boat has bananas on the pole.', 'vt_seacruise_img2_Vung_Vieng_Fishing_Village_In_HaLong.jpg', 'VungVieng Fishing Village HaLong', 10, 10, '2018-10-05 09:35:12', '2018-09-04 21:29:24', '2018-09-06 09:49:19', 0),
	(3, 1, 'One Pillar Pagoda', '50 minutes', 900, 'Rising from one pillar in the centre of an elegantly square shaped lotus pond, The One Pillar Pagoda is said to represent a lotus flower growing up out of the water. Built between the years of 1028 and1054 during the reign of Emperor Ly Thai Tong of the Ly Dynasty, the One Pillar Pagoda is one of Vietnam’s most iconic temples.', 'vt_seacruise_img3_One_Pillar_Pagoda.jpg', 'Pillar Pagoda', 10, 10, '2018-10-05 09:35:12', '2018-09-04 21:29:24', '2018-09-06 09:41:31', 0),
	(4, 1, 'Spectacular Halong Bay 2-Day Tour with Deluxe Cruises', '2 days', 1000, 'Experience the karst landscape of Halong Bay on a 2-day cruise, with round-trip transfers from Hanoi. Climb aboard a traditional junk boat and sail among thousands of small islets. Stop to swim and kayak in the emerald waters, and discover hidden caves.', 'vt_seacruise_img4__Halong_Bay.jpg', 'Halong Bay', 10, 10, '2018-10-05 09:35:12', '2018-09-04 21:29:24', '2018-09-04 21:29:24', 0),
	(5, 1, 'Cai Be Floating Market', '50 minutes', 1100, 'Another prominent floating market in Mekong Delta is Cai Be, which is located at Tien Rive in the intersection of Vinh Long, Tien Giang and Ben Tre Provinces. This market is busy from the early morning (around 3 AM) to late afternoon. The goods exhibited here are abundant and multiform, ranging from fruits, seafood, animals, chicken, materials and commodities to sweet mandarin, coconut soap and candy. About 400 to 500 rafts and boats packed with vegetables, fruits and other regional products will be anchored along the 2 sides of the river. Just like Cai Rang Floating Market, the samples of products in each boat here will also be hung on a pole to attract customers.', 'vt_seacruise_img5_MuongHoa_Valley.jpg', 'MuongHoa Valley', 10, 10, '2018-10-05 09:35:12', '2018-09-04 21:29:24', '2018-09-04 21:29:24', 0),
	(6, 1, 'Tra On Floating Market', '50 minutes', 1200, 'Tra On Floating Market will surely give you a feeling for the rhythm of how the daily life there is. Situated at the intersection of the Hau River and Mang Thit River, adjacent to Tra On Town, this is known as the final floating market on the Hau River. The market often starts a new day at the very early morning, about 2 AM to 3 AM. But, you can arrive in it around 5 AM to 6 AM as this is the busiest moment. At that time, various boats full of products will gather here.', 'vt_seacruise_img6_MySon_Holly_Land.jpg', 'MySon Holly Land', 10, 10, '2018-10-05 09:35:12', '2018-09-04 21:29:24', '2018-09-04 21:29:24', 0),
	(7, 1, 'Nga Nam Floating Market', '50 minutes', 1000, 'Located in Soc Trang Province, what make Nga Nam Floating Market stand out from others is that it lies on the convergent point of 5 rivers flowing into 5 directions: My Long (Hau Giang), Vinh Quoi (Soc Trang), Phung Hiep (Hau Giang), Phuoc Long (Bac Lieu) and Phu Loc (Soc Trang). That’s why it is also called Nga Nam Market.', 'vt_seacruise_img1_MekongDelta_Floating_Market.jpg', 'MekongDelta Floating Market', 10, 10, '2018-10-05 09:35:12', '2018-09-04 21:29:24', '2018-09-04 21:29:24', 0),
	(8, 1, 'Nga Bay Floating Market', '50 minutes', 1200, 'Also called Phung Hiep Floating Market, Nga Bay is known as the remarkable market in the Mekong Delta. In fact, it is just a general market on river, where people may purchase wholesale and retail. But, when arriving in here, visitors can be overwhelmed by inspirational colors of fruits, vegetables, seafood, poultry products, handicraft goods, domestic utensils as well as aquatic products of the waterway region. Goods come with various and abundant categories. Along with fruits, what make you amazed is the supply of snakes, birds, tortoises, iguanas and all year around.', 'vt_seacruise_img2_Vung_Vieng_Fishing_Village_In_HaLong.jpg', 'VungVieng Fishing Village HaLong', 10, 10, '2018-10-05 09:35:12', '2018-09-04 21:29:24', '2018-09-04 21:29:24', 0),
	(9, 1, 'Full Day Hoa Lu - Tam Coc', '1 day', 1000, 'Visit Hoa Lu and Tam Coc and learn more about the northern Vietnamese countryside, history, and culture on a full-day tour. Hoa Lu is an ancient capital, and Tam Coc, which means “3 caves,” features karst formations and caves you’ll explore by boat.', 'vt_seacruise_img3_One_Pillar_Pagoda.jpg', 'Full Day Hoa Lu Tam Coc', 10, 10, '2018-10-05 09:35:12', '2018-09-04 21:29:24', '2018-09-04 21:29:24', 0),
	(10, 1, 'Water Puppet Theatre', '50 minutes', 800, 'Enjoy a show at the Thang Long Water Puppet Theatre. Discover a unique part of Vietnamese culture.', 'vt_seacruise_img4__Halong_Bay.jpg', 'Water Puppet Theatre', 10, 10, '2018-10-05 09:35:12', '2018-09-04 21:29:24', '2018-09-04 21:29:24', 0),
	(11, 2, 'Hanoi to Puluong Nature Reserve One-Way Transfer', '2 days', 1200, 'Book a private one-way 7-seat transfer service from Hanoi Old Quarter to Puluong Nature Reserve and skip the stress of public transport.', 'vt_seacruise_img5_MuongHoa_Valley.jpg', 'Hanoi Puluong Nature Reserve', 10, 10, '2018-10-05 09:35:12', '2018-09-04 21:29:24', '2018-09-04 21:29:24', 0),
	(12, 2, 'Hoa Lu, Tam Coc, Mua Cave with Amazing View-All Inclusive', '3 days', 1500, 'Join a unique rowing boat trip to visit three caves, have a buffet lunch and bike to traditional villages. Visit Hoa Lu, an ancient capital, Mua Cave, and enjoy a stunning panoramic views of the countryside and Ngo Dong River.', 'vt_seacruise_img6_MySon_Holly_Land.jpg', 'Hoa Lu Tam Coc Mua Cave', 10, 10, '2018-10-05 09:35:12', '2018-09-04 21:29:24', '2018-09-04 21:29:24', 0),
	(13, 2, 'Halong Bay, Thien Cung Cave - Kayak with Incredible Cruise', '1 day', 1100, 'Cruise among the limestone rocks and islets of Ha Long Bay and experience the UNESCO World Heritage Site from the water on a full-day tour from Hanoi.', 'vt_seacruise_img1_MekongDelta_Floating_Market.jpg', 'MekongDelta Floating Market', 10, 10, '2018-10-05 09:35:12', '2018-09-04 21:29:24', '2018-09-04 21:29:24', 0),
	(14, 2, 'Special Street Food Tour with Expert Tour Guide', '2 hours', 500, 'Visiting Hanoi, you would like to try Vietnamese cuisine with its mouth-watering dishes but you don’t know which foods to try or the best places to try them. This tour will make your stomach happy. Come hungry, leave satisfied.', 'vt_seacruise_img2_Vung_Vieng_Fishing_Village_In_HaLong.jpg', 'VungVieng Fishing Village HaLong', 10, 10, '2018-10-05 09:35:12', '2018-09-04 21:29:24', '2018-09-04 21:29:24', 0),
	(15, 2, 'Full-Day Perfume Pagoda', '1 day', 1000, 'Perfume Pagoda is famous for scenic landscape and sacredness in the religious culture of Vietnamese people. It possesses a good combination of temples, pagodas, moutains and forest.', 'vt_seacruise_img3_One_Pillar_Pagoda.jpg', 'Perfume Pagoda', 10, 10, '2018-10-05 09:35:12', '2018-09-04 21:29:24', '2018-09-04 21:29:24', 0),
	(16, 2, 'Trang An and Bai Dinh Full-Day Tour', '1 day', 1200, 'Journey through the ancient landscapes of Bai Đinh and Trang An on a full-day tour from Hanoi. Visit the largest pagoda temple complex in South East Asia and row up to the grottoes of Trang An aboard a traditional sampan.', 'vt_seacruise_img4__Halong_Bay.jpg', 'Trang An Bai Dinh', 10, 10, '2018-10-05 09:35:12', '2018-09-04 21:29:24', '2018-09-04 21:29:24', 0),
	(17, 2, '2-Day Spectacular Sapa Trekking and Bus Tour', '2 days', 1500, 'The French used to consider Sapa to be the summer capital of Northern Vietnam in the early decades of the 20th century. Find out more about the townships history during this 2-day bus tour.', 'vt_seacruise_img5_MuongHoa_Valley.jpg', 'Sapa Trekking Bus Tour', 10, 10, '2018-10-05 09:35:12', '2018-09-04 21:29:24', '2018-09-04 21:29:24', 0),
	(18, 2, 'My Son Sanctuary', '1 day', 800, 'Between the 4th and 13th centuries a unique culture which owed its spiritual origins to Indian Hinduism developed on the coast of contemporary Viet Nam. This is graphically illustrated by the remains of a series of impressive tower-temples located in a dramatic site that was the religious and political capital of the Champa Kingdom for most of its existence.', 'vt_seacruise_img6_MySon_Holly_Land.jpg', 'My Son Sanctuary', 10, 10, '2018-10-05 09:35:12', '2018-09-04 21:29:24', '2018-09-04 21:29:24', 0),
	(19, 2, 'Half-Day Walking Tour of Hoi An Ancient Town', 'a half of day', 500, 'Spend the morning exploring the ancient city of Hoi An, a well-preserved South-East Asian trading port. Wander along the UNESCO-listed streets, learning about the ports rich international heritage—evident in its centuries-old architecture—from a knowledgeable local guide. Meet your guide at your hotel lobby in Hoi An and begin with a walk to the Ancient Town. Walk along the narrow streets, lined on either side by ancient houses with clear Chinese, Japanese, French, and European architectural influences. Learn about the heritage of both international and indigenous cultures in the ancient port.', 'vt_seacruise_img1_MekongDelta_Floating_Market.jpg', 'MekongDelta Floating Market', 10, 10, '2018-10-05 09:35:12', '2018-09-04 21:29:24', '2018-09-04 21:29:24', 0),
	(20, 2, 'Private Sightseeing Between Hoi An - Hue', '1 day', 1000, 'Cruise between 2 of Central Vietnams most intriguing cities in private and comfortable transpiration. With convenient pickup from your hotel in Hue or Hoi An, travel with your own tour guide and get the chance to stop off at historic sites, immersive museums, and breathtaking vantage points.', 'vt_seacruise_img2_Vung_Vieng_Fishing_Village_In_HaLong.jpg', 'VungVieng Fishing Village HaLong', 10, 10, '2018-10-05 09:35:12', '2018-09-04 21:29:24', '2018-09-04 21:29:24', 0);
/*!40000 ALTER TABLE `tour` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
