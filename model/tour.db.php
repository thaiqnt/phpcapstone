<?php
/**
 * Database module for Tour Information
 *
 * @author      Thai Tran Ngoc Quoc
 * @copyright   Copyright (c) 2018 - All rights reserved
 */

// ------------------------------------------------------------------------

    /**
     * Get all tour categories
     *
     * @return  array: an array of category information
     */
    function retrieveCategories() {
        global $pdo;

        $query = "SELECT `id`,
          `title`
          FROM `category`";

        // Prepare the query
        $stmt = $pdo->prepare($query);
        // Do it with id
        $stmt->execute();
        // Get the result
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        // Free all resource allocated
        $stmt = null;

        // To prevent XSS attacks
        if (count($result) > 0) {
          // There is only one customer found, pick one row only
          $result = getArrayForForm($result);
          
          return $result;
        }

        // Reach here? There is nothing found
        return array();
    }
    // --------------------------------------------------------------------



    /**
     * Get all of tour information if possible by its id
     *
     * @param  tourid: id of a tour
     * @return  array: an array of tour information
     */
    function retrieveTourInfo($tourid, $activetourcheck = true) {
        global $pdo;

        // Search only for active tours, not deleted tours
        $activetour = '';
        if ($activetourcheck) {
          $activetour = ' deleted = 0 AND ';
        }
        
        // Prepare the query
        $stmt = $pdo->prepare("SELECT `id`,
          `cid`,
          `title`,
          `duration`,
          `price`,
          `desc`,
          `image`,
          `keywords`,
          `maxpax`,
          `availpax`,
          `startdat`,
          `createdat`,
          `deleted`,
          `updatedat` 
          FROM `tour` 
          WHERE 
          $activetour
          id = ?");
        // Do it with id
        $stmt->execute([$tourid]);
        // Get the result
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        // Free all resource allocated
        $stmt = null;


        // To prevent XSS attacks
        if (count($result) > 0) {
          // There is only one customer found, pick one row only
          $result = getArrayForForm($result[0]);
          
          return $result;
        }

        // Get back the result
        return array();
    }
    // --------------------------------------------------------------------


    /**
     * Get all of tours information if possible
     *
     * @param  catid: id of a category of a tour
     * @param  keyword: keyword for searching
     * @return  array: an array of tour information
     */
    function retrieveTours($catid, $keyword = '', $activetourcheck = true) {
        global $pdo;

        // Search only for active tours, not deleted tours
        $activetour = '';
        if ($activetourcheck) {
          $activetour = ' deleted = 0 AND ';
        }
        
        // Search by keywords
        $str = '';
        if ($keyword == '') {
          $options = [$catid];
        } else {
          $str = ' AND (`desc` LIKE ? OR `title` LIKE ?)';
          $key = "%$keyword%";
          $options = [$catid, $key, $key];
          
        }
        
        // Prepare the query, return the tours which are not expired and available pax should be greater than zero
        $stmt = $pdo->prepare("SELECT `id`,
          `cid`,
          `title`,
          `duration`,
          `price`,
          `desc`,
          `image`,
          `keywords`,
          `maxpax`,
          `availpax`,
          `startdat`,
          `createdat`,
          `updatedat`,
          `deleted`
          FROM `tour` 
          WHERE 
          $activetour
          cid = ?
          AND
          availpax > 0 
          AND
          startdat > NOW() $str");
      
        // Do it with id
        $stmt->execute($options);
        // Get the result
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        // Free all resource allocated
        $stmt = null;


        // To prevent XSS attacks
        if (count($result) > 0) {
        //dumpList($result);
          // Prevent XSS attacks
          $result = getArrayForForm($result);
          
          return $result;
        }

        // Get back the result
        return $result;
    }
    // --------------------------------------------------------------------

    /**
     * Get all of payment information from a customer
     *
     * @param  customerid: id of a customer
     * @return  array: an array of payment information
     */
    function retrievePaymentInfo($customerid) {
        global $pdo;

        // First, we are looking into the lastest invoice of the user to get the lastest update of payment
        $stmt = $pdo->prepare("SELECT `id`,
          `cid`,
          `billto`,
          `address`,
          `phone`,
          `fax`,
          `email`,
          `tax`,
          `subtotal`,
          `depositeamt`,
          `duedate`,
          `createdat`,
          `updatedat`,
          `deleted`
          FROM `invoice` 
          WHERE 
          id = (
            SELECT MAX(id)
            FROM `invoice` 
            WHERE cid = :cid
          )");
        // Do it with customer id
        $stmt->execute([$customerid]);
        // Get the result
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        // Free all resource allocated
        $stmt = null;


        // To prevent XSS attacks
        if (count($result) > 0) {
          // There is only one invoice found, pick one row only
          $result = getArrayForForm($result[0]);
          
          return $result;
        }

        // Prepare the query to get the current time
        $stmt = $pdo->prepare("SELECT NOW() as thetime");
        // Do it 
        $stmt->execute();
        // Get the result
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        // Free all resource allocated
        $stmt = null;

        $duedate = $result[0]['thetime'];

        // In this case, we are trying our best to fill the payment information from customer information in session
        $paymentinfo = array('billto' => "{$_SESSION[USERINFO]['firstname']} {$_SESSION[USERINFO]['lastname']}",
            'address' => "{$_SESSION[USERINFO]['street']}, {$_SESSION[USERINFO]['postalcode']}, {$_SESSION[USERINFO]['city']}, {$_SESSION[USERINFO]['province']}, {$_SESSION[USERINFO]['country']}",
            'duedate' => $duedate,
            'phone' => $_SESSION[USERINFO]['phone'],
            'fax' => $_SESSION[USERINFO]['phone'],
            'email' => $_SESSION[USERINFO]['email'],
            'tax' => 0,
            'subtotal' => 0,
            'depositeamt' => 0
            );       

        return $paymentinfo;
    }
    // --------------------------------------------------------------------

    /**
     * order Tour, put information from a cart into orders
     *
     * @param  customerid: id of a customer
     * @param  cartArr: cart info
     * @param  paymentinfo: payment info
     * @return  array: an empty array if successful or an array of tour information that were booked by other customers
     */
    function orderTour($customerid, $cartArr, $paymentinfo) {
        global $pdo;

        // If Cart is not empty
        if (count($cartArr)>0) {
        
          // Find tours which are booked by someone else by looking at a decreased available PAX in a tour
          $query = '';
          foreach ($cartArr as $key => $row) {
            $pax = intval($row['pax']);
            $query .= " OR (id = $key AND availpax < $pax)";
          }

          // Remove the first OR to prevent syntax error :)
          if (strlen($query)>0) {
            $query = "SELECT `id`
              FROM `tour` 
              WHERE " . substr($query, 4);
          }
          
          // Prepare the query
          $stmt = $pdo->prepare($query);
          // Do it with id
          $stmt->execute();
          // Get the result
          $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
          // Free all resource allocated
          $stmt = null;


          // To prevent XSS attacks
          if (count($result) > 0) {
            // Prevent XSS attacks
            $result = getArrayForForm($result);

            // Yes, there are some tours which are booked by others
            return $result;
          } else {
            // OK, No tours in the cart are booked, go on with our orders
            try {
              // Create a new invoice for a tour 
              $stmt = $pdo->prepare("INSERT INTO `invoice`
                (`cid`,
                `billto`,
                `address`,
                `phone`,
                `fax`,
                `email`,
                `tax`,
                `subtotal`,
                `depositeamt`,
                `duedate`)
                VALUES
                (:cid,
                :billto,
                :address,
                :phone,
                :fax,
                :email,
                :tax,
                :subtotal,
                :depositeamt,
                :duedate)"
                );

              $stmt->bindParam(':cid', $customerid, PDO::PARAM_INT);
              $stmt->bindParam(':billto', $paymentinfo['billto'], PDO::PARAM_STR);
              $stmt->bindParam(':address', $paymentinfo['address'], PDO::PARAM_STR);
              $stmt->bindParam(':duedate', $paymentinfo['duedate'], PDO::PARAM_STR);
              $stmt->bindParam(':phone', $paymentinfo['phone'], PDO::PARAM_STR);
              $stmt->bindParam(':fax', $paymentinfo['fax'], PDO::PARAM_STR);
              $stmt->bindParam(':email', $paymentinfo['email'], PDO::PARAM_STR);
              $stmt->bindParam(':tax', $paymentinfo['tax'], PDO::PARAM_INT);
              $stmt->bindParam(':subtotal', $paymentinfo['subtotal'], PDO::PARAM_INT);
              $stmt->bindParam(':depositeamt', $paymentinfo['depositeamt'], PDO::PARAM_INT);


              // Do it 
              $stmt->execute();

              // Free all resource allocated
              $stmt = null;

              $invoiceid = $pdo->lastInsertId();

              foreach ($cartArr as $key => $row) {
                // Update available PAX in tour
                $stmt = $pdo->prepare("UPDATE `tour`
                  SET
                  availpax = availpax - :availpax,
                  updatedat = NOW()
                  WHERE id = :id"
                  );

                $stmt->bindParam(':id', $key, PDO::PARAM_INT);
                $stmt->bindParam(':availpax', $row['pax'], PDO::PARAM_INT);

                // Do it 
                $stmt->execute();
                // Free all resource allocated
                $stmt = null;
              
                // Create a new order for a tour in the Cart
                $stmt = $pdo->prepare("INSERT INTO `customer_order`
                  (`tranid`,
                  `iid`,
                  `tid`,
                  `pax`,
                  `title`,
                  `startdat`,
                  `price`,
                  `duration`)
                  VALUES
                  (:tranid,
                  :iid,
                  :tid,
                  :pax,
                  :title,
                  :startdat,
                  :price,
                  :duration)"
                  );
                
                // Transaction id needs a unique id
                $tmp = time();
                $stmt->bindParam(':tranid', $tmp, PDO::PARAM_INT);
                // Customer id
                $stmt->bindParam(':iid', $invoiceid, PDO::PARAM_INT);
                // Tour id
                $stmt->bindParam(':tid', $key, PDO::PARAM_INT);
                // Ordered PAX
                $stmt->bindParam(':pax', $row['pax'], PDO::PARAM_INT);
                // Tour id
                $stmt->bindParam(':title', $row['title'], PDO::PARAM_STR);
                // Tour id
                $stmt->bindParam(':startdat', $row['startdat'], PDO::PARAM_STR);
                // Tour id
                $stmt->bindParam(':price', $row['price'], PDO::PARAM_INT);
                // Tour id
                $stmt->bindParam(':duration', $row['duration'], PDO::PARAM_STR);

                // Do it 
                $stmt->execute();
                // Free all resource allocated
                $stmt = null;

              }
              
              
            } catch (Exception $e) {
              // There is a problem in database, we let users know
              return array('error' => $e->getMessage());
            }
            
          }

        }
      
      // Everything is OK
      return array();
    }
    // --------------------------------------------------------------------

    /**
     * Mark a tour as deleted by updating deleted field
     *
     * @param  tourid: id of a tour
     * @return  none
     */
    function activateTour($tourid, $isactive = true) {
      global $pdo;

      $str = ($isactive) ? 0 : 1;
      // Update available PAX in tour
      $stmt = $pdo->prepare("UPDATE `tour`
        SET
        deleted = $str
        WHERE id = :id"
        );

      $stmt->bindParam(':id', $tourid, PDO::PARAM_INT);

      // Do it 
      $stmt->execute();
      // Free all resource allocated
      $stmt = null;

    }

    // --------------------------------------------------------------------

    /**
     * Update an existed tour
     *
     * @return  string: a string indicates type of errors or is empty if there is OK
     */
    function createNewTour() {
        global $pdo;  
        global $tour_id;
        global $tour_categoryid;
        global $tour_title;
        global $tour_image;
        global $tour_price;
        global $tour_duration;
        global $tour_keywords;
        global $tour_startdate;
        global $tour_maxpax;
        global $tour_availpax;
        global $tour_desc;
        global $tour_createdat;
        global $tour_updatedat;

        global $tour_maxlength;
        global $tour_postedvalue;
        global $tour;

        try {
            // Prepare the query          
          $stmt = $pdo->prepare("INSERT INTO `tour`
          (`cid`,
          `title`,
          `duration`,
          `price`,
          `desc`,
          `image`,
          `keywords`,
          `maxpax`,
          `availpax`,
          `startdat`)
          VALUES
          (:cid,
          :title,
          :duration,
          :price,
          :desc, 
          :image,
          :keywords,
          :maxpax,
          :availpax,
          :startdat)"
          );
          // Bind them
          // Be noted that all: all field values have been SANTINIZED in userregistration.cfg that should be
          // looked into for verification to prevent any kind of XSS or injection attacks
          $stmt->bindParam(':cid', $tour[$tour_categoryid][$tour_postedvalue], PDO::PARAM_INT, 
            $tour[$tour_categoryid][$tour_maxlength]);
          $stmt->bindParam(':title', $tour[$tour_title][$tour_postedvalue], PDO::PARAM_STR, 
            $tour[$tour_title][$tour_maxlength]);
          $stmt->bindParam(':image', $tour[$tour_image][$tour_postedvalue], PDO::PARAM_STR, 
            $tour[$tour_image][$tour_maxlength]);
          $stmt->bindParam(':price', $tour[$tour_price][$tour_postedvalue], PDO::PARAM_INT, 
            $tour[$tour_price][$tour_maxlength]);
          $stmt->bindParam(':duration', $tour[$tour_duration][$tour_postedvalue], PDO::PARAM_STR, 
            $tour[$tour_duration][$tour_maxlength]);
          $stmt->bindParam(':keywords', $tour[$tour_keywords][$tour_postedvalue], PDO::PARAM_STR, 
            $tour[$tour_keywords][$tour_maxlength]);
          $stmt->bindParam(':startdat', $tour[$tour_startdate][$tour_postedvalue], PDO::PARAM_STR, 
            $tour[$tour_startdate][$tour_maxlength]);
          $stmt->bindParam(':maxpax', $tour[$tour_maxpax][$tour_postedvalue], PDO::PARAM_INT, 
            $tour[$tour_maxpax][$tour_maxlength]);
          $stmt->bindParam(':availpax', $tour[$tour_availpax][$tour_postedvalue], PDO::PARAM_INT, 
            $tour[$tour_availpax][$tour_maxlength]);
          $stmt->bindParam(':desc', $tour[$tour_desc][$tour_postedvalue], PDO::PARAM_STR, $tour[$tour_desc][$tour_maxlength]);

          // Do it
          $stmt->execute();
          $stmt = null;
        } catch(Exception $e) {
          // Something wrong with database is coming
          return 'Error has been found in database: '.$e->getMessage();
        }    

        // Everything is OK
        return '';
    }


    // --------------------------------------------------------------------

    /**
     * Get the last inserted id if possible
     *
     * @return  string: the desired id
     */
    function getLastInsertedId() {
        global $pdo;
        return $pdo->lastInsertId();
    }

    // --------------------------------------------------------------------

    /**
     * Update an existed tour
     *
     * @return  string: a string indicates type of errors or is empty if there is OK
     */
    function updateTour() {
        global $pdo;  
        global $tour_id;
        global $tour_categoryid;
        global $tour_title;
        global $tour_image;
        global $tour_price;
        global $tour_duration;
        global $tour_keywords;
        global $tour_startdate;
        global $tour_maxpax;
        global $tour_availpax;
        global $tour_desc;
        global $tour_createdat;
        global $tour_updatedat;

        global $tour_maxlength;
        global $tour_postedvalue;
        global $tour;

        try {
            // Prepare the query          
          $stmt = $pdo->prepare("UPDATE  `tour`
          SET 
          `cid` = :cid,
          `title` = :title,
          `duration` = :duration,
          `price` = :price,
          `desc` = :desc, 
          `image` = :image,
          `keywords` = :keywords,
          `maxpax` = :maxpax,
          `availpax` = :availpax,
          `startdat` = :startdat,
          `updatedat` = NOW()
          WHERE id = :id "
          );
          // Bind them
          // Be noted that all: all field values have been SANTINIZED in userregistration.cfg that should be
          // looked into for verification to prevent any kind of XSS or injection attacks
          $stmt->bindParam(':id', $tour[$tour_id][$tour_postedvalue], PDO::PARAM_INT);
          $stmt->bindParam(':cid', $tour[$tour_categoryid][$tour_postedvalue], PDO::PARAM_INT, 
            $tour[$tour_categoryid][$tour_maxlength]);
          $stmt->bindParam(':title', $tour[$tour_title][$tour_postedvalue], PDO::PARAM_STR, 
            $tour[$tour_title][$tour_maxlength]);
          $stmt->bindParam(':image', $tour[$tour_image][$tour_postedvalue], PDO::PARAM_STR, 
            $tour[$tour_image][$tour_maxlength]);
          $stmt->bindParam(':price', $tour[$tour_price][$tour_postedvalue], PDO::PARAM_INT, 
            $tour[$tour_price][$tour_maxlength]);
          $stmt->bindParam(':duration', $tour[$tour_duration][$tour_postedvalue], PDO::PARAM_STR, 
            $tour[$tour_duration][$tour_maxlength]);
          $stmt->bindParam(':keywords', $tour[$tour_keywords][$tour_postedvalue], PDO::PARAM_STR, 
            $tour[$tour_keywords][$tour_maxlength]);
          $stmt->bindParam(':startdat', $tour[$tour_startdate][$tour_postedvalue], PDO::PARAM_STR, 
            $tour[$tour_startdate][$tour_maxlength]);
          $stmt->bindParam(':maxpax', $tour[$tour_maxpax][$tour_postedvalue], PDO::PARAM_INT, 
            $tour[$tour_maxpax][$tour_maxlength]);
          $stmt->bindParam(':availpax', $tour[$tour_availpax][$tour_postedvalue], PDO::PARAM_INT, 
            $tour[$tour_availpax][$tour_maxlength]);
          $stmt->bindParam(':desc', $tour[$tour_desc][$tour_postedvalue], PDO::PARAM_STR, $tour[$tour_desc][$tour_maxlength]);

          // Do it
          $stmt->execute();
          $stmt = null;
        } catch(Exception $e) {
          // Something wrong with database is coming
          return 'Error has been found in database: '.$e->getMessage();
        }    

        // Everything is OK
        return '';
    }


    // --------------------------------------------------------------------
?>