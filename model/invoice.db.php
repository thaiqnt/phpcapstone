<?php
/**
 * Database module for Invoice Information
 *
 * @author      Thai Tran Ngoc Quoc
 * @copyright   Copyright (c) 2018 - All rights reserved
 */

// ------------------------------------------------------------------------



/**
 * Get all of invoices information based on customer id if possible
 *
 * @param  userid: id of a user of a tour
 * @return  array: an array of tour information
 */
function retrieveInvoicesByCustomer($userid) {
    global $pdo;

    // Prepare the query, return the invoices
    $stmt = $pdo->prepare("SELECT 
      `invoice`.`id`,
      `invoice`.`cid`,
      `invoice`.`billto`,
      `invoice`.`address`,
      `invoice`.`phone`,
      `invoice`.`fax`,
      `invoice`.`email`,
      `invoice`.`tax`,
      `invoice`.`subtotal`,
      `invoice`.`depositeamt`,
      `invoice`.`duedate`,
      `invoice`.`createdat`,
      `invoice`.`updatedat`,
      `invoice`.`deleted`
      FROM `invoice`
      WHERE `invoice`.`cid` = :cid
      ");

    $stmt->bindParam(':cid', $userid, PDO::PARAM_INT);
  
    // Do it with id
    $stmt->execute();
    // Get the result
    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
    // Free all resource allocated
    $stmt = null;


    // To prevent XSS attacks
    if (count($result) > 0) {
    //dumpList($result);
      // Prevent XSS attacks
      $result = getArrayForForm($result);

      return $result;
    }

    // Get back the result
    return $result;
}

// --------------------------------------------------------------------

/**
 * Get all of orders information if possible
 *
 * @param  invid: id of a invoice of a tour
 * @return  array: an array of tour information
 */
function retrieveOrdersByInvoice($invid) {
    global $pdo;

    $stmt = $pdo->prepare("SELECT 
      `invoice`.`cid`,
      `invoice`.`billto`,
      `invoice`.`address`,
      `invoice`.`phone`,
      `invoice`.`fax`,
      `invoice`.`email`,
      `invoice`.`tax`,
      `invoice`.`subtotal`,
      `invoice`.`depositeamt`,
      `invoice`.`duedate`,
      `invoice`.`createdat` AS icreatedat,
      `invoice`.`updatedat` AS iupdatedat,
      `invoice`.`deleted`,
      `customer_order`.`id` AS oid,
      `customer_order`.`tranid`,
      `customer_order`.`iid`,
      `customer_order`.`tid`,
      `customer_order`.`pax`,
      `customer_order`.`title`,
      `customer_order`.`duration`,
      `customer_order`.`price`,
      `customer_order`.`startdat`,
      `customer_order`.`createdat` AS ocreatedat,
      `customer_order`.`updatedat` AS oupdatedat,
      `category`.`title` AS ctitle
      FROM `invoice`
      JOIN `customer_order` 
      ON `invoice`.`id` = `customer_order`.`iid`
      JOIN `tour` 
      ON `customer_order`.`tid` = `tour`.`id`
      JOIN `category` 
      ON `tour`.`cid` = `category`.`id`
      WHERE `invoice`.`id` = :id
      ");
    $stmt->bindParam(':id', $invid, PDO::PARAM_INT);
  
    // Do it with id
    $stmt->execute();
    // Get the result
    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
    // Free all resource allocated
    $stmt = null;


    // To prevent XSS attacks
    if (count($result) > 0) {
    //dumpList($result);
      // Prevent XSS attacks
      $result = getArrayForForm($result);

      return $result;
    }

    // Get back the result
    return $result;
}

// --------------------------------------------------------------------


/**
 * Get all of statistics information of invoice in general
 *
 * @return  array: an array of statistics information
 */
function retrieveGeneralStatistics() {
    global $pdo;

    // Prepare the query, return the invoices
    $stmt = $pdo->prepare("SELECT 
        MAX(total) AS paymentmax, MIN(total) AS paymentmin, AVG(total) AS paymentavg
        FROM
        (
          SELECT SUM(subtotal + tax - depositeamt) AS total
          FROM
          invoice
          GROUP BY cid
        ) 
        AS
        customer
      ");

    // Do it 
    $stmt->execute();
    // Get the result
    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
    // Free all resource allocated
    $stmt = null;


    // To prevent XSS attacks
    if (count($result) > 0) {
    //dumpList($result);
      // Prevent XSS attacks
      $result = getArrayForForm($result[0]);

      return $result;
    }

    // Get back the result
    return $result;
}

// --------------------------------------------------------------------

/**
 * Get all of statistics information of invoice in detail
 *
 * @return  array: an array of statistics information
 */
function retrieveDetailStatistics() {
    global $pdo;

    // Prepare the query, return the invoices
    $stmt = $pdo->prepare("SELECT 
      tid, mosttourexp, tour.title, pax, theorder.price, iid, invoice.cid, customer.loginname, firstname, lastname
      FROM
      (
        SELECT iid, MAX(pax * price) AS mosttourexp, tid, pax, price
        FROM 
        customer_order
        WHERE
        (pax * price) = 
        (
          SELECT MAX(pax * price)
          FROM 
          customer_order
        )
      ) 
      AS
      theorder
      JOIN 
      tour
      ON
      tour.id = theorder.tid
      JOIN 
      invoice
      ON
      invoice.id = theorder.iid
      JOIN 
      customer
      ON
      invoice.cid = customer.id
      ");

    // Do it 
    $stmt->execute();
    // Get the result
    $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
    // Free all resource allocated
    $stmt = null;


    // To prevent XSS attacks
    if (count($result) > 0) {
    //dumpList($result);
      // Prevent XSS attacks
      $result = getArrayForForm($result[0]);

      return $result;
    }

    // Get back the result
    return $result;
}

// --------------------------------------------------------------------


?>