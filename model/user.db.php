<?php
/**
 * Database module for User Access
 *
 * @author      Thai Tran Ngoc Quoc
 * @copyright   Copyright (c) 2018 - All rights reserved
 */

// ------------------------------------------------------------------------


    /**
     * Get all of customer information if possible by its id
     *
     * @param  customerId: id of a customer
     * @return  array: an array of customer information
     */
    function retrieveLoginInfo() {
        global $pdo;
        global $user_login_loginname;
        global $user_login_password;
        global $user_login_postedvalue;
        global $user_login;

        // Prepare the query
        $stmt = $pdo->prepare("SELECT `id`,`loginname`,`password`,`firstname`,`lastname`,`street`,`city`,".
            "`postalcode`,`province`,`country`,`phone`,`email`,`createdat`,`updatedat`,`deleted` ".
            "FROM `customer` WHERE loginname = ?");
        // Do it with id
        $stmt->execute([$user_login[$user_login_loginname][$user_login_postedvalue]]);
        // Get the result
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        // Free all resource allocated
        $stmt = null;


        // To prevent XSS attacks
        if (count($result) > 0) {
          // There is only one customer found, pick one row only
          $result = getArrayForForm($result[0]);
          
          $password = $user_login[$user_login_password][$user_login_postedvalue];
          
          if (password_verify($password, $result['password'])) {
            return $result;
          } else {
            return array();
          }
        }

        // Get back the result
        return $result;
    }
    // --------------------------------------------------------------------


    /**
     * Create a new customer
     *
     * @return  string: a string indicates type of errors or is empty if there is OK
     */
    function createNewCustomer() {
        global $pdo;
        global $user_reg_loginname;
        global $user_reg_password;
        global $user_reg_repeatedpass;
        global $user_reg_firstname;
        global $user_reg_lastname;
        global $user_reg_street;
        global $user_reg_city;
        global $user_reg_postalcode;
        global $user_reg_province;
        global $user_reg_country;
        global $user_reg_phone;
        global $user_reg_email;
        global $user_reg_deleted;

        global $user_reg_maxlength;
        global $user_reg_postedvalue;
        global $user_reg;

        try {
            // Prepare the query          
          $stmt = $pdo->prepare("INSERT INTO `customer`(`loginname`,`password`,`firstname`,`lastname`,".
            "`street`,`city`,`postalcode`,`province`,`country`,`phone`,`email`,`deleted`) VALUES ".
            "(:loginname,:password,:firstname,:lastname,:street,:city,:postalcode,:province,".
            ":country,:phone,:email,:deleted)");
          // Bind them
          // Be noted that all: all field values have been SANTINIZED in userregistration.cfg that should be
          // looked into for verification to prevent any kind of XSS or injection attacks
          $stmt->bindParam(':loginname', $user_reg[$user_reg_loginname][$user_reg_postedvalue], PDO::PARAM_STR, 
            $user_reg[$user_reg_loginname][$user_reg_maxlength]);
          $password = password_hash($user_reg[$user_reg_password][$user_reg_postedvalue], PASSWORD_DEFAULT);
          $stmt->bindParam(':password', $password, PDO::PARAM_STR, $user_reg[$user_reg_password][$user_reg_maxlength]);
          $stmt->bindParam(':firstname', $user_reg[$user_reg_firstname][$user_reg_postedvalue], PDO::PARAM_STR, 
            $user_reg[$user_reg_firstname][$user_reg_maxlength]);
          $stmt->bindParam(':lastname', $user_reg[$user_reg_lastname][$user_reg_postedvalue], PDO::PARAM_STR, 
            $user_reg[$user_reg_lastname][$user_reg_maxlength]);
          $stmt->bindParam(':street', $user_reg[$user_reg_street][$user_reg_postedvalue], PDO::PARAM_STR, 
            $user_reg[$user_reg_street][$user_reg_maxlength]);
          $stmt->bindParam(':city', $user_reg[$user_reg_city][$user_reg_postedvalue], PDO::PARAM_STR, 
            $user_reg[$user_reg_city][$user_reg_maxlength]);
          $stmt->bindParam(':postalcode', $user_reg[$user_reg_postalcode][$user_reg_postedvalue], PDO::PARAM_STR, 
            $user_reg[$user_reg_postalcode][$user_reg_maxlength]);
          $stmt->bindParam(':province', $user_reg[$user_reg_province][$user_reg_postedvalue], PDO::PARAM_STR, 
            $user_reg[$user_reg_province][$user_reg_maxlength]);
          $stmt->bindParam(':country', $user_reg[$user_reg_country][$user_reg_postedvalue], PDO::PARAM_STR, 
            $user_reg[$user_reg_country][$user_reg_maxlength]);
          $stmt->bindParam(':phone', $user_reg[$user_reg_phone][$user_reg_postedvalue], PDO::PARAM_STR, 
            $user_reg[$user_reg_phone][$user_reg_maxlength]);
          $stmt->bindParam(':email', $user_reg[$user_reg_email][$user_reg_postedvalue], PDO::PARAM_STR, 
            $user_reg[$user_reg_email][$user_reg_maxlength]);
          // Convert to integer type when get value from form
          $tmp = getPostedBoolValue($user_reg[$user_reg_deleted][$user_reg_postedvalue]);
          $stmt->bindParam(':deleted', $tmp, PDO::PARAM_INT);

          // Do it
          $stmt->execute();
          $stmt = null;
        } catch(Exception $e) {
            // Is login name duplicated?
          if($e->errorInfo[1] === 1062) return 'Duplicated login name is found.';

          // Something wrong with database is coming
          return 'Error has been found in database: '.$e->getMessage();
        }    

        // Everything is OK
        return '';
    }
    // --------------------------------------------------------------------

    /**
     * Get the last inserted customer if possible
     *
     * @return  string: the desired id
     */
    function getLastInsertedCustomer() {
        global $pdo;
        return $pdo->lastInsertId();
    }

    // --------------------------------------------------------------------

    /**
     * Get all of customer information if possible by its id
     *
     * @param  customerId: id of a customer
     * @return  array: an array of customer information
     */
    function retrieveCustomer($customerId) {
        global $pdo;

        // Prepare the query
        $stmt = $pdo->prepare("SELECT `id`,`loginname`,`firstname`,`lastname`,`street`,`city`,".
            "`postalcode`,`province`,`country`,`phone`,`email`,`createdat`,`updatedat`,`deleted` ".
            "FROM `customer` WHERE id = ?");
        // Do it with id
        $stmt->execute([$customerId]);
        // Get the result
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        // Free all resource allocated
        $stmt = null;


        // To prevent XSS attacks
        if (count($result) > 0) {
          // There is only one customer found, pick one row only
          $result = getArrayForForm($result[0]);
        }

        // Get back the result
        return $result;
    }
    // --------------------------------------------------------------------

    /**
     * Update an existed customer
     *
     * @return  string: a string indicates type of errors or is empty if there is OK
     */
    function updateCustomer() {
        global $pdo;  
        global $user_update_id;
        global $user_update_loginname;
        global $user_update_password;
        global $user_update_repeatedpass;
        global $user_update_firstname;
        global $user_update_lastname;
        global $user_update_street;
        global $user_update_city;
        global $user_update_postalcode;
        global $user_update_province;
        global $user_update_country;
        global $user_update_phone;
        global $user_update_email;
        global $user_update_deleted;

        global $user_update_maxlength;
        global $user_update_postedvalue;
        global $user_update;

        try {
            // Prepare the query          
          $stmt = $pdo->prepare("UPDATE  `customer`
          SET `loginname` = :loginname,
          `password` = :password,
          `firstname` = :firstname,
          `lastname` = :lastname,
          `street` = :street, 
          `city` = :city,
          `postalcode` = :postalcode,
          `province` = :province,
          `country` = :country,
          `phone` = :phone,
          `email` = :email,
          `deleted` = :deleted,
          `updatedat` = NOW()
          WHERE id = :id "
          );
          // Bind them
          // Be noted that all: all field values have been SANTINIZED in userregistration.cfg that should be
          // looked into for verification to prevent any kind of XSS or injection attacks
          $stmt->bindParam(':id', $user_update[$user_update_id][$user_update_postedvalue], PDO::PARAM_INT);
          $stmt->bindParam(':loginname', $user_update[$user_update_loginname][$user_update_postedvalue], PDO::PARAM_STR, 
            $user_update[$user_update_loginname][$user_update_maxlength]);
          $password = password_hash($user_update[$user_update_password][$user_update_postedvalue], PASSWORD_DEFAULT);
          $stmt->bindParam(':password', $password, PDO::PARAM_STR, $user_update[$user_update_password][$user_update_maxlength]);
          $stmt->bindParam(':firstname', $user_update[$user_update_firstname][$user_update_postedvalue], PDO::PARAM_STR, 
            $user_update[$user_update_firstname][$user_update_maxlength]);
          $stmt->bindParam(':lastname', $user_update[$user_update_lastname][$user_update_postedvalue], PDO::PARAM_STR, 
            $user_update[$user_update_lastname][$user_update_maxlength]);
          $stmt->bindParam(':street', $user_update[$user_update_street][$user_update_postedvalue], PDO::PARAM_STR, 
            $user_update[$user_update_street][$user_update_maxlength]);
          $stmt->bindParam(':city', $user_update[$user_update_city][$user_update_postedvalue], PDO::PARAM_STR, 
            $user_update[$user_update_city][$user_update_maxlength]);
          $stmt->bindParam(':postalcode', $user_update[$user_update_postalcode][$user_update_postedvalue], PDO::PARAM_STR, 
            $user_update[$user_update_postalcode][$user_update_maxlength]);
          $stmt->bindParam(':province', $user_update[$user_update_province][$user_update_postedvalue], PDO::PARAM_STR, 
            $user_update[$user_update_province][$user_update_maxlength]);
          $stmt->bindParam(':country', $user_update[$user_update_country][$user_update_postedvalue], PDO::PARAM_STR, 
            $user_update[$user_update_country][$user_update_maxlength]);
          $stmt->bindParam(':phone', $user_update[$user_update_phone][$user_update_postedvalue], PDO::PARAM_STR, 
            $user_update[$user_update_phone][$user_update_maxlength]);
          $stmt->bindParam(':email', $user_update[$user_update_email][$user_update_postedvalue], PDO::PARAM_STR, 
            $user_update[$user_update_email][$user_update_maxlength]);
          // Convert to integer type when get value from form
          $tmp = getPostedBoolValue($user_update[$user_update_deleted][$user_update_postedvalue]);
          
          $stmt->bindParam(':deleted', $tmp, PDO::PARAM_INT);

          // Do it
          $stmt->execute();
          $stmt = null;
        } catch(Exception $e) {
            // Is login name duplicated?
          if($e->errorInfo[1] === 1062) return 'Duplicated login name is found.';

          // Something wrong with database is coming
          return 'Error has been found in database: '.$e->getMessage();
        }    

        // Everything is OK
        return '';
    }
    // --------------------------------------------------------------------

    /**
     * Get current time of database
     *
     * @return  string: a now time
     */
    function retrieveDBCurrentTime() {
        global $pdo;

        // Prepare the query
        $stmt = $pdo->prepare("SELECT NOW() as thetime");
        // Do it 
        $stmt->execute();
        // Get the result
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        // Free all resource allocated
        $stmt = null;


        // To prevent XSS attacks
        if (count($result) > 0) {
          return $result[0]['thetime'];
        }

        // Get back the empty result
        return array();
    }
    // --------------------------------------------------------------------


?>