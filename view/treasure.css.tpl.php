<?php
$csscontent = <<<EOT
  <style>
    /* Changes are for all treasure items */
    .content.vt-most-treasure {
      background: var(--color-light-0);
    }


    ul.vt-most-treasure {
      background: var(--color-light-0);
    }

    ul.vt-most-treasure {
      padding-bottom: 48px;
    }

    li.vt-most-treasure {
      border-bottom: solid 1px var(--color-light-1);
    }

    a.vt-treasure-inner {
      display: block;
    }

    a.vt-treasure-inner:hover {
      text-decoration: none;
    }

    a.vt-treasure-inner:hover .title.vt-treasure-inner {
      color: var(--color-accent-dark);
    }

    article.vt-treasure-inner {
      position: relative;
    }

    article.vt-treasure-inner {
      padding: 12px 12px 12px 12px;
    }

    h1.vt-treasure-inner {
      color: black;
    }

    .title.vt-treasure-inner {
      font-weight: 700;
    }

    /* Adjusted by screen resizing */

    .title.vt-treasure-inner {
      font-size: 21px;
      line-height: 27px;
    }

    .img.vt-treasure-inner {
      background-size: cover;
      background-position: center;
    }

    .img-self.vt-treasure-inner {
      opacity: 0;
      width: 100%;
      height: 100%;
    }

    .excerpt.vt-treasure-inner {
      color: black;
      margin-top: 6px;
      font-size: 14px;
    }
    
    .doModal {
    }


    @media (min-width: 361px) {
      article.vt-treasure-inner {
        padding: 12px 12px 12px 12px;
      }
    }

    @media (min-width: 701px) {
      article.vt-treasure-inner {
        padding: 24px;
        padding-left: calc(24px + 252px + 24px);
        min-height: calc(24px + 156px + 24px);
      }

      .img.vt-treasure-inner {
        position: absolute;
        top: 24px;
        left: 24px;
        width: 252px;
        height: 156px;
      }

    }

    @media (max-width: 700px) {
      .content.vt-most-treasure {
        padding: 0 24px;
      }

      .img.vt-treasure-inner {
        float: right;
        margin-top: 6px;
        margin-left: 12px;
        width: 160px;
        height: 90px;
      }

      .title.vt-treasure-inner {
        font-size: 16px;
        line-height: 21px;
      }
    }
  </style>
EOT;
?>