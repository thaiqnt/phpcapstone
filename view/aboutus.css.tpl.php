<?php
$csscontent = <<<EOT
  <style>
    /* Setup is for founders */
    .vt-founders {
      padding-top: 40px;
      padding-bottom: 40px;
    }

    .vt-founders__co-founder,
    .vt-founders__heading {
      text-align: center;
    }

    .aa-text-h3 {
      font-size: 28px;
      line-height: 32px;
      letter-spacing: -.4px;
      font-weight: 700;
    }

    .vt-grid-container {
      padding-right: 16px;
      padding-left: 16px;
    }

    .vt-grid-container {
      width: 100%;
      max-width: 1440px;
      margin-right: auto;
      margin-left: auto;
    }

    .vt-grid-row {
      display: -webkit-box;
      display: -webkit-flex;
      display: flex;
      -webkit-flex-wrap: wrap;
      flex-wrap: wrap;
    }

    .vt-founders__co-founder,
    .vt-founders__heading {
      text-align: center;
    }

    .vt-founders__co-founder {
      padding-top: 32px;
    }

    .vt-grid-col {
      padding-right: 8px;
      padding-left: 8px;
    }

    .vt-grid-col {
      position: relative;
      width: 100%;
      min-height: 1px;
    }

    .vt-founders__co-founder,
    .vt-founders__heading {
      text-align: center;
    }

    .vt-founders__image {
      width: 250px;
      height: 250px;
    }

    .vt-founders__name {
      padding-top: 24px;
    }

    .vt-founders__info,
    .vt-founders__name,
    .vt-founders__position {
      max-width: 552px;
      margin-left: auto;
      margin-right: auto;
    }

    .aa-text-h5 {
      font-size: 20px;
      line-height: 24px;
      font-weight: 700;
      letter-spacing: -.2px;
    }

    .vt-founders__info,
    .vt-founders__name,
    .vt-founders__position {
      max-width: 552px;
      margin-left: auto;
      margin-right: auto;
    }

    .vt-founders__position {
      color: #818d99;
    }

    .aa-text-info {
      font-size: 14px;
      line-height: 24px;
    }

    .vt-founders__info {
      padding-top: 16px;
      text-align: left;
    }

    /* Changes are for vt-founders */
    @media (min-width: 568px) {
      .vt-founders,
      .ao-highlights__navigation {
        padding-top: 56px;
      }
    }

    @media (min-width: 568px) {
      .vt-founders {
        padding-bottom: 56px;
      }
    }

    @media (min-width: 1280px) {
      .vt-founders,
      .ao-highlights__navigation {
        padding-top: 72px;
      }
    }

    /* Changes are for aa-text-h3 */
    @media (min-width: 768px) {
      .aa-text-h3 {
        font-size: 32px;
        line-height: 40px;
        letter-spacing: -.6px;
      }
    }


    @media (min-width: 568px) {
      .vt-grid-container {
        padding-right: 28px;
        padding-left: 28px;
      }
    }

    /* Changes are for vt-grid-container */
    @media (min-width: 768px) {
      .vt-grid-container {
        padding-right: 32px;
        padding-left: 32px;
      }
    }

    @media (min-width: 1024px) {
      .vt-grid-container {
        padding-right: 32px;
        padding-left: 32px;
      }
    }

    @media (min-width: 1280px) {
      .vt-grid-container {
        padding-right: 64px;
        padding-left: 64px;
      }
    }

    @media (min-width: 1440px) {
      .vt-grid-container {
        padding-right: 48px;
        padding-left: 48px;
      }
    }

    /* Changes are for vt-founders__co-founder */
    @media (min-width: 568px) {
      .vt-founders__co-founder {
        padding-top: 40px;
      }
    }

    /* Changes are for vt-grid-col */
    @media (min-width: 568px) {
      .vt-grid-col {
        padding-right: 8px;
        padding-left: 8px;
      }
    }

    /* Changes are for vt-grid-col-inner */
    @media (min-width: 568px) {
      .vt-grid-col-inner {
        -webkit-box-flex: 0;
        -webkit-flex: 0 0 50%;
        flex: 0 0 45%;
        max-width: 50%;
      }
    }

    @media (min-width: 768px) {
      .vt-grid-col {
        padding-right: 8px;
        padding-left: 8px;
      }
    }

    @media (min-width: 1024px) {
      .vt-grid-col {
        padding-right: 8px;
        padding-left: 8px;
      }
    }

    @media (min-width: 1280px) {
      .vt-founders__co-founder {
        padding-top: 56px;
      }
    }

    @media (min-width: 1280px) {
      .vt-grid-col {
        padding-right: 12px;
        padding-left: 12px;
      }
    }

    @media (min-width: 1440px) {
      .vt-grid-col {
        padding-right: 12px;
        padding-left: 12px;
      }
    }

    /* Changes are for vt-founders__image */
    @media (min-width: 568px) {
      .vt-founders__image {
        width: 240px;
        height: 240px;
      }
    }

    @media (min-width: 768px) {
      .vt-founders__image {
        width: 250px;
        height: 250px;
      }
    }

    /* Changes are for vt-founders__name */
    @media (min-width: 768px) {
      .vt-founders__name {
        padding-top: 16px;
      }
    }

    @media (min-width: 768px) {
      .vt-founders__info {
        padding-top: 28px;
      }
    }

    @media (min-width: 1024px) {
      .vt-founders__info {
        padding-top: 16px;
      }
    }
    
  </style>
EOT;
?>