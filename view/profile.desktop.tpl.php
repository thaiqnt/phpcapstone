<?php
$hero = <<<EOT
<div id="flashMessageBox" class="flashMessageModal">

  <!-- Show tracking content in modal mode -->
  <div class="flashMessageContent">
    <span class="flashMessageClose">&times;</span>
    <div id="flashMessageDetail"></div>
  </div>
</div>
<script>
    // We show user info that was kept in the session
    function showUserInfo(str) {
      // Get the cart modal
      var infoBox = document.getElementById('flashMessageBox');

      // Get the <span> element that closes the modal
      var span = document.getElementsByClassName("flashMessageClose")[0];

      // When the user clicks on <span> (x), close the modal
      span.onclick = function() {
        infoBox.style.display = "none";
        window.location = 'index.php?p=profile.php';
      }
      
      // When the user clicks anywhere outside of the modal, close it
      window.onclick = function(event) {
        if (event.target == infoBox) {
          infoBox.style.display = "none";
          window.location = 'index.php?p=profile.php';
        }
      }

      infoDetail = document.getElementById("flashMessageDetail");
      if (infoDetail) {
        infoDetail.innerHTML = str;
      }

      // Display the infobox for user
      infoBox.style.display = "block";
    }
    
    function showWelcomeBack() {
      var str = "<div><strong style=\"font-size: 32px; font-weight: bold;\">Welcome back, </strong> <strong style=\"font-size: 32px; font-weight: bold; color: red;\">{$flashMsgInfo[$user_profile_loginname]}</strong></div>Your first time here is <strong style=\"color: red;\">{$flashMsgInfo[$user_profile_createdat]}</strong> and you have successfully loged in. Have a good day in Vietnam Tour!";
      
      showUserInfo(str);

    }
$showUserInfo
</script>
<div class="mainImg_area">
  <div class="mainImg_area__imgBox" style="background-image: url(images/tourbackground.jpg); ">
    <div class="profile">

      <h1 class="text-center title">Customer Profile</h1>
      <h2 class="text-center title">Your information that is being stored in our system</h2>

      <form action="index.php" method="get" id="profile1" autocomplete="off" novalidate>

        <div class="container5">

          <div>
            <label for="$user_profile_loginname">{$user_profile[$user_profile_loginname][$user_profile_label_name]}<span style="color: red">*</span></label>
          </div>
          <div>
            <input type="hidden" name="p" value="index.php"></input>
            <input disabled type="text" size="22" name="$user_profile_loginname" value="{$userinfo[$user_profile_loginname]}"  />
          </div>

          <div>
            <label for="$user_profile_firstname">{$user_profile[$user_profile_firstname][$user_profile_label_name]}<span style="color: red">*</span></label>
          </div>
          <div>
            <input disabled type="text" size="22" name="$user_profile_firstname" value="{$userinfo[$user_profile_firstname]}"  />
          </div>

          <div>
            <label for="$user_profile_lastname">{$user_profile[$user_profile_lastname][$user_profile_label_name]}<span style="color: red">*</span></label>
          </div>
          <div>
            <input disabled type="text" size="22" name="$user_profile_lastname" value="{$userinfo[$user_profile_lastname]}"  />
          </div>

        </div>
        <div class="container6">

          <div>
            <label for="$user_profile_street">{$user_profile[$user_profile_street][$user_profile_label_name]}<span style="color: red">*</span></label>
          </div>
          <div>
            <input disabled type="text" size="22" name="$user_profile_street" value="{$userinfo[$user_profile_street]}"  />
          </div>

          <div>
            <label for="$user_profile_city">{$user_profile[$user_profile_city][$user_profile_label_name]}<span style="color: red">*</span></label>
          </div>
          <div>
            <input disabled type="text" size="22" name="$user_profile_city" value="{$userinfo[$user_profile_city]}"  />
          </div>

          <div>
            <label for="$user_profile_postalcode">{$user_profile[$user_profile_postalcode][$user_profile_label_name]}<span style="color: red">*</span></label>
          </div>
          <div>
            <input disabled type="text" size="22" name="$user_profile_postalcode" value="{$userinfo[$user_profile_postalcode]}"  />
          </div>

          <div>
            <label for="$user_profile_province">{$user_profile[$user_profile_province][$user_profile_label_name]}<span style="color: red">*</span></label>
          </div>
          <div>
            <input disabled type="text" size="22" name="$user_profile_province" value="{$userinfo[$user_profile_province]}"  />
          </div>

          <div>
            <label for="$user_profile_country">{$user_profile[$user_profile_country][$user_profile_label_name]}<span style="color: red">*</span></label>
          </div>
          <div>
            <input disabled type="text" size="22" name="$user_profile_country" value="{$userinfo[$user_profile_country]}"  />
          </div>



        </div>
        <div class="container7">


          <div>
            <label for="$user_profile_phone">{$user_profile[$user_profile_phone][$user_profile_label_name]}<span style="color: red">*</span></label>
          </div>
          <div>
            <input disabled type="text" size="22" name="$user_profile_phone" value="{$userinfo[$user_profile_phone]}"  />
          </div>

          <div>
            <label for="$user_profile_email">{$user_profile[$user_profile_email][$user_profile_label_name]}<span style="color: red">*</span></label>
          </div>
          <div>
            <input disabled type="text" size="22" name="$user_profile_email" value="{$userinfo[$user_profile_email]}"  />
          </div>

          <div>
            <label for="$user_profile_deleted">{$user_profile[$user_profile_deleted][$user_profile_label_name]}<span style="color: red">*</span></label>
          </div>
          <div>
            <input disabled type="checkbox" name="$user_profile_deleted" value="1" {$user_profile[$user_profile_deleted][ 'CHECKED']}/>
          </div>

          <div>
            <label for="$user_profile_createdat">{$user_profile[$user_profile_createdat][$user_profile_label_name]}<span style="color: red">*</span></label>
          </div>
          <div>
            <input disabled type="text" size="22" name="$user_profile_createdat" value="{$userinfo[$user_profile_createdat]}"  />
          </div>


          <div>
            <label for="$user_profile_updatedat">{$user_profile[$user_profile_updatedat][$user_profile_label_name]}<span style="color: red">*</span></label>
          </div>
          <div>
            <input disabled type="text" size="22" name="$user_profile_updatedat" value="{$userinfo[$user_profile_updatedat]}"  />
          </div>




        </div>

      <div style="margin: 0 auto; float: left; width: 100%;">
        <div class="btncontainer">

          <div>
            <p>
              <input type="button" name="change" value="Change" id="change1" class="btn" onclick="window.location.href = '?p=userupdate.php#update2'"/>
              <input type="submit" name="OK1" value="OK" id="OK1" class="btn rightbtn" />
            </p>
          </div>

        </div>
      </div>



      </form>

    </div>

  </div>
</div>
EOT;
?>