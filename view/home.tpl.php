<?php
/**
 * Home template with customized hero content that should be loaded before including this page
 *
 * @author    Thai Tran Ngoc Quoc
 * @copyright Copyright (c) 2018 - All rights reserved
 */

// ------------------------------------------------------------------------

// Setting up for mobile menu
require('mobilemenu.tpl.php');
// Setting up for desktop header 
require('desktopheader.tpl.php');
// Setting up for desktop menu
require('desktopmenu.tpl.php');
// Setting up for footer
require('footer.tpl.php');
// Setting up for the whole thing
require('main.tpl.php');
?>