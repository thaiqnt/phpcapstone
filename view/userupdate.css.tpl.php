<?php
$csscontent = <<<EOT
  <style>
    .text-center {
        text-align: center;
    } 
    .mainImg_area__imgBox .update {
        width: 770px;
        height: 450px;
        position: absolute;
        top: 50%; left: 50%;
        transform: translate(-50%,-50%);
        background: #fff;
        padding: 20px;
        box-shadow: 0 0 8px #555;
        -moz-box-shadow: 0 0 8px #555;
        -webkit-box-shadow: 0 0 8px #555;    
    }
    .contentOnMB .update {
        max-width: 100%;
        width: 100%;
        height: 1500px;
        background: #fff;
        padding: 20px;
    }
    .update form input {
        font-size: 16px;
        border: none;
        border-bottom: 1px solid #a1a1a1;
        border-radius: 0;
    }
   .update span.error {
        font-size: 12px;
        color: #a94442;
        display: block;
        margin-top: 2px;
    }

   .update span.error ul {
        list-style-type: circle;
    }

    .update h1.title {
        font-size: 20px;
        margin: 5px 0 15px;
        text-transform: uppercase;
    }
   .update .btn {
        border-radius: 0;
        -webkit-transition: all .3s;
        -o-transition: all .3s;
        transition: all .3s;
        text-transform: uppercase;
        font-size: 11px;
        font-weight: 500;
        padding-top: 10px;
        padding-bottom: 8px;
        color: #fff;
        background-color: #337ab7;
        border-color: #2e6da4;     
        white-space: nowrap;
        vertical-align: middle;
        width: 80px;
        margin: 0 auto; 
    }

    .container1,.container2,.container3,.container4,.container5,.container6,.container7 {
        float: left;
        margin-top: 20px;
        padding-right: 10px;
        padding-left: 10px;
    }

    .container1,.container2,.container3,.container4 {
        width: 22%;
    }

    .container5,.container6,.container7 {
        width: 30%;
    }

    .container1,.container2,.container3,.container5,.container6 {
        border-right: 1px solid #ccc;
    }

    .btncontainer {
        width: 200px; 
        margin: 20px auto;   
    }

    .contentOnMB .update div {
        float: left;
        margin-top: 20px;
        padding-right: 10px;
        padding-left: 10px;
        width: 100%;
    }
    
    .contentOnMB .update form input {
        width: 90%;
    }


  </style>
EOT;
?>