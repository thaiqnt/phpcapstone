<?php
$csscontent = <<<EOT
  <style>
    /* The Flash Message Modal (background) */

    .flashMessageModal {
      display: none;
      /* Hidden by default */
      position: fixed;
      /* Stay in place */
      z-index: 501;
      /* Sit on top */
      padding-top: 100px;
      /* Location of the box */
      left: 0;
      top: 0;
      width: 100%;
      /* Full width */
      height: 100%;
      /* Full height */
      overflow: auto;
      /* Enable scroll if needed */
      background-color: rgb(0, 0, 0);
      /* Fallback color */
      background-color: rgba(0, 0, 0, 0.4);
      /* Black w/ opacity */
    }

    /* Flash Message Content */

    .flashMessageContent {
      background-color: #fefefe;
      border: 1px solid #888;
      padding: 20px;
      width: 37%;
      height: auto;
      position: absolute;
      top: 37%; left: 50%;
      transform: translate(-50%,-50%);
      box-shadow: 0 0 8px #555;
      -moz-box-shadow: 0 0 8px #555;
      -webkit-box-shadow: 0 0 8px #555;    

    }

    /* The Close Button */

    .flashMessageClose {
      color: #aaaaaa;
      float: right;
      font-size: 28px;
      font-weight: bold;
    }

    .flashMessageClose:hover,
    .flashMessageClose:focus {
      color: #000;
      text-decoration: none;
      cursor: pointer;
    }
      


  </style>
EOT;
?>