<?php

$regioncontent = <<<EOT
      <!-- the treasure section is beginning with this vt-most-treasure -->
      <div class="vt-most-treasure" id="getCruise">
      </div>      
      <!-- Modal -->

          <div class="modal fade" id="cruisedetail" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  <h3 class="modal-title" id="myModalLabel">Tour Details</h3>
                  <div class="col-md-12 text-danger" id="cartmsg"></div>
                </div>
                <div class="modal-body" id='dynamic_content'>
                  ...
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

                </div>
              </div>
            </div>
          </div>

       <!-- Modal ends-->
      
      <script>
        function cruise() {
            $.ajax({
                url: "action.php",
                method: "POST",
                data: {getCruise:1},
                success: function(data){
                    $('#getCruise').html(data);
                }
            })
        }

        cruise();

        function updateAvailPax(cruiseid) {
            $.ajax({
                url: "action.php",
                method: "POST",
                data: {getAvailPax:1, tourid:cruiseid},
                success: function(data){
                    $('#availpax').val(data);
                    //console.log(data);
                }
            })
        }

        $('body').delegate('.doModal','click',function(event){
            event.preventDefault();
            var cruiseid=$(this).attr('data-id');
            $.ajax({
                url: 'action.php',
                method: 'POST',
                data: {tourDetail:1,tourid:cruiseid},
                success: function(data){
                    //console.log(data);
                    // Empty the cart message before showing anything
                    $('#cartmsg').html('');
                    $('#dynamic_content').html(data);
                    $('#cruisedetail').modal('show');
                }
            })
        })

        $('body').delegate('.addCart','click',function(){
            var cruiseid = $(this).attr('data-id');
            $.ajax({
                url: "action.php",
                method: "POST",
                data: {addCart:1,tourid:cruiseid,pax:$('#pax').val()},
                success: function(data){
                    $('#cartmsg').html(data);
                    // Update item numbers in the Cart
                    cartcount();
                    // Update available PAX in a tour detail
                    updateAvailPax(cruiseid);
                    //$('body').animate({scrollTop:0},500);
                }
            })
        })

      
      </script>

EOT;
?>