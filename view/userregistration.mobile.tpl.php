<?php
$regioncontent = <<<EOT
<div class="contentOnMB">
  <div>
    <div class="register">

      <h1 class="text-center title">Customer Registration</h1>

      <form action="index.php" method="post" id="registerNewCustomer2" autocomplete="off" novalidate>
      <input type="hidden" name="csrf" value="$csrfkey" />
      <input type="hidden" name="p" value="register.php" />

        <div>
            <span class="error">$user_registration_error_messages</span>
        </div>

        <div>
        <label for="$user_reg_loginname">{$user_reg[$user_reg_loginname][$user_reg_label_name]}<span style="color: red">*</span></label>
        </div>
        <div>
        <input type="text" size="33" name="$user_reg_loginname" value="{$user_reg[$user_reg_loginname][$user_reg_postedvalue]}" id="$user_reg_loginname" />
        </div>

        <div>
        <label for="$user_reg_password">{$user_reg[$user_reg_password][$user_reg_label_name]}<span style="color: red">*</span></label>
        </div>
        <div>
        <input type="password" size="33" name="$user_reg_password" value="{$user_reg[$user_reg_password][$user_reg_postedvalue]}" id="$user_reg_password" />
        </div>

        <div>
        <label for="$user_reg_repeatedpass">{$user_reg[$user_reg_repeatedpass][$user_reg_label_name]}<span style="color: red">*</span></label>
        </div>
        <div>
        <input type="password" size="33" name="$user_reg_repeatedpass" value="{$user_reg[$user_reg_repeatedpass][$user_reg_postedvalue]}" id="$user_reg_repeatedpass" />
        </div>

        <div>
        <label for="$user_reg_firstname">{$user_reg[$user_reg_firstname][$user_reg_label_name]}<span style="color: red">*</span></label>
        </div>
        <div>
        <input type="text" size="33" name="$user_reg_firstname" value="{$user_reg[$user_reg_firstname][$user_reg_postedvalue]}" id="$user_reg_firstname" />
        </div>

        <div>
        <label for="$user_reg_lastname">{$user_reg[$user_reg_lastname][$user_reg_label_name]}<span style="color: red">*</span></label>
        </div>
        <div>
        <input type="text" size="33" name="$user_reg_lastname" value="{$user_reg[$user_reg_lastname][$user_reg_postedvalue]}" id="$user_reg_lastname" />
        </div>

        <div>
        <label for="$user_reg_street">{$user_reg[$user_reg_street][$user_reg_label_name]}<span style="color: red">*</span></label>
        </div>
        <div>
        <input type="text" size="33" name="$user_reg_street" value="{$user_reg[$user_reg_street][$user_reg_postedvalue]}" id="$user_reg_street" />
        </div>

        <div>
        <label for="$user_reg_city">{$user_reg[$user_reg_city][$user_reg_label_name]}<span style="color: red">*</span></label>
        </div>
        <div>
        <input type="text" size="33" name="$user_reg_city" value="{$user_reg[$user_reg_city][$user_reg_postedvalue]}" id="$user_reg_city" />
        </div>

        <div>
        <label for="$user_reg_postalcode">{$user_reg[$user_reg_postalcode][$user_reg_label_name]}<span style="color: red">*</span></label>
        </div>
        <div>
        <input type="text" size="33" name="$user_reg_postalcode" value="{$user_reg[$user_reg_postalcode][$user_reg_postedvalue]}" id="$user_reg_postalcode" />
        </div>

        <div>
        <label for="$user_reg_province">{$user_reg[$user_reg_province][$user_reg_label_name]}<span style="color: red">*</span></label>
        </div>
        <div>
        <input type="text" size="33" name="$user_reg_province" value="{$user_reg[$user_reg_province][$user_reg_postedvalue]}" id="$user_reg_province" />
        </div>

        <div>
        <label for="$user_reg_country">{$user_reg[$user_reg_country][$user_reg_label_name]}<span style="color: red">*</span></label>
        </div>
        <div>
        <input type="text" size="33" name="$user_reg_country" value="{$user_reg[$user_reg_country][$user_reg_postedvalue]}" id="$user_reg_country" />
        </div>


        <div>
        <label for="$user_reg_phone">{$user_reg[$user_reg_phone][$user_reg_label_name]}<span style="color: red">*</span></label>
        </div>
        <div>
        <input type="text" size="33" name="$user_reg_phone" value="{$user_reg[$user_reg_phone][$user_reg_postedvalue]}" id="$user_reg_phone" />
        </div>

        <div>
        <label for="$user_reg_email">{$user_reg[$user_reg_email][$user_reg_label_name]}<span style="color: red">*</span></label>
        </div>
        <div>
        <input type="text" size="33" name="$user_reg_email" value="{$user_reg[$user_reg_email][$user_reg_postedvalue]}" id="$user_reg_email" />
        </div>

        <div>
        <label for="$user_reg_deleted">{$user_reg[$user_reg_deleted][$user_reg_label_name]}<span style="color: red">*</span></label>
        </div>
        <div>
        <input type="checkbox" name="$user_reg_deleted" value="1" id="$user_reg_deleted" {$user_reg[$user_reg_deleted][ 'CHECKED']}/>
        </div>

        <div>
          <input type="submit" name="register2" value="register" id="register2" class="btn" />
        </div>


      </form>

    </div>

  </div>
</div>
EOT;
?>