<?php

$adminlink = '';

// Enable Admin menu for admin user
if ($isAdminUser) {
  $adminlink = <<<EOT
                  <li class="nav_item nav_item-l5">
                    <div class="nav-target">
                      <ul class="clearfix">
                        <li class="list-1col clearfix">
                          <a href="?p=tourlist.php" class="title" title="Tour Management">Tour Management</a>
                          <ul class="link_list clearfix">
                            <li><a href="?p=adminstatistics.php" title="Administration Statistics">Administration Statistics</a></li>
                            <li><a href="?p=profile.php" title="Profile Management">Profile Management</a></li>
                          </ul>
                        </li>
                      </ul>
                    </div>
                    <div class="nav-trigger {$tourmenu['desktopadminactive']}">Admin</div>
                  </li>
EOT;
}


$desktopmenu = <<<EOT
      <!-- This menu is for desktop version only -->
      <div class="nav_block_sub contentOnPC">
        <nav class="nav_principalBar">
          <div class="nav-simple-inner clearfix">
            <div class="logo mt10"><a href="#" title="Home Page for Vietnam Tours"><img src="images/vt_logo.png" alt="Vietnam Tours" width="300" height="41"></a></div>
            <div class="nav_block">
              <ul class="nav_list clearfix">
                <li class="nav_item nav_item-l1">
                  <div class="nav-target">
                    <ul class="clearfix">
                      <li class="list-1col clearfix">
                        <a href="?p=treasure.php#getTour" class="title" title="Visit to the most favorite sites in the world.">Most Favorite Sites</a>
                        <ul class="link_list clearfix">
                          <li><a href="#">Asia</a></li>
                          <li><a href="#">Europe</a></li>
                          <li><a href="#">North America</a></li>
                        </ul>
                      </li>
                    </ul>
                  </div>
                  <div class="nav-trigger {$tourmenu['desktoptreasureactive']}">Treasures</div>
                </li>
                <li class="nav_item nav_item-l2">
                  <div class="nav-target">
                    <ul class="clearfix">
                      <li class="list-1col clearfix">
                        <a href="?p=seacruise.php#getCruise" class="title" title="Find a Sea Cruise Trip">Find a Sea Cruise Trip</a>
                        <ul class="link_list clearfix">
                          <li><a href="#">Royal Bermuda &amp; Boston</a></li>
                        </ul>
                      </li>
                    </ul>
                  </div>
                  <div class="nav-trigger {$tourmenu['desktopseacruiseactive']}">Sea Cruises</div>
                </li>
                <li class="nav_item nav_item-l3">
                  <div class="nav-target">
                    <ul class="clearfix">
                      <li class="list-1col clearfix">
                        <a href="?p=flight.php#flightcontent" class="title" title="International Flights">Intenational Flights</a>
                        <ul class="link_list clearfix">
                          <li><a href="#"></a></li>
                          <li><a href="#">Hotels</a></li>
                          <li><a href="#">Pubs</a></li>
                        </ul>
                        <a href="#" class="title">Domestic Flights</a>
                        <ul class="link_list clearfix">
                          <li><a href="#">Discount Tickets</a></li>
                          <li><a href="#">Online Reservation Sites</a></li>
                        </ul>
                      </li>
                    </ul>
                  </div>
                  <div class="nav-trigger {$tourmenu['desktopflightactive']}">Flight</div>
                </li>
                <li class="nav_item nav_item-l4">
                  <div class="nav-target">
                    <ul class="clearfix">
                      <li class="list-1col clearfix">
                        <a href="?p=aboutus.php#aboutus" class="title" title="Who we are ? What can we help you ?">Who We Are?</a>
                        <ul class="link_list clearfix">
                          <li><a href="#">FAQs</a></li>
                        </ul>
                      </li>
                    </ul>
                  </div>
                  <div class="nav-trigger {$tourmenu['desktopaboutusactive']}">About us</div>
                </li>
                $adminlink
              </ul>
              <!--/.nav_list-->
            </div>
            <!--/.nav_block-->
          </div>
        </nav>
      </div>
EOT;
?>