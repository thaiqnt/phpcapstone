<?php
$hero = <<<EOT
<div class="mainImg_area">
  <div class="mainImg_area__imgBox" style="background-image: url(images/tourbackground.jpg); ">
    <div class="update">

      <h1 class="text-center title">Customer Update</h1>

      <form action="index.php" method="post" id="updateCustomer1" autocomplete="off" novalidate>
      <input type="hidden" name="csrf" value="$csrfkey" />
      <input type="hidden" name="p" value="userupdate.php" />

        <div class="container1">
            <span class="error">$user_update_error_messages</span>
        </div>

        <div class="container2">

            <div>
            <label for="$user_update_loginname">{$user_update[$user_update_loginname][$user_update_label_name]}<span style="color: red">*</span></label>
            </div>
            <div>
            <input type="text" size="15" name="$user_update_loginname" value="{$user_update[$user_update_loginname][$user_update_postedvalue]}"  />
            </div>

            <div>
            <label for="$user_update_password">{$user_update[$user_update_password][$user_update_label_name]}<span style="color: red">*</span></label>
            </div>
            <div>
            <input type="password" size="15" name="$user_update_password" value="{$user_update[$user_update_password][$user_update_postedvalue]}"  />
            </div>

            <div>
            <label for="$user_update_repeatedpass">{$user_update[$user_update_repeatedpass][$user_update_label_name]}<span style="color: red">*</span></label>
            </div>
            <div>
            <input type="password" size="15" name="$user_update_repeatedpass" value="{$user_update[$user_update_repeatedpass][$user_update_postedvalue]}"  />
            </div>

            <div>
            <label for="$user_update_firstname">{$user_update[$user_update_firstname][$user_update_label_name]}<span style="color: red">*</span></label>
            </div>
            <div>
            <input type="text" size="15" name="$user_update_firstname" value="{$user_update[$user_update_firstname][$user_update_postedvalue]}"  />
            </div>

            <div>
            <label for="$user_update_lastname">{$user_update[$user_update_lastname][$user_update_label_name]}<span style="color: red">*</span></label>
            </div>
            <div>
            <input type="text" size="15" name="$user_update_lastname" value="{$user_update[$user_update_lastname][$user_update_postedvalue]}"  />
            </div>

        </div>

        <div class="container3">

            <div>
            <label for="$user_update_street">{$user_update[$user_update_street][$user_update_label_name]}<span style="color: red">*</span></label>
            </div>
            <div>
            <input type="text" size="15" name="$user_update_street" value="{$user_update[$user_update_street][$user_update_postedvalue]}"  />
            </div>

            <div>
            <label for="$user_update_city">{$user_update[$user_update_city][$user_update_label_name]}<span style="color: red">*</span></label>
            </div>
            <div>
            <input type="text" size="15" name="$user_update_city" value="{$user_update[$user_update_city][$user_update_postedvalue]}"  />
            </div>

            <div>
            <label for="$user_update_postalcode">{$user_update[$user_update_postalcode][$user_update_label_name]}<span style="color: red">*</span></label>
            </div>
            <div>
            <input type="text" size="15" name="$user_update_postalcode" value="{$user_update[$user_update_postalcode][$user_update_postedvalue]}"  />
            </div>

            <div>
            <label for="$user_update_province">{$user_update[$user_update_province][$user_update_label_name]}<span style="color: red">*</span></label>
            </div>
            <div>
            <input type="text" size="15" name="$user_update_province" value="{$user_update[$user_update_province][$user_update_postedvalue]}"  />
            </div>

            <div>
            <label for="$user_update_country">{$user_update[$user_update_country][$user_update_label_name]}<span style="color: red">*</span></label>
            </div>
            <div>
            <input type="text" size="15" name="$user_update_country" value="{$user_update[$user_update_country][$user_update_postedvalue]}"  />
            </div>



        </div>



        <div class="container4">


            <div>
            <label for="$user_update_phone">{$user_update[$user_update_phone][$user_update_label_name]}<span style="color: red">*</span></label>
            </div>
            <div>
            <input type="text" size="15" name="$user_update_phone" value="{$user_update[$user_update_phone][$user_update_postedvalue]}"  />
            </div>

            <div>
            <label for="$user_update_email">{$user_update[$user_update_email][$user_update_label_name]}<span style="color: red">*</span></label>
            </div>
            <div>
            <input type="text" size="15" name="$user_update_email" value="{$user_update[$user_update_email][$user_update_postedvalue]}"  />
            </div>

            <div>
            <label for="$user_update_deleted">{$user_update[$user_update_deleted][$user_update_label_name]}<span style="color: red">*</span></label>
            </div>
            <div>
            <input type="checkbox" name="$user_update_deleted" value="1"  {$user_update[$user_update_deleted][ 'CHECKED']}/>
            </div>

        
            <div class="btncontainer">

              <div>
                <input type="submit" name="update1" value="update" id="update1" class="btn" />&nbsp;
                <input type="button" name="back" value="back" class="btn" onclick="window.location.href = '?p=profile.php#profile2'"/>
              </div>

            </div>

        </div>




      </form>

    </div>

  </div>
</div>
EOT;
?>