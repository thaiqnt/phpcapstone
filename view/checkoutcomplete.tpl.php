<?php
$regioncontent = <<<EOT
	<!--Pre-loader -->
	<div class="checkoutprocess"><img src="images/checkoutcomplete.gif" style="width:400px;
      height: 400px;
      position: relative;
      top: 0px;
      left: 469px;">
    </div>

	
    <div id='checkoutcomplete'></div>
	<div class='container-fluid checkoutcomplete'>
		<div class='row'>
		  <div class='col-md-2'></div>
		  <div class='col-md-8'>
			<div class="panel panel-default">
  				<div class="panel-heading"><h1>Thank you!</h1></div>
  				<div class="panel-body">
    				Hello $username, the payment is proceeded SUCCESSFULLY.
    				<br>Your Payment Transaction ID is $tranid 
    				<br>Have a good day on shopping.
    				<p></p>
    				<a href="?p=invoice.php#invoicesection" class='btn btn-success btn-lg'>View my invoice</a>
                    &nbsp;
                    <a href="?p=index.php" class='btn btn-success btn-lg'>Back to shopping</a>
  				</div>
			</div>
		  <div class='col-md-2'></div>
	     </div>

    </div>

	<script type="text/javascript">
		
    	
    	$(".checkoutprocess").fadeOut(5000, function(){
          $(".checkoutcomplete").fadeIn(500);        	
		}); 

	</script>
EOT;
?>