<?php
$hero = <<<EOT
<div class="mainImg_area">
  <div class="mainImg_area__imgBox" style="background-image: url(images/tourbackground.jpg); ">
    <div class="update">

      <h1 class="text-center title">Your info has been updated!</h1>

      <form action="?p=index.php" method="get" id="updateComplete1" autocomplete="off" novalidate>

        <div class="container5">

          <div>
            <label for="$user_update_loginname">{$user_update[$user_update_loginname][$user_update_label_name]}<font color="red">*</font></label>
          </div>
          <div>
            <input disabled type="text" size="22" name="$user_update_loginname" value="{$user_update[$user_update_loginname][$user_update_postedvalue]}" id="$user_update_loginname" />
          </div>

          <div>
            <label for="$user_update_firstname">{$user_update[$user_update_firstname][$user_update_label_name]}<font color="red">*</font></label>
          </div>
          <div>
            <input disabled type="text" size="22" name="$user_update_firstname" value="{$user_update[$user_update_firstname][$user_update_postedvalue]}" id="$user_update_firstname" />
          </div>

          <div>
            <label for="$user_update_lastname">{$user_update[$user_update_lastname][$user_update_label_name]}<font color="red">*</font></label>
          </div>
          <div>
            <input disabled type="text" size="22" name="$user_update_lastname" value="{$user_update[$user_update_lastname][$user_update_postedvalue]}" id="$user_update_lastname" />
          </div>

        </div>
        <div class="container6">

          <div>
            <label for="$user_update_street">{$user_update[$user_update_street][$user_update_label_name]}<font color="red">*</font></label>
          </div>
          <div>
            <input disabled type="text" size="22" name="$user_update_street" value="{$user_update[$user_update_street][$user_update_postedvalue]}" id="$user_update_street" />
          </div>

          <div>
            <label for="$user_update_city">{$user_update[$user_update_city][$user_update_label_name]}<font color="red">*</font></label>
          </div>
          <div>
            <input disabled type="text" size="22" name="$user_update_city" value="{$user_update[$user_update_city][$user_update_postedvalue]}" id="$user_update_city" />
          </div>

          <div>
            <label for="$user_update_postalcode">{$user_update[$user_update_postalcode][$user_update_label_name]}<font color="red">*</font></label>
          </div>
          <div>
            <input disabled type="text" size="22" name="$user_update_postalcode" value="{$user_update[$user_update_postalcode][$user_update_postedvalue]}" id="$user_update_postalcode" />
          </div>

          <div>
            <label for="$user_update_province">{$user_update[$user_update_province][$user_update_label_name]}<font color="red">*</font></label>
          </div>
          <div>
            <input disabled type="text" size="22" name="$user_update_province" value="{$user_update[$user_update_province][$user_update_postedvalue]}" id="$user_update_province" />
          </div>

          <div>
            <label for="$user_update_country">{$user_update[$user_update_country][$user_update_label_name]}<font color="red">*</font></label>
          </div>
          <div>
            <input disabled type="text" size="22" name="$user_update_country" value="{$user_update[$user_update_country][$user_update_postedvalue]}" id="$user_update_country" />
          </div>



        </div>
        <div class="container7">


          <div>
            <label for="$user_update_phone">{$user_update[$user_update_phone][$user_update_label_name]}<font color="red">*</font></label>
          </div>
          <div>
            <input disabled type="text" size="22" name="$user_update_phone" value="{$user_update[$user_update_phone][$user_update_postedvalue]}" id="$user_update_phone" />
          </div>

          <div>
            <label for="$user_update_email">{$user_update[$user_update_email][$user_update_label_name]}<font color="red">*</font></label>
          </div>
          <div>
            <input disabled type="text" size="22" name="$user_update_email" value="{$user_update[$user_update_email][$user_update_postedvalue]}" id="$user_update_email" />
          </div>

          <div>
            <label for="$user_update_deleted">{$user_update[$user_update_deleted][$user_update_label_name]}<font color="red">*</font></label>
          </div>
          <div>
            <input disabled type="checkbox" name="$user_update_deleted" value="1" id="$user_update_deleted" {$user_update[$user_update_deleted][ 'CHECKED']}/>
          </div>

          <div>
            <label for="$user_update_createdat">{$user_update[$user_update_createdat][$user_update_label_name]}<font color="red">*</font></label>
          </div>
          <div>
            <input disabled type="text" size="22" name="$user_update_createdat" value="{$user_update[$user_update_createdat][$user_update_postedvalue]}" id="$user_update_createdat" />
          </div>


          <div>
            <label for="$user_update_updatedat">{$user_update[$user_update_updatedat][$user_update_label_name]}<font color="red">*</font></label>
          </div>
          <div>
            <input disabled type="text" size="22" name="$user_update_updatedat" value="{$user_update[$user_update_updatedat][$user_update_postedvalue]}" id="$user_update_updatedat" />
          </div>




        </div>

      <div style="margin: 0 auto; float: left; width: 100%;">
        <div class="btncontainer">

          <div>
            <input type="button" name="back" value="back" class="btn" onclick="window.location.href = '?p=profile.php#profile2'"/>
          </div>

        </div>
      </div>



      </form>

    </div>

  </div>
</div>
EOT;
?>