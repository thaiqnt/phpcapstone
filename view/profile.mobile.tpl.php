<?php
$regioncontent = <<<EOT
<div class="contentOnMB">
  <div>
    <div class="profile">

      <h1 class="text-center title">Customer Profile</h1>
      <h2 class="text-center title">Your information that is being stored in our system</h2>

      <form action="index.php" method="get" id="profile2" autocomplete="off" novalidate>

        <div>
          <label for="$user_profile_loginname">{$user_profile[$user_profile_loginname][$user_profile_label_name]}<span style="color: red">*</span></label>
        </div>
        <div>
          <input type="hidden" name="p" value="index.php"></input>
          <input disabled type="text" size="33" name="$user_profile_loginname" value="{$userinfo[$user_profile_loginname]}" id="$user_profile_loginname" />
        </div>

        <div>
          <label for="$user_profile_firstname">{$user_profile[$user_profile_firstname][$user_profile_label_name]}<span style="color: red">*</span></label>
        </div>
        <div>
          <input disabled type="text" size="33" name="$user_profile_firstname" value="{$userinfo[$user_profile_firstname]}" id="$user_profile_firstname" />
        </div>

        <div>
          <label for="$user_profile_lastname">{$user_profile[$user_profile_lastname][$user_profile_label_name]}<span style="color: red">*</span></label>
        </div>
        <div>
          <input disabled type="text" size="33" name="$user_profile_lastname" value="{$userinfo[$user_profile_lastname]}" id="$user_profile_lastname" />
        </div>

        <div>
          <label for="$user_profile_street">{$user_profile[$user_profile_street][$user_profile_label_name]}<span style="color: red">*</span></label>
        </div>
        <div>
          <input disabled type="text" size="33" name="$user_profile_street" value="{$userinfo[$user_profile_street]}" id="$user_profile_street" />
        </div>

        <div>
          <label for="$user_profile_city">{$user_profile[$user_profile_city][$user_profile_label_name]}<span style="color: red">*</span></label>
        </div>
        <div>
          <input disabled type="text" size="33" name="$user_profile_city" value="{$userinfo[$user_profile_city]}" id="$user_profile_city" />
        </div>

        <div>
          <label for="$user_profile_postalcode">{$user_profile[$user_profile_postalcode][$user_profile_label_name]}<span style="color: red">*</span></label>
        </div>
        <div>
          <input disabled type="text" size="33" name="$user_profile_postalcode" value="{$userinfo[$user_profile_postalcode]}" id="$user_profile_postalcode" />
        </div>

        <div>
          <label for="$user_profile_province">{$user_profile[$user_profile_province][$user_profile_label_name]}<span style="color: red">*</span></label>
        </div>
        <div>
          <input disabled type="text" size="33" name="$user_profile_province" value="{$userinfo[$user_profile_province]}" id="$user_profile_province" />
        </div>

        <div>
          <label for="$user_profile_country">{$user_profile[$user_profile_country][$user_profile_label_name]}<span style="color: red">*</span></label>
        </div>
        <div>
          <input disabled type="text" size="33" name="$user_profile_country" value="{$userinfo[$user_profile_country]}" id="$user_profile_country" />
        </div>

        <div>
          <label for="$user_profile_phone">{$user_profile[$user_profile_phone][$user_profile_label_name]}<span style="color: red">*</span></label>
        </div>
        <div>
          <input disabled type="text" size="33" name="$user_profile_phone" value="{$userinfo[$user_profile_phone]}" id="$user_profile_phone" />
        </div>

        <div>
          <label for="$user_profile_email">{$user_profile[$user_profile_email][$user_profile_label_name]}<span style="color: red">*</span></label>
        </div>
        <div>
          <input disabled type="text" size="33" name="$user_profile_email" value="{$userinfo[$user_profile_email]}" id="$user_profile_email" />
        </div>

        <div>
          <label for="$user_profile_deleted">{$user_profile[$user_profile_deleted][$user_profile_label_name]}<span style="color: red">*</span></label>
        </div>
        <div>
          <input disabled type="checkbox" name="$user_profile_deleted" value="1" id="$user_profile_deleted" {$user_profile[$user_profile_deleted][ 'CHECKED']}/>
        </div>

        <div>
          <label for="$user_profile_createdat">{$user_profile[$user_profile_createdat][$user_profile_label_name]}<span style="color: red">*</span></label>
        </div>
        <div>
          <input disabled type="text" size="33" name="$user_profile_createdat" value="{$userinfo[$user_profile_createdat]}" id="$user_profile_createdat" />
        </div>


        <div>
          <label for="$user_profile_updatedat">{$user_profile[$user_profile_updatedat][$user_profile_label_name]}<span style="color: red">*</span></label>
        </div>
        <div>
          <input disabled type="text" size="33" name="$user_profile_updatedat" value="{$userinfo[$user_profile_updatedat]}" id="$user_profile_updatedat" />
        </div>


        <div>
          <p>
            <input type="button" name="change" value="Change" id="change2" class="btn" onclick="window.location.href = '?p=userupdate.php#updateCustomer2'"/>
            <input type="submit" name="OK2" value="OK" id="OK2" class="btn" />
          </p>
        </div>




      </form>

    </div>

  </div>
</div>
EOT;
?>