<?php
$msg = '';

if (count($generalinfo)>0) {
  $msg .= "<div>Maximum payment is now <b>{$generalinfo['paymentmax']}</b></div> ".
          "<div>Minimum payment is now <b>{$generalinfo['paymentmin']}</b></div> ".
          "<div>Payment in average is now <b>{$generalinfo['paymentavg']}</b><div> ".
          "<br/>";
}

if (count($detailinfo)>0) {
  $msg .= "<div><b>{$detailinfo['firstname']} {$detailinfo['lastname']} ({$detailinfo['loginname']})</b> booked a LUXURY tour ".
          "<b>(\${$detailinfo['mosttourexp']})</b></div> ".
          "<div><strong style=\\\"font-size: 32px; font-weight: bold; color: red;\\\">{$detailinfo['title']}</strong> </div> ".
          "<div>with price <b>\${$detailinfo['price']}</b> for <b>{$detailinfo['pax']}</b> guy(s)</div>";
}


$regioncontent = <<<EOT
<div id="flashMessageBox" class="flashMessageModal">

  <!-- Show tracking content in modal mode -->
  <div class="flashMessageContent">
    <span class="flashMessageClose">&times;</span>
    <div id="flashMessageDetail"></div>
  </div>
</div>
<script>
    // We show user info that was kept in the session
    function showStatisticsInfo(str) {
      // Get the cart modal
      var infoBox = document.getElementById('flashMessageBox');

      // Get the <span> element that closes the modal
      var span = document.getElementsByClassName("flashMessageClose")[0];

      // When the user clicks on <span> (x), close the modal
      span.onclick = function() {
        infoBox.style.display = "none";
        //window.location = 'index.php?p=profile.php';
      }
      
      // When the user clicks anywhere outside of the modal, close it
      window.onclick = function(event) {
        if (event.target == infoBox) {
          infoBox.style.display = "none";
          //window.location = 'index.php?p=profile.php';
        }
      }

      infoDetail = document.getElementById("flashMessageDetail");
      if (infoDetail) {
        infoDetail.innerHTML = str;
      }

      // Display the infobox for user
      infoBox.style.display = "block";
    }
    
    function showAdminStatistics() {
      var str = "<div><strong style=\"font-size: 32px; font-weight: bold;\">Good day, </strong> <strong style=\"font-size: 32px; font-weight: bold; color: red;\">Admin</strong></div><div>$msg</div>";
      
      showStatisticsInfo(str);

    }
showAdminStatistics();
    
</script>
EOT;

/*

*/
?>