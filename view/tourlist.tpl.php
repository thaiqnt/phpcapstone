<?php

$hero = <<<EOT
    <div id="flashMessageBox" class="flashMessageModal">

      <!-- Show tracking content in modal mode -->
      <div class="flashMessageContent">
        <span class="flashMessageClose">&times;</span>
        <div id="flashMessageDetail"></div>
      </div>
    </div>
	<div class="container-fluid">
		<div class="row">
			<div class="row"></div>
			<div>
                <div class="panel panel-primary text-center">
                    <div class="panel-heading">Tour Search</div>
                    <div class="panel-body"></div>
                    <form action="?p=tourlist.php" method="post" autocomplete="off" novalidate>
                    <div class="row">
                        <div class="col-md-1"></div>
                        <div class="col-md-1"><b>Category</b></div>
                        <div class="col-md-2" id="categorylist">
                        </div>
                        <div class="col-md-2"><b>Title or Description</b></div>
                        <div class="col-md-3"><input id='keyword' class='form-control' type='text'/>
                        </div>
                        <div class="col-md-2"><a href="javascript:fillTours($('#categoryid').val())" class="btn btn-success"><span class="glyphicon glyphicon-search"></span></a>&nbsp;<a href="#" class="btn btn-success"><span class="glyphicon glyphicon-refresh" onclick="$('#keyword').val('');"></span></a>&nbsp;<a href="?p=tourdetail.php&id=new" class="btn btn-success"><span class="glyphicon glyphicon-plus"></span></a>&nbsp;</div>
                        <div class="col-md-1"></div>
                    </div>
                    </form>
                    <br/>
                    <div class="panel-footer">

                    </div>
                </div>
            </div>
			<div>
				<div class="panel panel-primary text-center">
					<div class="panel-heading">Tour List</div>
					<div class="panel-body"><div class="col-md-12 alert text-success" id="tourmsg"></div></div>
					<div class="row" id="tourheader">
                        <div class="col-md-1"><b></b></div>
						<div class="col-md-1"><b>Image</b></div>
						<div class="col-md-2"><b>Title</b></div>
                        <div class="col-md-2"><b>Start Date</b></div>
						<div class="col-md-1"><b>Dura-tion</b></div>
						<div class="col-md-1"><b>Price</b></div>
						<div class="col-md-1"><b>Max Pax</b></div>
						<div class="col-md-1"><b>Avail Pax</b></div>
                        <div class="col-md-2"><b>Action</b></div>
					</div>
					<br>
                    <div class="row" id="tourlist">
					<div class="panel-footer">

					</div>
				</div>
			</div>
			<div class="col-md-2"></div>
		</div>
	</div>
    <script>
      // We show action result
      function showActionResult(str) {
        // Get the cart modal
        var infoBox = document.getElementById('flashMessageBox');

        // Get the <span> element that closes the modal
        var span = document.getElementsByClassName("flashMessageClose")[0];

        // When the user clicks on <span> (x), close the modal
        span.onclick = function() {
          infoBox.style.display = "none";
        }

        // When the user clicks anywhere outside of the modal, close it
        window.onclick = function(event) {
          if (event.target == infoBox) {
            infoBox.style.display = "none";
          }
        }

        infoDetail = document.getElementById("flashMessageDetail");
        if (infoDetail) {
          infoDetail.innerHTML = str;
        }

        // Display the infobox for user
        infoBox.style.display = "block";
      }

      function showResult(str) {
        var str = "<div><strong style=\"font-size: 32px; font-weight: bold; color: red;\">Well done!</strong></div>"+str;

        showActionResult(str);

      }

      function fillCategories(){
          $.ajax({
              url: 'admin.php',
              method: 'POST',
              data: {getCategory:1},
              success: function(data){
                // Fill the categories
                $('#categorylist').html(data);
                //console.log(data);
                
                // Reset the selected category after the filling
                var result = document.URL.match(/catid=([0-9]+)/);
                var catid = (!result) ? 1 : result[1];
                document.getElementById("categoryid").value = catid; 

              
              }
          })
      }
      fillCategories();

      function fillTours(categoryid){
          $.ajax({
              url: 'admin.php',
              method: 'POST',
              data: {getTourList:1,categoryid:categoryid,keyword:$('#keyword').val()},
              success: function(data){
                // Error is found?
                if (data.indexOf('<div') < 0) {
                  $('#tourheader').hide();
                  $('#tourlist').html('');
                  
                  // Put message in the cart message
                  $('#tourmsg').html(data);
                  //console.log(data);
                } else {
                  // Everything is OK
                  $('#tourmsg').html('');
                  $('#tourlist').html(data);
                }
              }
          })
          
      }
      
      var result = document.URL.match(/catid=([0-9]+)/);
      var catid = (!result) ? 1 : result[1];
      
      // First, load default category
      fillTours(catid);
      
      function removeTour(tourid, title) {
          $.ajax({
              url: 'admin.php',
              method: 'POST',
              data: {removeTour:1,
                tourid:tourid,
                categoryid:$('#categoryid').val(),
                keyword:$('#keyword').val()},
              success: function(data){
                  // Error is found?
                  if (data.indexOf('<div') < 0) {
                    $('#tourheader').hide();
                    $('#tourlist').html('');

                    // Put message in the cart message
                    $('#tourmsg').html(data);
                    //console.log(data);
                  } else {
                    // Everything is OK
                    str = 'The tour ' + title + ' has been DELETED.';
                    $('#tourmsg').html(str);
                    $('#tourlist').html(data);

                    // Pop up the flash message
                    showResult(str);
                  }
              }
          })
      }




    </script>
EOT;
?>