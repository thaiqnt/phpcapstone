<?php
$hero = <<<EOT
<div class="mainImg_area">
  <div class="mainImg_area__imgBox" style="background-image: url(images/tourbackground.jpg); ">
    <div class="register">

      <h1 class="text-center title">Customer Registration</h1>

      <form action="index.php" method="post" id="registerNewCustomer1" autocomplete="off" novalidate>
      <input type="hidden" name="csrf" value="$csrfkey" />
      <input type="hidden" name="p" value="register.php" />

        <div class="container1">
            <span class="error">$user_registration_error_messages</span>
        </div>

        <div class="container2">

            <div>
            <label for="$user_reg_loginname">{$user_reg[$user_reg_loginname][$user_reg_label_name]}<span style="color: red">*</span></label>
            </div>
            <div>
            <input type="text" size="15" name="$user_reg_loginname" value="{$user_reg[$user_reg_loginname][$user_reg_postedvalue]}"  />
            </div>

            <div>
            <label for="$user_reg_password">{$user_reg[$user_reg_password][$user_reg_label_name]}<span style="color: red">*</span></label>
            </div>
            <div>
            <input type="password" size="15" name="$user_reg_password" value="{$user_reg[$user_reg_password][$user_reg_postedvalue]}"  />
            </div>

            <div>
            <label for="$user_reg_repeatedpass">{$user_reg[$user_reg_repeatedpass][$user_reg_label_name]}<span style="color: red">*</span></label>
            </div>
            <div>
            <input type="password" size="15" name="$user_reg_repeatedpass" value="{$user_reg[$user_reg_repeatedpass][$user_reg_postedvalue]}"  />
            </div>

            <div>
            <label for="$user_reg_firstname">{$user_reg[$user_reg_firstname][$user_reg_label_name]}<span style="color: red">*</span></label>
            </div>
            <div>
            <input type="text" size="15" name="$user_reg_firstname" value="{$user_reg[$user_reg_firstname][$user_reg_postedvalue]}"  />
            </div>

            <div>
            <label for="$user_reg_lastname">{$user_reg[$user_reg_lastname][$user_reg_label_name]}<span style="color: red">*</span></label>
            </div>
            <div>
            <input type="text" size="15" name="$user_reg_lastname" value="{$user_reg[$user_reg_lastname][$user_reg_postedvalue]}"  />
            </div>

        </div>

        <div class="container3">

            <div>
            <label for="$user_reg_street">{$user_reg[$user_reg_street][$user_reg_label_name]}<span style="color: red">*</span></label>
            </div>
            <div>
            <input type="text" size="15" name="$user_reg_street" value="{$user_reg[$user_reg_street][$user_reg_postedvalue]}"  />
            </div>

            <div>
            <label for="$user_reg_city">{$user_reg[$user_reg_city][$user_reg_label_name]}<span style="color: red">*</span></label>
            </div>
            <div>
            <input type="text" size="15" name="$user_reg_city" value="{$user_reg[$user_reg_city][$user_reg_postedvalue]}"  />
            </div>

            <div>
            <label for="$user_reg_postalcode">{$user_reg[$user_reg_postalcode][$user_reg_label_name]}<span style="color: red">*</span></label>
            </div>
            <div>
            <input type="text" size="15" name="$user_reg_postalcode" value="{$user_reg[$user_reg_postalcode][$user_reg_postedvalue]}"  />
            </div>

            <div>
            <label for="$user_reg_province">{$user_reg[$user_reg_province][$user_reg_label_name]}<span style="color: red">*</span></label>
            </div>
            <div>
            <input type="text" size="15" name="$user_reg_province" value="{$user_reg[$user_reg_province][$user_reg_postedvalue]}"  />
            </div>

            <div>
            <label for="$user_reg_country">{$user_reg[$user_reg_country][$user_reg_label_name]}<span style="color: red">*</span></label>
            </div>
            <div>
            <input type="text" size="15" name="$user_reg_country" value="{$user_reg[$user_reg_country][$user_reg_postedvalue]}"  />
            </div>



        </div>



        <div class="container4">


            <div>
            <label for="$user_reg_phone">{$user_reg[$user_reg_phone][$user_reg_label_name]}<span style="color: red">*</span></label>
            </div>
            <div>
            <input type="text" size="15" name="$user_reg_phone" value="{$user_reg[$user_reg_phone][$user_reg_postedvalue]}"  />
            </div>

            <div>
            <label for="$user_reg_email">{$user_reg[$user_reg_email][$user_reg_label_name]}<span style="color: red">*</span></label>
            </div>
            <div>
            <input type="text" size="15" name="$user_reg_email" value="{$user_reg[$user_reg_email][$user_reg_postedvalue]}"  />
            </div>

            <div>
            <label for="$user_reg_deleted">{$user_reg[$user_reg_deleted][$user_reg_label_name]}<span style="color: red">*</span></label>
            </div>
            <div>
            <input type="checkbox" name="$user_reg_deleted" value="1"  {$user_reg[$user_reg_deleted][ 'CHECKED']}/>
            </div>

        
            <div class="btncontainer">

              <div>
                <input type="submit" name="register1" value="register" id="register1" class="btn" />
              </div>

            </div>

        </div>




      </form>

    </div>

  </div>
</div>
EOT;
?>