<?php
$regioncontent = <<<EOT
<div class="contentOnMB">
  <div>
    <div class="register">

      <h1 class="text-center title">Thank you for registering</h1>
      <h2 class="text-center title">You submitted the following information</h2>

      <form action="?p=index.php" method="get" id="registerComplete2" autocomplete="off" novalidate>

        <div>
          <label for="$user_reg_loginname">{$user_reg[$user_reg_loginname][$user_reg_label_name]}<span style="color: red">*</span></label>
        </div>
        <div>
          <input disabled type="text" size="33" name="$user_reg_loginname" value="{$regcompleteresult[$user_reg_loginname]}" id="$user_reg_loginname" />
        </div>

        <div>
          <label for="$user_reg_firstname">{$user_reg[$user_reg_firstname][$user_reg_label_name]}<span style="color: red">*</span></label>
        </div>
        <div>
          <input disabled type="text" size="33" name="$user_reg_firstname" value="{$regcompleteresult[$user_reg_firstname]}" id="$user_reg_firstname" />
        </div>

        <div>
          <label for="$user_reg_lastname">{$user_reg[$user_reg_lastname][$user_reg_label_name]}<span style="color: red">*</span></label>
        </div>
        <div>
          <input disabled type="text" size="33" name="$user_reg_lastname" value="{$regcompleteresult[$user_reg_lastname]}" id="$user_reg_lastname" />
        </div>

        <div>
          <label for="$user_reg_street">{$user_reg[$user_reg_street][$user_reg_label_name]}<span style="color: red">*</span></label>
        </div>
        <div>
          <input disabled type="text" size="33" name="$user_reg_street" value="{$regcompleteresult[$user_reg_street]}" id="$user_reg_street" />
        </div>

        <div>
          <label for="$user_reg_city">{$user_reg[$user_reg_city][$user_reg_label_name]}<span style="color: red">*</span></label>
        </div>
        <div>
          <input disabled type="text" size="33" name="$user_reg_city" value="{$regcompleteresult[$user_reg_city]}" id="$user_reg_city" />
        </div>

        <div>
          <label for="$user_reg_postalcode">{$user_reg[$user_reg_postalcode][$user_reg_label_name]}<span style="color: red">*</span></label>
        </div>
        <div>
          <input disabled type="text" size="33" name="$user_reg_postalcode" value="{$regcompleteresult[$user_reg_postalcode]}" id="$user_reg_postalcode" />
        </div>

        <div>
          <label for="$user_reg_province">{$user_reg[$user_reg_province][$user_reg_label_name]}<span style="color: red">*</span></label>
        </div>
        <div>
          <input disabled type="text" size="33" name="$user_reg_province" value="{$regcompleteresult[$user_reg_province]}" id="$user_reg_province" />
        </div>

        <div>
          <label for="$user_reg_country">{$user_reg[$user_reg_country][$user_reg_label_name]}<span style="color: red">*</span></label>
        </div>
        <div>
          <input disabled type="text" size="33" name="$user_reg_country" value="{$regcompleteresult[$user_reg_country]}" id="$user_reg_country" />
        </div>

        <div>
          <label for="$user_reg_phone">{$user_reg[$user_reg_phone][$user_reg_label_name]}<span style="color: red">*</span></label>
        </div>
        <div>
          <input disabled type="text" size="33" name="$user_reg_phone" value="{$regcompleteresult[$user_reg_phone]}" id="$user_reg_phone" />
        </div>

        <div>
          <label for="$user_reg_email">{$user_reg[$user_reg_email][$user_reg_label_name]}<span style="color: red">*</span></label>
        </div>
        <div>
          <input disabled type="text" size="33" name="$user_reg_email" value="{$regcompleteresult[$user_reg_email]}" id="$user_reg_email" />
        </div>

        <div>
          <label for="$user_reg_deleted">{$user_reg[$user_reg_deleted][$user_reg_label_name]}<span style="color: red">*</span></label>
        </div>
        <div>
          <input disabled type="checkbox" name="$user_reg_deleted" value="1" id="$user_reg_deleted" {$user_reg[$user_reg_deleted][ 'CHECKED']}/>
        </div>

        <div>
          <label for="$user_reg_createdat">{$user_reg[$user_reg_createdat][$user_reg_label_name]}<span style="color: red">*</span></label>
        </div>
        <div>
          <input disabled type="text" size="33" name="$user_reg_createdat" value="{$regcompleteresult[$user_reg_createdat]}" id="$user_reg_createdat" />
        </div>


        <div>
          <label for="$user_reg_updatedat">{$user_reg[$user_reg_updatedat][$user_reg_label_name]}<span style="color: red">*</span></label>
        </div>
        <div>
          <input disabled type="text" size="33" name="$user_reg_updatedat" value="{$regcompleteresult[$user_reg_updatedat]}" id="$user_reg_updatedat" />
        </div>


        <div>
          <input type="submit" name="back2" value="back" id="back2" class="btn" />
        </div>



      </form>

    </div>

  </div>
</div>
EOT;
?>