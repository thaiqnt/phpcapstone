<?php
$regioncontent = <<<EOT
<div class="contentOnMB">
  <div>
    <div class="update">

      <h1 class="text-center title">Customer Update</h1>

      <form action="index.php" method="post" id="updateCustomer2" autocomplete="off" novalidate>
      <input type="hidden" name="csrf" value="$csrfkey" />
      <input type="hidden" name="p" value="userupdate.php" />

        <div>
            <span class="error">$user_update_error_messages</span>
        </div>

        <div>
        <label for="$user_update_loginname">{$user_update[$user_update_loginname][$user_update_label_name]}<span style="color: red">*</span></label>
        </div>
        <div>
        <input type="text" size="33" name="$user_update_loginname" value="{$user_update[$user_update_loginname][$user_update_postedvalue]}" id="$user_update_loginname" />
        </div>

        <div>
        <label for="$user_update_password">{$user_update[$user_update_password][$user_update_label_name]}<span style="color: red">*</span></label>
        </div>
        <div>
        <input type="password" size="33" name="$user_update_password" value="{$user_update[$user_update_password][$user_update_postedvalue]}" id="$user_update_password" />
        </div>

        <div>
        <label for="$user_update_repeatedpass">{$user_update[$user_update_repeatedpass][$user_update_label_name]}<span style="color: red">*</span></label>
        </div>
        <div>
        <input type="password" size="33" name="$user_update_repeatedpass" value="{$user_update[$user_update_repeatedpass][$user_update_postedvalue]}" id="$user_update_repeatedpass" />
        </div>

        <div>
        <label for="$user_update_firstname">{$user_update[$user_update_firstname][$user_update_label_name]}<span style="color: red">*</span></label>
        </div>
        <div>
        <input type="text" size="33" name="$user_update_firstname" value="{$user_update[$user_update_firstname][$user_update_postedvalue]}" id="$user_update_firstname" />
        </div>

        <div>
        <label for="$user_update_lastname">{$user_update[$user_update_lastname][$user_update_label_name]}<span style="color: red">*</span></label>
        </div>
        <div>
        <input type="text" size="33" name="$user_update_lastname" value="{$user_update[$user_update_lastname][$user_update_postedvalue]}" id="$user_update_lastname" />
        </div>

        <div>
        <label for="$user_update_street">{$user_update[$user_update_street][$user_update_label_name]}<span style="color: red">*</span></label>
        </div>
        <div>
        <input type="text" size="33" name="$user_update_street" value="{$user_update[$user_update_street][$user_update_postedvalue]}" id="$user_update_street" />
        </div>

        <div>
        <label for="$user_update_city">{$user_update[$user_update_city][$user_update_label_name]}<span style="color: red">*</span></label>
        </div>
        <div>
        <input type="text" size="33" name="$user_update_city" value="{$user_update[$user_update_city][$user_update_postedvalue]}" id="$user_update_city" />
        </div>

        <div>
        <label for="$user_update_postalcode">{$user_update[$user_update_postalcode][$user_update_label_name]}<span style="color: red">*</span></label>
        </div>
        <div>
        <input type="text" size="33" name="$user_update_postalcode" value="{$user_update[$user_update_postalcode][$user_update_postedvalue]}" id="$user_update_postalcode" />
        </div>

        <div>
        <label for="$user_update_province">{$user_update[$user_update_province][$user_update_label_name]}<span style="color: red">*</span></label>
        </div>
        <div>
        <input type="text" size="33" name="$user_update_province" value="{$user_update[$user_update_province][$user_update_postedvalue]}" id="$user_update_province" />
        </div>

        <div>
        <label for="$user_update_country">{$user_update[$user_update_country][$user_update_label_name]}<span style="color: red">*</span></label>
        </div>
        <div>
        <input type="text" size="33" name="$user_update_country" value="{$user_update[$user_update_country][$user_update_postedvalue]}" id="$user_update_country" />
        </div>


        <div>
        <label for="$user_update_phone">{$user_update[$user_update_phone][$user_update_label_name]}<span style="color: red">*</span></label>
        </div>
        <div>
        <input type="text" size="33" name="$user_update_phone" value="{$user_update[$user_update_phone][$user_update_postedvalue]}" id="$user_update_phone" />
        </div>

        <div>
        <label for="$user_update_email">{$user_update[$user_update_email][$user_update_label_name]}<span style="color: red">*</span></label>
        </div>
        <div>
        <input type="text" size="33" name="$user_update_email" value="{$user_update[$user_update_email][$user_update_postedvalue]}" id="$user_update_email" />
        </div>

        <div>
        <label for="$user_update_deleted">{$user_update[$user_update_deleted][$user_update_label_name]}<span style="color: red">*</span></label>
        </div>
        <div>
        <input type="checkbox" name="$user_update_deleted" value="1" id="$user_update_deleted" {$user_update[$user_update_deleted][ 'CHECKED']}/>
        </div>

        <div>
          <input type="submit" name="update2" value="update" id="update2" class="btn" />
          <input type="button" name="back" value="back" class="btn" onclick="window.location.href = '?p=profile.php#profile2'"/>
        </div>


      </form>

    </div>

  </div>
</div>
EOT;
?>