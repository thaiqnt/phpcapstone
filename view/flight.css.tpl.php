<?php
$csscontent = <<<EOT
  <style>
    /* Setting for flight container box */

    .flight-container {
      margin-right: auto;
      margin-left: auto;
      padding-left: 15px;
      padding-right: 15px;
      font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
      font-size: 14px;
      line-height: 1.42857143;
      position: relative;
    }

    .flight-container:before,
    .flight-container:after {
      content: " ";
      display: table;
    }

    .flight-container:after {
      clear: both;
    }

    /* Navigation of the container */

    .nav {
      margin-bottom: 0;
      padding-left: 0;
      list-style: none;
    }

    .nav-tabs {
      border-bottom: 1px solid #ddd;
    }

    .nav>li {
      position: relative;
      display: block;
    }

    .nav-tabs>li {
      float: left;
      margin-bottom: -1px;
    }

    .nav>li {
      padding-right: 5px;
      margin-top: 5px;
    }

    .nav>li {
      padding-right: 0;
    }

    .nav>li>a,
    .nav>li,
    .nav>li.flight-hightlight>div,
    .nav>li.flight-hightlight {
      background-color: #ffffff;
      color: #000000;
      border: 0;
      margin-right: 0px;

    }

    .nav>li>div {
      position: relative;
      display: block;
      padding: 10px 15px;
    }

    .nav-tabs>li>div {
      margin-right: 2px;
      line-height: 1.42857143;
      border: 1px solid transparent;
      border-radius: 4px 4px 0 0;
    }

    .nav>li>div {
      position: relative;
      display: block;
      padding: 10px 15px;
      background-color: rgba(107, 107, 107, 0.54);
      text-align: center;
      color: white;
    }

    .nav-tabs>li.flight-hightlight>div,
    .nav-tabs>li.flight-hightlight>div:hover,
    .nav-tabs>li.flight-hightlight>div:focus {
      color: #555;
      background-color: #fff;
      border: 1px solid #ddd;
      border-bottom-color: transparent;
      cursor: default;
    }

    .nav>li>div,
    .nav>li,
    .nav>li.flight-hightlight>div,
    .nav>li.flight-hightlight {
      background-color: #ffffff;
      color: #000000;
      border: 0;
      margin-right: 0px;
    }

    /* Only have one tab inside */

    #myTab>li,
    .tab-content {
      border: solid 1px blue;
    }

    #myTab>li,
    .tab-content {
      border: 1px solid #689bd7;
      box-shadow: 0 1px 3px #689bd7;
    }

    #myTab>li>div {
      padding: 0px 5px;
    }

    #myTab>li.flight-hightlight,
    #myTab>li.flight-hightlight>div {
      background-color: #689bd7;
    }

    #myTab>li.flight-hightlight>div {
      color: #ffffff;
    }

    [class^="soap-icon"],
    [class*=" soap-icon"] {
      vertical-align: baseline;
    }


    [class^="soap-icon"]:before,
    [class*=" soap-icon"]:before {
      font-family: 'FontAwesome';
      font-style: normal;
      font-weight: normal;
      speak: none;
      display: inline-block;
      text-decoration: inherit;
      text-align: center;
      font-variant: normal;
      text-transform: none;
      line-height: 1em;
      /* font-size: 120%; */
    }

    .soap-icon-plane-right:before {
      content: '\f0fb';
    }

    #myTab>li,
    .tab-content {
      border: solid 1px blue;
    }

    #myTab>li,
    .tab-content {
      border: 1px solid #689bd7;
      box-shadow: 0 1px 3px #689bd7;
    }

    .tab-content {
      background: #fff;
      clear: left;
    }

    /* Some effects */

    .fade {
      opacity: 0;
      -webkit-transition: opacity .15s linear;
      -o-transition: opacity .15s linear;
      transition: opacity .15s linear;
    }

    .fade.in {
      opacity: 1;
    }

    .tab-content>.active {
      display: block;
    }

    .flight-container .tab-content .tab-pane {
      padding: 20px;
      line-height: 1.7em;
    }

    .flight-container .tab-content .tab-pane:after {
      content: "";
      display: table;
      clear: both;
    }

    #tours-tab {
      position: relative;
    }

    .flight-container .tab-content .tab-pane .row {
      margin-bottom: 15px;
      width: 100%;
    }

    /* Setup for form submission */

    .form-group {
      margin-bottom: 15px;
    }

    form .form-group {
      margin-bottom: 15px;
    }

    .tour-searchform .form-group {
      margin-bottom: 12px;
      float: left;
      padding: 0 8px;
    }

    .tour-searchform .form-group label {
      max-width: 100%;
      display: block;
      margin-bottom: 5px;
      font-weight: normal;
      font-size: 0.8333em;
      text-transform: uppercase;
      color: #838383;
      margin-bottom: 3px;
    }

    .tour-searchform .form-group button,
    .tour-searchform .form-group input,
    .tour-searchform .form-group optgroup,
    .tour-searchform .form-group select {
      margin: 0;
    }

    .full-width {
      width: 100% !important;
    }

    .tour-searchform .form-group input.input-text,
    .tour-searchform .form-group select,
    .tour-searchform .form-group custom-select {
      background: #fff;
      border: 1px solid #ccc;
      line-height: normal;
    }

    .tour-searchform .form-group input.input-text,
    .tour-searchform .form-group custom-select {
      padding-left: 15px;
      padding-right: 15px;
      height: 34px;
    }

    .selector {
      position: relative;
      min-width: 60px;
      width: 100%;
      line-height: 0;
    }

    .tour-searchform .form-group select {
      height: 34px;
      padding: 8px 0 8px 8px;
    }

    .selector select {
      position: absolute;
      z-index: 1;
      width: 100%;
      -webkit-appearance: menulist-button;
      line-height: 30px;
    }

    select option {
      padding: 2px 10px;
    }

    .selector custom-select {
      display: inline-block;
      line-height: 32px;
      padding: 0 10px;
      position: relative;
      width: 100%;
      overflow: hidden;
      white-space: nowrap;
    }

    .selector custom-select:before {
      position: absolute;
      right: 0;
      top: 0;
      content: '';
      background: #ff3e3e;
      width: 24px;
      height: 100%;
    }

    .selector custom-select:after {
      position: absolute;
      right: 9px;
      top: 15px;
      border-top: 5px solid #fff;
      border-left: 3px solid transparent;
      border-right: 3px solid transparent;
      content: "";
    }

    .memberdiscount {
      width: 50px;
      height: 50px;
    }

    .tour-searchform .form-group button,
    .tour-searchform .form-group label {
      letter-spacing: 0.04em;
    }

    .tour-searchform .form-group button,
    .tour-searchform .form-group input[type="submit"] {
      border: none;
      color: #fff;
      cursor: pointer;
      padding: 0 15px;
      white-space: nowrap;
      font-size: 0.9167em;
      font-weight: bold;
      background: #98ce44;
      height: 34px;
      line-height: 34px;
    }

    /* Some effects for decoration */

    .animated {
      -webkit-animation-duration: 1s;
      animation-duration: 1s;
      -webkit-animation-fill-mode: both;
      animation-fill-mode: both;
    }

    .bounce {
      -webkit-animation-name: bounce;
      animation-name: bounce;
    }

    .uppercase {
      text-transform: uppercase;
    }

    .animated {
      visibility: hidden;
    }

    .icon-check {
      position: relative;
      padding-right: 30px !important;
      padding-left: 0 !important;
      overflow: hidden;
    }

    .icon-check:after {
      content: "";
      position: absolute;
      top: 0;
      right: 0;
      width: 30px;
      height: 100%;
      background: url(images/vt_icon_check.png) no-repeat center center #7db921;
    }

    button.btn-medium,
    input[type="submit"].btn-medium.btn-medium {
      padding: 0 32px;
      height: 34px;
      line-height: 34px;
      font-size: 1em;
    }

    button.full-width,
    input[type="submit"].full-width.full-width {
      padding-left: 0;
      padding-right: 0;
    }


    /* Resize content for mobile mode */

    @media (max-width: 767px) {
      .tour-searchform .form-group {
        float: left;
        width: 100%;
        padding: 0 20px;
      }

      .tour-searchform .form-group .selector select {
        width: 95%;
      }

      .tour-searchform .form-group .input-text {
        width: 80%;
      }
    }

    /* Reset the container width according to desktop screen changes */

    @media (min-width: 768px) {
      .flight-container {
        width: 750px;
      }
    }

    @media (min-width: 992px) {

      .flight-container {
        width: 970px;
      }
    }

    @media (max-width: 1199px) and (min-width: 992px) {
      .flight-container {
        padding-left: 0;
        padding-right: 0;
      }
    }

    @media (min-width: 1200px) {
      .flight-container {
        width: 1170px;
        padding-left: 0;
        padding-right: 0;
      }
    }
  </style>
EOT;
?>