<?php
$csscontent = <<<EOT
  <style>
  /* Setup for table */
    table#seacruiseinvietnam {
      width: 100%; 
      margin: 0 auto;
    }

    table#seacruiseinvietnam caption {
      padding: 5px;
      color: black;
      font-size: 32px;
      font-weight: 700;
      text-align: center;
    }

    table#seacruiseinvietnam tr td {
      /*
            the header elements should be in the middle as requested
        */
      vertical-align: middle;
      /* 
            the header padding is 5px as requested
        */
      padding: 5px;
      /*
            Make the cells look like the demand with dark grey and solid border
        */
      border: 0;

    }

    table#seacruiseinvietnam tr td p {
      background-color: red;
      font-size: 32px;
      font-weight: 700;
      text-align: center;
    }

    .animated {
      -webkit-animation-duration: 1s;
      animation-duration: 1s;
      -webkit-animation-fill-mode: both;
      animation-fill-mode: both;
      animation-delay: 0.3s;
      visibility: visible;
    }

    @-webkit-keyframes fadeInLeft {
      0% {
        opacity: 0;
        -webkit-transform: translateX(-20px);
        transform: translateX(-20px)
      }
      100% {
        opacity: 1;
        -webkit-transform: translateX(0);
        transform: translateX(0)
      }
    }

    @keyframes fadeInLeft {
      0% {
        opacity: 0;
        -webkit-transform: translateX(-20px);
        -ms-transform: translateX(-20px);
        transform: translateX(-20px)
      }
      100% {
        opacity: 1;
        -webkit-transform: translateX(0);
        -ms-transform: translateX(0);
        transform: translateX(0)
      }
    }

    /* Effect for the begginning loading time */

    .fadeInLeft {
      -webkit-animation-name: fadeInLeft;
      animation-name: fadeInLeft
    }

    #seacruisegallery {
      margin: 0 auto;
      padding: 5px;
    }

    #seacruisegallery img {
      -moz-transition: all 0.4s ease-out;
      -o-transition: all 0.4s ease-out;
      -webkit-transition: all 0.4s ease-out;
      -ms-transition: all 0.4s ease-out;
      transition: all 0.4s ease-out;
      -webkit-backface-visibility: hidden;
    }

    #seacruisegallery>div {
      padding: 5px;
      border: 1px solid grey;
      display: inline-block;
      position: relative;
    }
    
    #seacruisegallery>div:hover>img {
      -webkit-transform: scale(1.2);
      -moz-transform: scale(1.2);
      -ms-transform: scale(1.2);
      -o-transform: scale(1.2);
      transform: scale(1.2);
    }

/* Resize the column according to the width */
    @media (max-width: 650px) {
      #seacruisegallery {
        width: 330px;
      }
    }

    @media (min-width: 651px) {
      #seacruisegallery {
        width: 650px;
      }
    }
    
    @media (min-width: 981px) {
      #seacruisegallery {
        width: 980px;
      }
    }

    @media (min-width: 1301px) {
      #seacruisegallery {
        width: 1300px;
      }
    }

  </style>
EOT;
?>