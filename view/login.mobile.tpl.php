<?php
$regioncontent = <<<EOT
<div class="contentOnMB">
  <div>
    <div class="login">

      <h1 class="text-center title">Customer Login</h1>

      <form action="index.php" method="post" id="CustomerLogin2" autocomplete="off" novalidate>
      <input type="hidden" name="csrf" value="$csrfkey" />
      <input type='hidden' name='p' value='login.php'/>

        <div class="container1">
        
            <div>
            <span class="error">$user_login_error_messages</span>
            </div>

            <div>
            <label for="$user_login_loginname">{$user_login[$user_login_loginname][$user_login_label_name]}<font color="red">*</font></label>
            </div>
            <div>
            <input type="text" size="39" name="$user_login_loginname" value="{$user_login[$user_login_loginname][$user_login_postedvalue]}"></input>
            </div>

            <div>
            <label for="$user_login_password">{$user_login[$user_login_password][$user_login_label_name]}<font color="red">*</font></label>
            </div>
            <div>
            <input type="password" size="39" name="$user_login_password" value="{$user_login[$user_login_password][$user_login_postedvalue]}"></input>
            </div>

            <div class="btncontainer">

              <div>
                <input type="submit" name="login2" value="Log me in" id="login2" class="btn" />
              </div>

            </div>

        </div>

      </form>

    </div>

  </div>
</div>
EOT;
?>