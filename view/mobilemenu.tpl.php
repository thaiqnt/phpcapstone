<?php

// Enable search box for each category
if ($tourmenu['desktoptreasureactive'] != $notactive) {
$globalsearch = <<<EOT
          <form method="post" action="#" onsubmit="event.preventDefault(); return false;">
            <input type="text" id="searchonmobile" name="lookingfor" size="27" value="" class="text-search">
            <input type="button" name="search-submit" value="&#61442;" class="btn-search" onclick="searchtour('#searchonmobile');">
          </form>
EOT;

} else if ($tourmenu['desktopseacruiseactive'] != $notactive) {
$globalsearch = <<<EOT
          <form method="post" action="#" onsubmit="event.preventDefault(); return false;">
            <input type="text" id="searchonmobile" name="lookingfor" size="27" value="" class="text-search">
            <input type="button" name="search-submit" value="&#61442;" class="btn-search" onclick="searchcruise('#searchonmobile');">
          </form>
EOT;
} else {
  $globalsearch = '';
}



// There is no global search if users do not select Treasure tab
if ($tourmenu['mobiletreasureactive'] == $notactive) {
  $globalsearch = '';
}


$adminlink = '';

// Enable Admin menu for admin user
if ($isAdminUser) {
  $adminlink = <<<EOT
      <div>
        <span class="{$tourmenu['mobileadminactive']}">Admin</span>
        <ul>
          <li class="normalLink"><a href="?p=tourlist.php" title="Tour Management">Tour Management<i class="fa fa-next"></i></a></li>
          <li class="normalLink"><a href="?p=adminstatistics.php" title="Administration Statistics">Administration Statistics<i class="fa fa-next"></i></a></li>
          <li class="normalLink"><a href="?p=profile.php" title="Profile Management">Profile Management<i class="fa fa-next"></i></a></li>

        </ul>
      </div>
EOT;
}

$mobilemenu = <<<EOT
  <!-- This logo is for mobile version -->
  <div class="mainNavArea_mb__headWp contentOnMB">
    <div class="mainNavArea_mb__head">
      <div class="logoImg">
        <a href="#" title="Home Page for Vietnam Tours">
          <img src="images/vt_logo_w.png" alt="Vietnam Tours" width="220" height="42">
        </a>
      </div>
    </div>
  </div>
  <!-- This search area is for mobile version -->
  <div class="mainNavArea_mb_wp" style="display: block;">
    <div class="mainNavArea_mb contentOnMB">
      <div class="search-page_wp">
        <div class="search-page">
        $globalsearch
        </div>
      </div>
      <!-- payment methods are for mobile version -->
      <div class="mainNavArea_mb__payArea">
        <ul class="clearfix">
          {$tourlink['usercart']}
          {$tourlink['userinvoice']}
          {$tourlink['userreg']}
          {$tourlink['userprofile']}
          {$tourlink['usersignin']}
          {$tourlink['usersignout']}
        </ul>
      </div>
      <div class="mainLinkList clearfix mbMenu">
        <div>
          <span class="{$tourmenu['mobiletreasureactive']}">Treasures</span>
          <ul>
            <li class="normalLink"><a href="?p=treasure.php#getTour" title="Visit to the most favorite sites in the world.">Most Favorite Sites<i class="fa fa-next"></i></a></li>
            <li class="normalLink"><a href="#">Asia<i class="fa fa-next"></i></a></li>
            <li class="normalLink"><a href="#">Europe<i class="fa fa-next"></i></a></li>
            <li class="normalLink"><a href="#">North America<i class="fa fa-next"></i></a></li>
          </ul>
        </div>
        <div>
          <span class="{$tourmenu['mobileseacruiseactive']}">Sea Cruises</span>
          <ul>
            <li class="normalLink"><a href="?p=seacruise.php#getCruise" title="Find a Sea Cruise Trip">Find a Sea Cruise Trip<i class="fa fa-next"></i></a></li>
            <li class="normalLink"><a href="#">Royal Bermuda &amp; Boston<i class="fa fa-next"></i></a></li>
          </ul>
        </div>
        <div>
          <span class="{$tourmenu['mobileflightactive']}">Flight</span>
          <ul>
            <li class="normalLink"><a href="?p=flight.php#flightcontent" title="International Flights">International Flights<i class="fa fa-next"></i></a></li>
            <li class="normalLink"><a href="#">Hotels<i class="fa fa-next"></i></a></li>
            <li class="normalLink"><a href="#">Pubs<i class="fa fa-next"></i></a></li>
            <li class="normalLink"><a href="#">Domestic Flights<i class="fa fa-next"></i></a></li>
            <li class="normalLink"><a href="#">Discount Tickets<i class="fa fa-next"></i></a></li>
            <li class="normalLink"><a href="#">Online Reservation Sites<i class="fa fa-next"></i></a></li>
          </ul>
        </div>
        <div>
          <span class="{$tourmenu['mobileaboutusactive']}">About us</span>
          <ul>
            <li class="normalLink"><a href="?p=aboutus.php#aboutus" title="Who we are ? What can we help you ?">Who We Are?<i class="fa fa-next"></i></a></li>
            <li class="normalLink"><a href="#">FAQs<i class="fa fa-next"></i></a></li>
          </ul>
        </div>
        $adminlink
      </div>
    </div>
  </div>
EOT;
?>