<?php
$regioncontent = <<<EOT
    <div id="flashMessageBox" class="flashMessageModal">

      <!-- Show tracking content in modal mode -->
      <div class="flashMessageContent">
        <span class="flashMessageClose">&times;</span>
        <div id="flashMessageDetail"></div>
      </div>
    </div>
	<div class="container-fluid">
		<div class="row" id='checkoutheader'>
			<div class="row"></div>
			<div>
				<div class="panel panel-primary text-center">
					<div class="panel-heading">Cart Checkout</div>
					<div class="panel-body"><div class="col-md-12 alert text-success" id="cartmsg"></div></div>
					<div class="row" id='checkoutdetail'>
						<div class="col-md-2"><b>Tour Image</b></div>
						<div class="col-md-2"><b>Tour Name</b></div>
						<div class="col-md-2"><b>Tour Price</b></div>
						<div class="col-md-2"><b>Pax</b></div>
						<div class="col-md-2"><b>Price in $</b></div>
						<div class="col-md-2"><b>Action</b></div>
					</div>
					<br>
					<div id='cartdetail'>
					</div>
					<div class="panel-footer">

					</div>
				</div>
			</div>
			<div id='paymentsection' >
                <div class="panel panel-primary text-center">
                    <div class="panel-heading">Payment Information</div>
                    <div class="panel-body"><div class="col-md-12 alert text-danger" id="paymentmsg"></div></div>
                    <div class="row">
                        <div class="col-md-2"><b>Bill To</b></div>
                        <div class="col-md-3"><input id='billto' class='form-control' type='text' value='{$paymentinfo['billto']}'/></div>
                        <div class="col-md-2"><b>Due Date</b></div>
                        <div class="col-md-4"><input id='duedate' readonly class='form-control' type='text' value='{$paymentinfo['duedate']}'/>
                        </div>
                        <div class="col-md-1"></div>
                    </div>
                    <br/>
                    <div class="row">
                        <div class="col-md-2"><b>Address</b></div>
                        <div class="col-md-9"><input id='address' class='form-control' type='text' value='{$paymentinfo['address']}'/></div>
                        <div class="col-md-1"></div>
                    </div>
                    <br/>
                    <div class="row">
                        <div class="col-md-2"><b>Phone</b></div>
                        <div class="col-md-3"><input id='phone' class='form-control' type='text' value='{$paymentinfo['phone']}'/></div>
                        <div class="col-md-2"><b>Fax</b></div>
                        <div class="col-md-4"><input id='fax' class='form-control' type='text' value='{$paymentinfo['fax']}'/></div>
                        <div class="col-md-1"></div>
                    </div>
                    <br/>
                    <div class="row">
                        <div class="col-md-2"><b>Email</b></div>
                        <div class="col-md-3"><input id='email' class='form-control' type='text' value='{$paymentinfo['email']}'/></div>
                        <div class="col-md-2"><b>Tax</b></div>
                        <div class="col-md-4"><input disabled id='tax' class='form-control' type='text' value='{$paymentinfo['tax']}'/></div>
                        <div class="col-md-1"></div>
                    </div>
                    <br/>
                    <div class="row">
                        <div class="col-md-2"><b>Sub Total</b></div>
                        <div class="col-md-3"><input id='subtotal' class='form-control' type='text' value='{$paymentinfo['subtotal']}' disabled/></div>
                        <div class="col-md-2"><b>Deposite Amount</b></div>
                        <div class="col-md-4"><input id='depositeamt' class='form-control' type='text' value='{$paymentinfo['depositeamt']}'/></div>
                        <div class="col-md-1"></div>
                    </div>
                    <br/>
                    <div class="panel-footer">

                    </div>
                </div>
                <button class='btn btn-success btn-lg pull-right' id='checkoutbtn' data-toggle="modal" data-target="#myModal">Checkout</button>
            </div>
			<div class="col-md-2"></div>
		</div>
	</div>
    <script>
      // We show action result
      function showActionResult(str) {
        // Get the cart modal
        var infoBox = document.getElementById('flashMessageBox');

        // Get the <span> element that closes the modal
        var span = document.getElementsByClassName("flashMessageClose")[0];

        // When the user clicks on <span> (x), close the modal
        span.onclick = function() {
          infoBox.style.display = "none";
        }

        // When the user clicks anywhere outside of the modal, close it
        window.onclick = function(event) {
          if (event.target == infoBox) {
            infoBox.style.display = "none";
          }
        }

        infoDetail = document.getElementById("flashMessageDetail");
        if (infoDetail) {
          infoDetail.innerHTML = str;
        }

        // Display the infobox for user
        infoBox.style.display = "block";
      }

      function showResult(str) {
        var str = "<div><strong style=\"font-size: 32px; font-weight: bold; color: red;\">Well done!</strong></div>"+str;

        showActionResult(str);

      }

      // Implement the cart checkout
      function cartcheckout(){
          $.ajax({
              url: 'action.php',
              method: 'POST',
              data: {cartcheckout:1},
              success: function(data){
                // Empty Cart is found?
                if (data.indexOf('<div') < 0) {
                  $('#cartdetail').hide();
                  $('#checkoutdetail').hide();
                  $('#paymentsection').hide();
                  
                  // Put message in the cart message
                  $('#cartmsg').html(data);
                  //console.log(data);
                } else {
                  $('#cartdetail').html(data);
                  
                  // Update subtotal and tax
                  amount = $('#amounttotal').html();
                  $('#subtotal').val(amount);
                  
                  i = parseInt(amount);
                  
                  amount = (i == NaN) ? 0 : i * 0.13;
                  
                  $('#tax').val(amount);
                }
              }
          })
      }
      cartcheckout();

      // Remove button handler
      $('body').delegate('.remove','click',function(e){
          e.preventDefault();
          var tourid=$(this).attr('data-id');
          $.ajax({
              url: 'action.php',
              method: 'POST',
              data: {removefromcart:1,tourid:tourid},
              success: function(data){
                  $('#cartmsg').html(data);
                  cartcheckout();
                  cartcount();
                  
                  // Display the removal result
                  showResult(data);
              }
          })
      })

      // Do the payment
      $('#checkoutbtn').click(function(){
          //removeInputBlackListKeywords();
          
          $.ajax({
              url: 'action.php',
              method: 'POST',
              data: {paymentcheckout:1,
                csrf: '$csrfkey',
                billto: $('#billto').val(),
                address: $('#address').val(),
                // Fix security bug on Salsa server hosting
                duedate: $('#duedate').val().replace(new RegExp('-', 'g'), '/'),
                phone: $('#phone').val(),
                fax: $('#fax').val(),
                email: $('#email').val(),
                tax: $('#tax').val(),
                subtotal: $('#subtotal').val(),
                depositeamt: $('#depositeamt').val()
                },
              success: function(data){
                  if (data == 'OK') {
                    window.location.href = "?p=checkoutcomplete.php#checkoutcomplete";
                    return;
                  }
                  //console.log(data);
                  // Is an error message, print it out
                  if (data.indexOf('<div') < 0) {
                    $('#paymentmsg').html(data);
                    $('body').animate({scrollTop:0},500);
                  } else {
                    $('#cartdetail').html(data);
                    $('body').animate({scrollTop:0},500);
                  }
              }
          })
      })

    $("#duedate").datetimepicker({format: 'yyyy/mm/dd hh:ii'});

    </script>
EOT;
?>