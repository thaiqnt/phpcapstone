<?php

$regioncontent = <<<EOT
	<div class="container-fluid">
		<div class="row" id='invoicesection'>
			<div class="row"></div>
			<div>
                <div class="panel panel-primary text-center">
                    <div class="panel-heading">Invoice List</div>
                    <div class="panel-body"><div class="col-md-12 alert text-success" id="invoicemsg"></div></div>
					<div class="row" id="invoiceheader">
                        <div class="col-md-1"><b></b></div>
						<div class="col-md-2"><b>Invoice start date</b></div>
						<div class="col-md-1"><b>Total & Sub total</b></div>
                        <div class="col-md-2"><b>Due date</b></div>
						<div class="col-md-1"><b>Deposite amt / Tax</b></div>
						<div class="col-md-1"><b>Bill to</b></div>
                        <div class="col-md-1"><b>Phone</b></div>
						<div class="col-md-2"><b>Address</b></div>
						<div class="col-md-1"></div>
					</div>
					<br>
                    <div class="row" id="invoicelist"></div>
                    <div class="panel-footer">

                    </div>
                </div>            
				<div class="panel panel-primary text-center">
					<div class="panel-heading">Order List</div>
					<div class="panel-body"><div class="col-md-12 alert text-success" id="ordermsg"></div></div>
					<div class="row" id="orderheader">
						<div class="col-md-2"><b>Category</b></div>
						<div class="col-md-2"><b>Title</b></div>
						<div class="col-md-2"><b>Start date</b></div>
						<div class="col-md-2"><b>Duration</b></div>
                        <div class="col-md-2"><b>Price $</b></div>
                        <div class="col-md-2"><b>Pax</b></div>
					</div>
					<br>
                    <div class="row" id="orderlist">
					<div class="panel-footer">

					</div>
				</div>
			</div>
			<div class="col-md-2"></div>
		</div>
	</div>
    <script>
      function fillInvoice(){
          $.ajax({
              url: 'action.php',
              method: 'POST',
              data: {getInvoiceList:1},
              success: function(data){
                // Error is found?
                if (data.indexOf('<div') < 0) {
                  $('#invoiceheader').hide();
                  $('#orderheader').hide();
                  $('#invoicelist').html('');
                  
                  // Put message in the history message
                  $('#invoicemsg').html(data);
                  //console.log(data);
                } else {
                  // Everything is OK
                  $('#invoicemsg').html('');
                  $('#invoicelist').html(data);
                  
                  invdate = document.getElementById("invdateid");
                  
                  //console.log(invdate);
                  if (invdate != null) {
                    invdate.selectedIndex = 0; 
                    fillOrders(invdate.options[0].value);
                    updateInvoice(invdate.options[0]);
                  }
                  
                }
              }
          })
          
      }
      
      // First, load Invoices 
      fillInvoice();
      

      function fillOrders(invid){
          $.ajax({
              url: 'action.php',
              method: 'POST',
              data: {getOrderList:1,invid:invid},
              success: function(data){
                // Error is found?
                if (data.indexOf('<div') < 0) {
                  $('#orderheader').hide();
                  $('#orderlist').html('');
                  
                  // Put message in the invoice message
                  $('#ordermsg').html(data);
                  //console.log(data);
                } else {
                  // Everything is OK
                  $('#ordermsg').html('');
                  $('#orderlist').html(data);
                  
                }
              }
          })
          
      }
      
      function updateInvoice(item) {
        amount = item.getAttribute('data-subtotal'); 
        tax = item.getAttribute('data-tax'); 
        depot = item.getAttribute('data-depositeamt');
        
        a = parseInt(amount);
        t = parseInt(tax);
        d = parseInt(depot);

        total = (a == NaN || t == NaN || d == NaN ) ? 0 : a + t - d;
        
        // Round it
        total = Number(Math.round(total+'e5')+'e-5');

        document.getElementById('total').innerHTML = total+' / '+amount;
        
        document.getElementById('duedate').innerHTML = item.getAttribute('data-duedate'); 
        document.getElementById('depositeamt').innerHTML = item.getAttribute('data-depositeamt'); 
        document.getElementById('tax').innerHTML = item.getAttribute('data-tax'); 
        document.getElementById('billto').innerHTML = item.getAttribute('data-billto'); 
        document.getElementById('address').innerHTML = item.getAttribute('data-address'); 
        document.getElementById('phone').innerHTML = item.getAttribute('data-phone'); 
      }


    </script>
EOT;
?>