<?php
$regioncontent = <<<EOT
      <!-- Starting flight container section -->
      <div class="flight-container" style="padding:0;">
        <div style="padding: 15px;">
          <!-- Defining a flight icon -->
          <ul id="flightcontent" class="nav nav-tabs" style="border-bottom: 0px">
            <li class="flight-hightlight">
              <div>
                <span style="font-size: 25px;">
                <i class="soap-icon-plane-right" aria-hidden="true"></i>
              </span> Flight
              </div>
            </li>
          </ul>
          <!-- the flight content is starting here -->
          <div class="tab-content">
            <div class="tab-pane fade active in" id="tours-tab">
              <form class="tour-searchform" method="post" action="http://www.scott-media.com/test/form_display.php">
                <div class="row">
                  <div class="form-group">
                    <label for="tripcodeid">Tour name or trip code</label>
                    <input type="text" id="tripcodeid" name="tripcode" class="input-text" placeholder="" value=""></div>
                  <div class="form-group">
                    <label for="destinationid">Destination</label>
                    <div class="selector">
                      <select id="destinationid" name="destionation" class="custom-select">
                        <option value="0">Select all</option>
                        <option value="306">Vietnam</option>
                        <option value="312">&nbsp;&nbsp;&nbsp;Ha Long Bay</option>
                        <option value="322">&nbsp;&nbsp;&nbsp;Da Nang – Hoi An – Hue</option><option value="323">&nbsp;&nbsp;&nbsp;Ha Noi</option>
                        <option value="324">&nbsp;&nbsp;&nbsp;Ho Chi Minh city</option>
                        <option value="325">&nbsp;&nbsp;&nbsp;Mui Ne Beach</option>
                        <option value="326">&nbsp;&nbsp;&nbsp;Nha Trang Beach</option>
                        <option value="327">&nbsp;&nbsp;&nbsp;Phu Quoc Island</option>
                        <option value="328">&nbsp;&nbsp;&nbsp;Sapa &amp; Northern Highland</option><option value="348">&nbsp;&nbsp;&nbsp;Mekong Delta</option>
                        <option value="307">Laos</option>
                        <option value="329">&nbsp;&nbsp;&nbsp;Laos Cruise</option>
                        <option value="330">&nbsp;&nbsp;&nbsp;Luang Prabang</option>
                        <option value="331">&nbsp;&nbsp;&nbsp;Multi City</option>
                        <option value="332">&nbsp;&nbsp;&nbsp;Vientiane Destinations</option><option value="333">&nbsp;&nbsp;&nbsp;Pakse</option>
                        <option value="308">Cambodia</option>
                        <option value="334">&nbsp;&nbsp;&nbsp;Phnom Penh</option>
                        <option value="335">&nbsp;&nbsp;&nbsp;Siem Reap Destinations</option><option value="336">&nbsp;&nbsp;&nbsp;Sihanouk Ville</option>
                        <option value="337">&nbsp;&nbsp;&nbsp;Cambodia Cruise</option><option value="309">Thailand</option>
                        <option value="343">&nbsp;&nbsp;&nbsp;Bangkok</option><option value="344">&nbsp;&nbsp;&nbsp;Pataya</option>
                        <option value="345">&nbsp;&nbsp;&nbsp;Chiang Mai</option>
                        <option value="346">&nbsp;&nbsp;&nbsp;Kosamui</option>
                        <option value="310">Myanmar</option>
                        <option value="338">&nbsp;&nbsp;&nbsp;Mandalay</option>
                        <option value="339">&nbsp;&nbsp;&nbsp;Bagan</option>
                        <option value="340">&nbsp;&nbsp;&nbsp;Multi City</option><option value="341">&nbsp;&nbsp;&nbsp;Myanmar cruise</option>
                        <option value="342">&nbsp;&nbsp;&nbsp;Yangon</option>
                        <option value="347">Multi Country Tours</option>
                    </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="tourtypeid">Preference</label>
                    <div class="selector">
                      <select id="tourtypeid" name="tourtype" class="custom-select">
                        <option value="0">Select all</option>
                        <option value="1">Tour Guide Service</option>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="memberdiscountid">Member Discount</label>
                    <div><input type="checkbox" id="memberdiscountid" name="memberdiscount"></div>
                    <div class="clearer"></div>
                  </div>
                  <br>
                  <button class="btn-medium icon-check uppercase full-width animated bounce" style="animation-duration: 1s; visibility: visible;">search </button></div>
              </form>
            </div>
          </div>
        </div>
      </div>
EOT;
?>