<?php
$hero = <<<EOT
<div id="flashMessageBox" class="flashMessageModal">

  <!-- Show tracking content in modal mode -->
  <div class="flashMessageContent">
    <span class="flashMessageClose">&times;</span>
    <div id="flashMessageDetail"></div>
  </div>
</div>
<script>
    // We show user info that was kept in the session
    function showUserInfo(str) {
      // Get the cart modal
      var infoBox = document.getElementById('flashMessageBox');

      // Get the <span> element that closes the modal
      var span = document.getElementsByClassName("flashMessageClose")[0];

      // When the user clicks on <span> (x), close the modal
      span.onclick = function() {
        infoBox.style.display = "none";
        window.location = 'index.php?p=profile.php';
      }
      
      // When the user clicks anywhere outside of the modal, close it
      window.onclick = function(event) {
        if (event.target == infoBox) {
          infoBox.style.display = "none";
          window.location = 'index.php?p=profile.php';
        }
      }

      infoDetail = document.getElementById("flashMessageDetail");
      if (infoDetail) {
        infoDetail.innerHTML = str;
      }

      // Display the infobox for user
      infoBox.style.display = "block";
    }
    
    function showLogout() {
      var str = "<div><strong style=\"font-size: 32px; font-weight: bold;\">See you, </strong> <strong style=\"font-size: 32px; font-weight: bold; color: red;\">{$flashMsgInfo[$user_login_loginname]}</strong></div>You have successfully loged out. Have a good day! Thank you for using Vietnam Tour!";
      
      showUserInfo(str);

    }
$showUserInfo
</script>
<div class="mainImg_area">
  <div class="mainImg_area__imgBox" style="background-image: url(images/tourbackground.jpg);">
    <div class="login">

      <h1 class="text-center title">Customer Login</h1>

      <form action="index.php" method="post" id="CustomerLogin1" autocomplete="off" novalidate>
      <input type="hidden" name="csrf" value="$csrfkey" />
      <input type='hidden' name='p' value='login.php'/>

        <div class="container1">
        
            <div>
            <span class="error">$user_login_error_messages</span>
            </div>

            <div>
            <label for="$user_login_loginname">{$user_login[$user_login_loginname][$user_login_label_name]}<font color="red">*</font></label>
            </div>
            <div>
            <input type="text" name="$user_login_loginname" value="{$user_login[$user_login_loginname][$user_login_postedvalue]}"></input>
            </div>

            <div>
            <label for="$user_login_password">{$user_login[$user_login_password][$user_login_label_name]}<font color="red">*</font></label>
            </div>
            <div>
            <input type="password" name="$user_login_password" value="{$user_login[$user_login_password][$user_login_postedvalue]}"></input>
            </div>

            <div class="btncontainer">

              <div>
                <input type="submit" name="login1" value="Log me in" id="login1" class="btn" />
              </div>

            </div>

        </div>

      </form>

    </div>

  </div>
</div>
EOT;
?>