<?php
$content = <<<EOT
<!doctype html>
<html lang="en">

<head>
  <title>$title</title>
  <meta http-equiv="content-type" content="text/html; charset=utf-8" />
  <!--
                      .....                             .*(##*.                             .....                      
                    .((////((/*,.                       ,(####(                       .,*//(////((.                    
                    .((*.     .,*//*,      .###/         *###(*    ,(##(,         ,*//*,.     .*((.                    
                     ,/((*.         .,**,. ,###(         ,###,     .(##(*    .,**,.         .*((/,                     
                       */((*,             ..(##*    ./(##(**,(#((,  ,##*..,,.             ,*((/*                       
                         */((/*.         .*/*/.*##########((######(##(*,,.             .*/((/*                         
                           */((//,.    ,(##(,* ,##########################/         .,//((/*                           
                             */((//*,  (###/*( .############*(#############,      ,*//((/*                             
                               ,*(((/**((##*/(  (################((######*/(* .,,*/(((*,                               
                                 ,/((//,**(*((  *(##############/.*######**#(*,,//((/,                                 
                                   .,/(((/,,//.  .##############( .##(/##(/,,//((/,.                                   
                                      .*/(((/*,,*/##############(.*##((*,,/(((/*.                                      
                                         .(((((/,.,/(############*//,,,/(((/*                                          
                                          (##(((((/*,.*/(#####(/*.,*/(((((/..                                          
                                       ,*,*#####((((((/*.,///,.*/((((((##(, .**,                                       
                                    .//.  ,(###(####//(/*,...,*/(((######(.    .//.                                    
                                 .*/*.    ./##(,/#((*,.,//(((//,.,*(#####*.      .*/*.                                 
                               ,//*        *##(/,,,,//(((/**/((((//*,,*(#,          *//,                               
                             *//*         .,*,**/(((((((#*  *##//(((((/**,,,.         *//*                             
                           ,/(/      ..,**//((((//(#####(*  ,##(/##/**/((((//**,..      /(/,                           
                          ./(*,.,*/////(((//((#/  (#####(*  *##(/##,  .##**//(((/////*,.,*(/.                          
                           */(((((///*,.   .(##/  /######/  ,##(/#(   .##/.    .,*///(((((/*                           
                                          **,.    ./#####/ .*##/(#(    (###,.                                          
                                                   .####,  ./##//#(.                                                   
                                                    /##(.    ,*#(...                                                   
                                                    **.                                                                
                       .*((((((((***.              ./,    /((((((((,                                                   
                         ..,((,...*(,               ,.     ..,((,..                                                    
                           ,((.  .*##((#(* ,#(((#(.,#*       .((.  ,(##(/(#((##( ,##((##.                              
                           ,((.  .*#*  /#(.*/.  (#.,#*       .((.  ,(/,.,(*  *(#.,#(  *(*.                             
                           ,((.  .*(,  *#(.,(#((##.,#*       .((.  ,(*  .(((((##.,#*  ,(*.                             
                           ,((.  .*(,  *#(*(/. .##,,#*       .((.  ,(,  *#*  /##.,#*  ,(*.                             
                           .**.   ,*.  ,/* ./(/**/,,/,       .**.  ,/,   */(/,*/,./,  .*,                              
                                                                                                                       
  -->
  <meta name="viewport" content="width=device-width, initial-scale=1" />
  <!--[if LT IE 8]>
   <div id="MessageForIE6OrBelow">Please update this Internet Explorer to a newest version of Internet Explorer as this page is not serving correctly in the browser. Make sure your "Compatibility View" (next to the address bar at the top of the window) is not on if you are browsing Internet Explorer 8 or higher. </div>  
  <![endif]-->
  <!--[if IE 8]>
   <div id="MessageForIE8">Sorry, please be advised that these web site functionalities do not work fully with Internet Explorer 8.</div>  
  <![endif]-->
  <link rel="stylesheet" type="text/css" href="styles/print.css" media="print" />
  <link rel="stylesheet" type="text/css" href="styles/desktop.css" media="screen and (min-width: 768px)" />
  <link rel="stylesheet" type="text/css" href="styles/mobile.css" media="screen and (max-width: 767px)" />
  <link rel="stylesheet" type="text/css" href="styles/bootstrap.css">
  <link rel="stylesheet" type="text/css" href="styles/bootstrap-datetimepicker.min.css"/>

  <link rel="icon" type="image/png" sizes="32x32" href="favicons/favicon-32x32.png">
  <link rel="apple-touch-icon" sizes="57x57" href="favicons/apple-icon-57x57.png">
  <link rel="apple-touch-icon" sizes="60x60" href="favicons/apple-icon-60x60.png">
  <link rel="apple-touch-icon" sizes="72x72" href="favicons/apple-icon-72x72.png">
  <link rel="apple-touch-icon" sizes="76x76" href="favicons/apple-icon-76x76.png">
  <link rel="apple-touch-icon" sizes="114x114" href="favicons/apple-icon-114x114.png">
  <link rel="apple-touch-icon" sizes="120x120" href="favicons/apple-icon-120x120.png">
  <link rel="apple-touch-icon" sizes="144x144" href="favicons/apple-icon-144x144.png">
  <link rel="apple-touch-icon" sizes="152x152" href="favicons/apple-icon-152x152.png">
  <link rel="apple-touch-icon" sizes="180x180" href="favicons/apple-icon-180x180.png">
  <link rel="icon" type="image/png" sizes="192x192" href="favicons/android-icon-192x192.png">
  <link rel="icon" type="image/png" sizes="96x96" href="favicons/favicon-96x96.png">
  <link rel="icon" type="image/png" sizes="16x16" href="favicons/favicon-16x16.png">
  <script src="js/jquery.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <!-- Bootstrap Date-Picker Plugin -->
  <script src="js/bootstrap-datetimepicker.js"></script>
  <script src="js/main.js"></script>
  
  $csscontent
  <!--[if LTE IE 8]>
    <script> 
        document.createElement('header');
        document.createElement('nav');
        document.createElement('footer');
        document.createElement('article');
        document.createElement('section');
        document.createElement('article');
    </script>

    <style type="text/css">
        header,
        footer,
        article,
        nav
        {
          display: block;
        }
    </style>
  <![endif]-->
</head>

<body>
  <!-- There are two versions of logo: one is for mobile and one is for desktop -->
  $mobilemenu

  <!-- This container is used for both versions -->
  <div class="thecontainer">
    $desktopheader
    <div id='desktopheaderholder' style='position: relative; height: 38px;'></div>
    <!-- This content is responsible for the whole content -->
    <div class="content">

      $desktopmenu
      <div id='desktopmenuholder' style='position: relative; height: 60px;'></div>
      <!-- Sometimes it is called hero  -->
      $hero
      <div style='height: 10px;'></div>
	  $regioncontent
    </div>

    <!--/.content-->
    $footer
  </div>
</body>

</html>
EOT;
?>