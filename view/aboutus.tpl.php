<?php
$regioncontent = <<<EOT
      <article class="vt-founders" id="aboutus">
        <h2 class="vt-founders__heading aa-text-h3">Vietnam Tour Founder and Investor</h2>
        <ul class="vt-grid-container vt-grid-row">
          <li class="vt-founders__co-founder vt-grid-col vt-grid-col-inner"><img class="vt-founders__image" src="images/vt_ceo.png" alt="CEO - Thai Tran Ngoc Quoc" width="250" height="250">
            <div class="vt-founders__name aa-text-h5">Thai Tran</div>
            <div class="aa-text-info vt-founders__position">CEO</div>
            <p class="vt-founders__info">A longtime newsroom manager with a career spanning a quarter-century and five countries, Thai Tran has worked in print, radio, television and real-time electronic media, with a broad range of experience, including in business journalism, general news, data and graphics, newspaper design, strategic planning, project management, and newsroom budgets and administration.</p>
          </li>
          <li class="vt-founders__co-founder vt-grid-col vt-grid-col-inner"><img class="vt-founders__image" src="images/vt_cfo.png" alt="CFO - Tran Thai" width="250" height="250">
            <div class="vt-founders__name aa-text-h5">Tran Thai</div>
            <div class="aa-text-info vt-founders__position">Principal Investor</div>
            <p class="vt-founders__info">Tran Thai is heading up a team of data scientists at Thomson Reuters Labs – Waterloo Region, jointly exploring opportunities in big data with financial, legal, and accounting customers as well as pursuing partnerships with startups and academia. Prior to joining Thomson Reuters, he worked as an Executive in Residence at Communitech, where he supported over 100 startups helping them scale all aspects of their business. In his previous role as Director of Developer Relations at BlackBerry, Thai led a global team of 30+ developers through rapid expansion and managed direct technical relationships with over 600 major brands.</p>
          </li>
        </ul>
      </article>
EOT;
?>