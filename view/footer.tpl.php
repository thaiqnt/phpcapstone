<?php
$footer = <<<EOT
    <!-- There are two footers: one is for desktop version and one is for mobile version -->
    <!-- the footer is for desktop version -->
    <footer class="themefooter contentOnPC ">
      <div class="inner ">
        <ul class="footer-nav clearfix ">
          <li><a href="#">About VNTOURS</a></li>
          <li><a href="#">Terms Conditions</a></li>
          <li><a href="#">Private Policy</a></li>
          <li><a href="#">For Investors</a></li>
          <li><a href="#">Travel Agents</a></li>
        </ul>
        <ul class="pay-list clearfix ">
          <li class="mainNavArea_mb__payArea__pay_item"><a href="#"><i class="fa fa-visa"></i><span>Visa</span></a></li>
          <li class="mainNavArea_mb__payArea__pay_item"><a href="#"><i class="fa fa-paypal"></i><span>Paypal</span></a></li>
          <li class="mainNavArea_mb__payArea__pay_item"><a href="#"><i class="fa fa-bitcoin"></i><span>Bitcoin</span></a></li>
        </ul>
        <p><small>Copyright © Vietnam Tours. All Rights Reserved.</small></p>
      </div>
      <!--/.inner-->
    </footer>
    <!-- /.themefooter -->
    <!-- the footer is for mobile version -->
    <footer class="themefooter contentOnMB ">
      <div class="inner ">
        <!-- payment methods are on the mobile footer -->
        <ul class="pay-list clearfix ">
          <li class="mainNavArea_mb__payArea__pay_item"><a href="#"><i class="fa fa-visa"></i><span>Visa</span></a></li>
          <li class="mainNavArea_mb__payArea__pay_item"><a href="#"><i class="fa fa-paypal"></i><span>Paypal</span></a></li>
          <li class="mainNavArea_mb__payArea__pay_item"><a href="#"><i class="fa fa-bitcoin"></i><span>Bitcoin</span></a></li>
        </ul>
        <!-- the mobile footer links -->
        <ul class="footer-nav clearfix ">
          <li><a href="#">About VNTOURS</a></li>
          <li><a href="#">Terms Conditions</a></li>
          <li><a href="#">Private Policy</a></li>
          <li><a href="#">For Investors</a></li>
          <li><a href="#">Travel Agents</a></li>
        </ul>
        <!-- The copyright annoucement -->
        <p><small>Copyright © Vietnam Tours. All Rights Reserved.</small></p>

      </div>
      <!--/.inner-->
    </footer>
EOT;
?>