<?php

// Default form attributes
$formmethod = 'post';
$inputattr = '';
$showActionResult = '';


if ($isActionComplete) {
  // Disable inputs
  $inputattr = 'disabled';
  // Disable form
  $formmethod = 'get';
  // Pop up the flash message
  $showActionResult = "showResult('$tour_messages');";
}

// List out all tour categories
$categorylist = '';

if (count($categories)>0) {
  $categorylist .= "<select $inputattr name='$tour_categoryid' class='form-control'>";
  foreach ($categories as $cat) {
    // Is it selected?
    $str = ($cat['id'] == $tour[$tour_categoryid][$tour_postedvalue]) ? ' selected ' : '';
    $categorylist .= "<option value=\"{$cat['id']}\"$str>{$cat['title']}</option>";
  }
  $categorylist .= '</select>';
}

$tourdetailheader = '';

$undeletebutton = '';
$updatebutton = '';

// Enable update / create functions 
if (!$isActionComplete) {
  if ($tour[$tour_id][$tour_postedvalue] == -1) {
    $tourdetailheader = 'Create a Brand New Tour';
    //$updatebutton = '<input type="submit" name="cmd" value="create" class="tourbtn" onclick="
    //  removeInputBlackListKeywords(); checkMaxLengthInSalsaHost(\'tourdesc\');"/>';
    // Fix security bug on Salsa server hosting
    $updatebutton = '<input type="submit" name="cmd" value="create" class="tourbtn" />';
  } else {
    $tourdetailheader = 'Tour Update';
    //$updatebutton = '<input type="submit" name="cmd" value="update" class="tourbtn" onclick="
    //  removeInputBlackListKeywords(); checkMaxLengthInSalsaHost(\'tourdesc\');"/>';
    // Fix security bug on Salsa server hosting
    $updatebutton = '<input type="submit" name="cmd" value="update" class="tourbtn" onclick="$(\'#startdat\').val($(\'#startdat\').val().replace(new RegExp(\'-\', \'g\'), \'/\'));"/>';
  }

  if ($tour[$tour_deleted][$tour_postedvalue]) {
    //$undeletebutton = '<input type="submit" name="cmd" value="undelete" class="tourbtn" onclick="
    // removeInputBlackListKeywords();"/>';
    // Fix security bug on Salsa server hosting
    $undeletebutton = '<input type="submit" name="cmd" value="undelete" class="tourbtn" onclick="$(\'#startdat\').val($(\'#startdat\').val().replace(new RegExp(\'-\', \'g\'), \'/\'));"/>';
  }

}

foreach ($imagelinks as $link) {
  $tourimglinks .= <<<EOT
                              <li>
                                <a href="javascript: changeImage('$link');">
                                  <img src="images/$link" alt="$link" width="60" /></a>
                              </li>
EOT;
}
                              

$hero = <<<EOT
<div id="flashMessageBox" class="flashMessageModal">

  <!-- Show tracking content in modal mode -->
  <div class="flashMessageContent">
    <span class="flashMessageClose">&times;</span>
    <div id="flashMessageDetail"></div>
  </div>
</div>
<script>
    // We show action result
    function showActionResult(str) {
      // Get the cart modal
      var infoBox = document.getElementById('flashMessageBox');

      // Get the <span> element that closes the modal
      var span = document.getElementsByClassName("flashMessageClose")[0];

      // When the user clicks on <span> (x), close the modal
      span.onclick = function() {
        infoBox.style.display = "none";
        window.location = '?p=tourlist.php&catid={$tour[$tour_categoryid][$tour_postedvalue]}';
      }
      
      // When the user clicks anywhere outside of the modal, close it
      window.onclick = function(event) {
        if (event.target == infoBox) {
          infoBox.style.display = "none";
          window.location = '?p=tourlist.php&catid={$tour[$tour_categoryid][$tour_postedvalue]}';
        }
      }

      infoDetail = document.getElementById("flashMessageDetail");
      if (infoDetail) {
        infoDetail.innerHTML = str;
      }

      // Display the infobox for user
      infoBox.style.display = "block";
    }
    
    function showResult(str) {
      var str = "<div><strong style=\"font-size: 32px; font-weight: bold; color: red;\">Well done!</strong></div>"+str;
      
      showActionResult(str);

    }
$showActionResult
</script>
	<div class="container-fluid">
		<div class="row">
			<div class="row"></div>
			<div>
                <div class="panel panel-primary text-center">
                    <div class="panel-heading">$tourdetailheader</div>
                    <div class="panel-body"><div class="col-md-12 alert text-success">$tour_messages</div></div>
                    <form action="index.php" method="$formmethod" autocomplete="off" novalidate>
                    <input type="hidden" name="csrf" value="$csrfkey" />
                    <div class="row">
                        <div class="col-md-1">
                        <input type='hidden' name='p' value='tourdetail.php'/>
                        <input type='hidden' name="$tour_id" value='{$tour[$tour_id][$tour_postedvalue]}'/>
                        </div>
                        <div class="col-md-1"><b>Category</b><span style="color: red">*</span></div>
                        <div class="col-md-2">
                        $categorylist
                        </div>
						<div class="col-md-1"><b>Title</b><span style="color: red">*</span></div>
                        <div class="col-md-6"><input $inputattr class='form-control' type='text' name="$tour_title" value='{$tour[$tour_title][$tour_postedvalue]}'/></div>
                        <div class="col-md-1"><input type='hidden' name="$tour_deleted" value='{$tour[$tour_deleted][$tour_postedvalue]}'/></div>
                    </div>
                    <br/>
                    <div class="row">
                        <div class="col-md-1"></div>
						<div class="col-md-1"><b>Price</b><span style="color: red">*</span></div>
                        <div class="col-md-2"><input $inputattr class='form-control' type='text' name="$tour_price" value='{$tour[$tour_price][$tour_postedvalue]}'/></div>
						<div class="col-md-1"><b>Duration</b><span style="color: red">*</span></div>
                        <div class="col-md-2"><input $inputattr class='form-control' type='text' name="$tour_duration" value='{$tour[$tour_duration][$tour_postedvalue]}'/></div>
						<div class="col-md-1"><b>Max Pax</b><span style="color: red">*</span></div>
                        <div class="col-md-1"><input $inputattr class='form-control' type='text' name="$tour_maxpax" value='{$tour[$tour_maxpax][$tour_postedvalue]}'/></div>
						<div class="col-md-1"><b>Avail Pax</b><span style="color: red">*</span></div>
                        <div class="col-md-1"><input $inputattr class='form-control' type='text' name="$tour_availpax" value='{$tour[$tour_availpax][$tour_postedvalue]}'/></div>
                        <div class="col-md-1"></div>
                    </div>
                    <br/>
                    <div class="row">
                        <div class="col-md-1"></div>
						<div class="col-md-1"><b>Start Date</b><span style="color: red">*</span></div>
                        <div class="col-md-2"><input id='startdat' $inputattr readonly class='form-control' type='text' name="$tour_startdate" value='{$tour[$tour_startdate][$tour_postedvalue]}'/></div>
						<div class="col-md-1"><b>Key-words</b><span style="color: red">*</span></div>
                        <div class="col-md-6"><input $inputattr class='form-control' type='text' name="$tour_keywords" value='{$tour[$tour_keywords][$tour_postedvalue]}'/></div>
                        <div class="col-md-1"></div>
                    </div>
                    <br/>
                    <div class="row">
                        <div class="col-md-1"></div>
                        <div class="col-md-1"><b>Image</b><span style="color: red">*</span></div>
                        <div class="col-md-2">
                          <div class="dropdown">
                            <button $inputattr class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" >&nbsp;&nbsp;&nbsp;Tour Image&nbsp;&nbsp;&nbsp;
                              <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu">
                              $tourimglinks
                            </ul>
                          </div>
                          <br/>
                          <img id= "showntourimage" src='images/{$tour[$tour_image][$tour_postedvalue]}' alt='{$tour[$tour_title][$tour_postedvalue]}' class='tourimg'/>
                          <input type='hidden' id= "postedtourimage" name="$tour_image" value='{$tour[$tour_image][$tour_postedvalue]}'/>
                        </div>                        
						<div class="col-md-1"><b>Brief</b><span style="color: red">*</span></div>
                        <div class="col-md-6"><textarea $inputattr id="tourdesc" name="$tour_desc" rows="5" cols="50" class='form-control'>{$tour[$tour_desc][$tour_postedvalue]}</textarea></div>
                        <div class="col-md-1"></div>
                    </div>
                    <br/>
                    <div class="row">
                        <div class="col-md-1"></div>
                        <div class="col-md-10">
                          $updatebutton&nbsp;
                          <input type="button" name="cmd" value="back" class="tourbtn" onclick="window.location.href = '?p=tourlist.php&catid={$tour[$tour_categoryid][$tour_postedvalue]}';"/>&nbsp;
                          $undeletebutton
                        </div>
                        <div class="col-md-1"></div>
                    </div>
                    <br/>
                    
                    </form>
                    <br/>
                    <div class="panel-footer">

                    </div>
                </div>
            </div>
			<div class="col-md-2"></div>
		</div>
	</div>
    <script>
    $("#startdat").datetimepicker({format: 'yyyy-mm-dd hh:ii'});

    function changeImage(filename) {
      document.getElementById('postedtourimage').value = filename;
      document.getElementById('showntourimage').setAttribute('src', 'images/'+filename);
    }
    </script>
EOT;
?>