<?php
$csscontent = <<<EOT
  <style>
    /* The Flash Message Modal (background) */

    .flashMessageModal {
      display: none;
      /* Hidden by default */
      position: fixed;
      /* Stay in place */
      z-index: 501;
      /* Sit on top */
      padding-top: 100px;
      /* Location of the box */
      left: 0;
      top: 0;
      width: 100%;
      /* Full width */
      height: 100%;
      /* Full height */
      overflow: auto;
      /* Enable scroll if needed */
      background-color: rgb(0, 0, 0);
      /* Fallback color */
      background-color: rgba(0, 0, 0, 0.4);
      /* Black w/ opacity */
    }

    /* Flash Message Content */

    .flashMessageContent {
      background-color: #fefefe;
      border: 1px solid #888;
      padding: 20px;
      width: 37%;
      height: auto;
      position: absolute;
      top: 37%; left: 50%;
      transform: translate(-50%,-50%);
      box-shadow: 0 0 8px #555;
      -moz-box-shadow: 0 0 8px #555;
      -webkit-box-shadow: 0 0 8px #555;    

    }

    /* The Close Button */

    .flashMessageClose {
      color: #aaaaaa;
      float: right;
      font-size: 28px;
      font-weight: bold;
    }

    .flashMessageClose:hover,
    .flashMessageClose:focus {
      color: #000;
      text-decoration: none;
      cursor: pointer;
    }
    
    .text-center {
        text-align: center;
    } 
    .mainImg_area__imgBox .profile {
        width: 770px;
        height: 450px;
        background: #fff;
        padding: 20px;
        position: absolute;
        top: 50%; left: 50%;
        transform: translate(-50%,-50%);
        box-shadow: 0 0 8px #555;
        -moz-box-shadow: 0 0 8px #555;
        -webkit-box-shadow: 0 0 8px #555;    
    }
    .contentOnMB .profile {
        max-width: 100%;
        width: 100%;
        height: 1500px;
        background: #fff;
        padding: 20px;
    }
    .profile form input {
        font-size: 16px;
        border: none;
        border-bottom: 1px solid #a1a1a1;
        border-radius: 0;
    }
   .profile span.error {
        font-size: 12px;
        color: #a94442;
        display: block;
        margin-top: 2px;
    }

   .profile span.error ul {
        list-style-type: circle;
    }

    .profile h1.title {
        font-size: 20px;
        margin: 5px 0 15px;
        text-transform: uppercase;
    }
   .profile .btn {
        border-radius: 0;
        -webkit-transition: all .3s;
        -o-transition: all .3s;
        transition: all .3s;
        text-transform: uppercase;
        font-size: 11px;
        font-weight: 500;
        padding-top: 10px;
        padding-bottom: 8px;
        color: #fff;
        background-color: #337ab7;
        border-color: #2e6da4;     
        white-space: nowrap;
        vertical-align: middle;
        width: 100px;
        margin: 0 auto; 
    }
    
   .profile .rightbtn {
        float: right;
    }

    .container1,.container2,.container3,.container4,.container5,.container6,.container7 {
        float: left;
        margin-top: 20px;
        padding-right: 10px;
        padding-left: 10px;
    }

    .container1,.container2,.container3,.container4 {
        width: 22%;
    }

    .container5,.container6,.container7 {
        width: 30%;
    }

    .container1,.container2,.container3,.container5,.container6 {
        border-right: 1px solid #ccc;
    }

    .btncontainer {
        width: 250px; 
        margin: 20px auto;   
    }

    .contentOnMB .profile div {
        float: left;
        margin-top: 20px;
        padding-right: 10px;
        padding-left: 10px;
        width: 100%;
    }

    .contentOnMB .profile form input {
        width: 90%;
    }

  </style>
EOT;
?>