<?php

// Enable search box for each category
if ($tourmenu['desktoptreasureactive'] != $notactive) {
  $globalsearch = <<<EOT
            <form method="post" action="#" onsubmit="event.preventDefault(); return false;">
              <input type="text" id="searchondesktop" name="q" size="27" value="" class="text-search">
              <input type="button" name="search-submit" value="&#61442;" class="btn-search" onclick="searchtour('#searchondesktop');">
            </form>
EOT;

} else if ($tourmenu['desktopseacruiseactive'] != $notactive) {
  $globalsearch = <<<EOT
            <form method="post" action="#" onsubmit="event.preventDefault(); return false;">
              <input type="text" id="searchondesktop" name="q" size="27" value="" class="text-search">
              <input type="button" name="search-submit" value="&#61442;" class="btn-search" onclick="searchcruise('#searchondesktop');">
            </form>
EOT;
} else {
  $globalsearch = '';
}


$desktopheader = <<<EOT
    <!-- This search pane is for desktop version -->
    <header class="themeheader contentOnPC">
      <div class="inner clearfix">
        <div class="search-page floatRight">
        $globalsearch
        </div>
        <ul class="pay-list clearfix floatRight">
          {$tourlink['usercart']}
          {$tourlink['userinvoice']}
          {$tourlink['userreg']}
          {$tourlink['userprofile']}
          {$tourlink['usersignin']}
          {$tourlink['usersignout']}
        </ul>
      </div>
    </header>
EOT;
?>