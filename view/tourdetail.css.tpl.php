<?php
$csscontent = <<<EOT
  <style>
    /* The Flash Message Modal (background) */

    .flashMessageModal {
      display: none;
      /* Hidden by default */
      position: fixed;
      /* Stay in place */
      z-index: 501;
      /* Sit on top */
      padding-top: 100px;
      /* Location of the box */
      left: 0;
      top: 0;
      width: 100%;
      /* Full width */
      height: 100%;
      /* Full height */
      overflow: auto;
      /* Enable scroll if needed */
      background-color: rgb(0, 0, 0);
      /* Fallback color */
      background-color: rgba(0, 0, 0, 0.4);
      /* Black w/ opacity */
    }

    /* Flash Message Content */

    .flashMessageContent {
      background-color: #fefefe;
      border: 1px solid #888;
      padding: 20px;
      width: 37%;
      height: auto;
      position: absolute;
      top: 37%; left: 50%;
      transform: translate(-50%,-50%);
      box-shadow: 0 0 8px #555;
      -moz-box-shadow: 0 0 8px #555;
      -webkit-box-shadow: 0 0 8px #555;    

    }

    /* The Close Button */

    .flashMessageClose {
      color: #aaaaaa;
      float: right;
      font-size: 28px;
      font-weight: bold;
    }

    .flashMessageClose:hover,
    .flashMessageClose:focus {
      color: #000;
      text-decoration: none;
      cursor: pointer;
    }
      
    .tourbtn {
      border-radius: 0;
      -webkit-transition: all .3s;
      -o-transition: all .3s;
      transition: all .3s;
      text-transform: uppercase;
      font-size: 11px;
      font-weight: 500;
      padding-top: 10px;
      padding-bottom: 8px;
      color: #fff;
      background-color: #337ab7;
      border-color: #2e6da4;     
      white-space: nowrap;
      vertical-align: middle;
      width: 100px;
      margin: 0 auto; 
    }

    .tourimg {
      max-width: 180px;
      max-height: 120px;
    }

    .tourimg:hover {
      -webkit-transform: scale(2);
      -moz-transform: scale(2);
      -ms-transform: scale(2);
      -o-transform: scale(2);
      transform: scale(2);
    }


  </style>
EOT;
?>