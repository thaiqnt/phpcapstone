<?php
/**
 * Validator Class
 *
 * @author      Thai Tran Ngoc Quoc
 * @copyright   Copyright (c) 2018 - All rights reserved
 */

// ------------------------------------------------------------------------

namespace classes\utility;

class validator {

    // ------------------------------------------------------------------------

    private $error_code             = array();

    // The internal variables
    private $_validation_data       = array();
    private $_error_detail          = array();    
    private $_customized_error_log  = array();  
    private $_message_log           = array();  



    // --------------------------------------------------------------------


    // --------------------------------------------------------------------

    /**
     * __construct
     *
     * This function is responsible for initializing some internal variables
     *
     * @return  none
     */
    public function __construct() {
        // Error code table for specific field
        $this->error_code['compulsory'] = "%s field is compulsory.";
        $this->error_code['isset'] = "%s field must have a value.";
        $this->error_code['validemail'] = "%s field must have a valid email address.";
        $this->error_code['validemails'] = "%s field must have all valid email addresses.";
        $this->error_code['minlength'] = "%s field must be at least %s characters in length.";
        $this->error_code['maxlength'] = "%s field can not be over %s characters in length.";
        $this->error_code['exactlength'] = "%s field must have exactly %s characters in length.";
        $this->error_code['alpha'] = "%s field may only have alphabetical characters.";
        $this->error_code['alphanumeric'] = "%s field may only have alpha-numeric characters.";
        $this->error_code['alphadash'] = "%s field may only have alpha characters, underscores, and dashes.";
        $this->error_code['alphadashspace'] = "%s field may only have alpha characters, spaces, underscores, and dashes.";
        $this->error_code['numericdash'] = "%s field may only have numeric characters, underscores, and dashes.";
        $this->error_code['numeric'] = "%s field must have a number.";
        $this->error_code['matches'] = "%s field does not match %s field.";
        $this->error_code['canadianpostal'] = "%s field must have a valid Canadian Postal Code.";
        $this->error_code['phone'] = "%s field must have a valid phone format.";
        $this->error_code['password'] = "%s field must have a valid password format.";

    }


    // --------------------------------------------------------------------

    /**
     * buildErrorMessages
     *
     * This function is responsible for build error lists if possible
     *
     * @return  string: the error list
     */
    public function buildErrorMessages() {


        $result = '';
        if (count($this->_error_detail)>0) {
            $result .= "<ul>";
            foreach ($this->_error_detail as $item) {
                $result .= "<li>$item</li>";
            }
            $result .= "</ul>";
        }

        return $result;
    }

    // --------------------------------------------------------------------
    

    /**
     * validation_bind
     *
     * This function takes an array of field names and validation
     * rules as input, validates the info, and stores it
     *
     * @param   field: the field to be validated by rules
     * @param   name: the name which will be displayed on html
     * @param   rules: the rules of validation  
     * @return  none
     */
    public function validation_bind($field, $name = '', $rules = '') {
        if (count($_POST) == 0) {
            // There is nothing to proceed if no data is submitted
            $this->internal_log('There is nothing to proceed if no data is submitted');
            return;
        }
        if ( ! is_string($field) OR  ! is_string($rules) OR $field == '') {
            // There is nothing to do if no rules or field is supplied
            $this->internal_log('There is nothing to do if no rules or field is supplied');
        }
        // If there is no name, we use field as name
        $name = ($name == '') ? $field : $name;

        $this->_validation_data[$field] = array(
                                'field' => $field, 
                                'name' => $name, 
                                'rules' => $rules
                            );
    
        

    }

	// --------------------------------------------------------------------
	
    /**
     *   
     *
     * This function does an internal logging.
     *
     * @param   message: a message to be logged in the system
     * @return  none
     */     
    private function internal_log($message) {
        $this->_message_log[] = array($message);
    }   
	
    // --------------------------------------------------------------------
    
    /**
     * Set Customized Error Message
     *
     * @param   funcname: the function name to be set when an error corresponding to that name occurs
     * @param   message: the new error message format
     * @return  none
     */
    public function set_customized_error($funcname, $message = '') {
        if ( ! is_array($funcname)) {
            $funcname = array($funcname => $message);
        }
    
        $this->_customized_error_log = array_merge($this->_customized_error_log, $funcname);
    }

    // --------------------------------------------------------------------
    
    /**
     * Set By-Defined Error Message when another rule is applied like password mismatch
     *
     * @param   funcname: the function name to be set
     * @param   message: the new error message format
     * @return  none
     */
    public function set_other_error($typename, $message = '') {
        $this->_error_detail[$typename] = $message;
    }

    // --------------------------------------------------------------------
    
	/**
	 * isValidated
	 *
	 * This function does all the work by going through each rule and proceed the validation
	 *
	 * @return	bool: tell the validation is OK or not
	 */		
	public function isValidated() {
		if (count($_POST) == 0) {
            // There is nothing to proceed if no data is submitted
            $this->internal_log('There is nothing to proceed if no data is submitted');
			return false;
		}
		
		if (count($this->_validation_data) == 0) {
            // There is no rule to proceed
            $this->internal_log('There is no rule to proceed');
            return false;
		}
	
							
		// Walk through the rules for each field, compare the 
		// corresponding $_POST item and detect for errors
		foreach ($this->_validation_data as $field => $row) {
			// Get the data from the corresponding $_POST array and store it in the _validation_data array.
			
            if (isset($_POST[$field]) AND $_POST[$field] != "") {
                $data = $_POST[$field];
                $this->_validation_data[$field]['value'] = $data;
            }
		      
            $value = isset($this->_validation_data[$field]['value']) ? $this->_validation_data[$field]['value'] : null;

            $this->validateAllRules($row, explode('/', $row['rules']), $value);					
		}

		// Did we end up with any errors?
		$num_errors = count($this->_error_detail);

		// Is OK now ?
		return ($num_errors == 0);
	}

	// --------------------------------------------------------------------
    
	
	/**
	 * Validate all defined rules
	 *
	 * @param	row: current row for checking rule
	 * @param	rules: an array of rules
	 * @param	value: a posted value
	 * @return	none
	 */	
	private function validateAllRules($row, $rules, $value = null) {
		// If there are blank field and no compulsory rule, no further steps are necessary
		if (is_null($value) and ! in_array('compulsory', $rules)) {
			return;
		}

		// It is a check of isset. Normally checkboxes is a good candidate for this
		if (is_null($value)) {
			if (in_array('compulsory', $rules) or in_array('isset', $rules, true)) {
				// What is the message type?
				$type = (in_array('compulsory', $rules)) ? 'compulsory' : 'isset';
			
				if ( isset($this->_customized_error_log[$type])) {
                    $code = $this->_customized_error_log[$type];
				}
				else {
                    if (($code = $this->error_code[$type]) === false) {
                        $code = 'The field was not set';
                    }                           
				}
				
				// Lookup the error code
				$msg = sprintf($code, $row['name']);

				// Store the error message
				$this->_validation_data[$row['field']]['error'] = $msg;
				
				if ( ! isset($this->_error_detail[$row['field']])) {
					$this->_error_detail[$row['field']] = $msg;
				}
			}
					
			return;
		}

		// Walk through each rule and execute it
		foreach ($rules as $rule) {
			$value = $this->_validation_data[$row['field']]['value'];

            $param = false;

			// Detect rule with a parameter like: max_length[2]
			if (preg_match("/(.*?)\[(.*?)\]/", $rule, $match)) {
				$rule	= $match[1];
				$param	= $match[2];
			}
			
            if (function_exists($rule)) {
                $result = $rule($value, $param);
            } else if (method_exists($this, $rule)) {
                $result = $this->$rule($value, $param);
            } else continue; // skip invalid rules!!!
			

			$this->_validation_data[$row['field']]['value'] = (is_bool($result)) ? $value : $result;
						
			// If there is an error, we continue build the error message
			if ($result === false) {
				if ( isset($this->_customized_error_log[$rule])) {
                    $code = $this->_customized_error_log[$rule];
				}
				else {
                    if (($code = $this->error_code[$rule]) === false) {
                        $code = 'There is no error code corresponding to the field name: ' . $row['field'];
                    }                       
				}
				
				// If the parameter is the name of another field?  If so, get the name of that field
				if (isset($this->_validation_data[$param]) and isset($this->_validation_data[$param]['name'])) {
					$param = $this->_validation_data[$param]['name'];
				}
				
				// Build the error message
				$msg = sprintf($code, $row['name'], $param);

				// Store the error message
				$this->_validation_data[$row['field']]['error'] = $msg;
				
                // Also update the error message in the error list
				if ( ! isset($this->_error_detail[$row['field']])) {
					$this->_error_detail[$row['field']] = $msg;
				}
				
                // return with errors
				return;
			}
		}
	}

	
    // --------------------------------------------------------------------
    
    /**
     * Match one field to another
     *
     * @param   str: a string that determines the match
     * @param   field: a field name to be matched
     * @return  bool: a boolean value should be returned indicating it is OK or not
     */
    private function matches($str, $field) {
        if ( ! isset($_POST[$field])) {
            return false;               
        }
        
        $field = $_POST[$field];

        return ($str !== $field) ? false : true;
    }
    
	
	// --------------------------------------------------------------------
	
	/**
	 * Check a compulsory field
	 *
     * @param   str: a field to be checked
     * @return  bool: a boolean value should be returned indicating it is OK or not
	 */
	private function compulsory($str) {
		if ( ! is_array($str)) {
			return (trim($str) == '') ? false : true;
		}
		else {
			return ( ! empty($str));
		}
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * Validate Minimum Length
	 *
     * @param   str: a string to be checked
     * @param   val: a length of a string
     * @return  bool: a boolean value should be returned indicating it is OK or not
	 */	
	private function minlength($str, $val) {
		if (preg_match("/[^0-9]/", $val)) {
			return false;
		}

		if (function_exists('mb_strlen')) {
			return (mb_strlen($str) < $val) ? false : true;		
		}
	
		return (strlen($str) < $val) ? false : true;
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * Validate Maximum Length
	 *
     * @param   str: a string to be checked
     * @param   val: a length of a string
     * @return  bool: a boolean value should be returned indicating it is OK or not
	 */	
	private function maxlength($str, $val) {
		if (preg_match("/[^0-9]/", $val)) {
			return false;
		}

		if (function_exists('mb_strlen')) {
			return (mb_strlen($str) > $val) ? false : true;		
		}
	
		return (strlen($str) > $val) ? false : true;
	}
	
	// --------------------------------------------------------------------
	
	/**
	 * Validate Exact Length
	 *
	 * @param	str: a string to be checked
	 * @param	val: a length of a string
     * @return  bool: a boolean value should be returned indicating it is OK or not
	 */	
	private function exactlength($str, $val) {
		if (preg_match("/[^0-9]/", $val)) {
			return false;
		}
 
		if (function_exists('mb_strlen')) {
			return (mb_strlen($str) != $val) ? false : true;		
		}
	
		return (strlen($str) != $val) ? false : true;
	}
	
    // --------------------------------------------------------------------
    
    /**
     * Validate Alpha-numeric input
     *
     * @param   str: a string to be checked
     * @return  bool: a boolean value should be returned indicating it is OK or not
     */ 
    private function alphanumeric($str) {
        return ( ! preg_match("/^([a-z0-9])+$/i", $str)) ? false : true;
    }
    
    // --------------------------------------------------------------------
    
    /**
     * Validate Alpha-dash with underscores and dashes input
     *
     * @param   str: a string to be checked
     * @return  bool: a boolean value should be returned indicating it is OK or not
     */ 
    private function alphadash($str) {
        return ( ! preg_match("/^([a-z_-])+$/i", $str)) ? false : true;
    }
    
    // --------------------------------------------------------------------
    
    /**
     * Validate Alpha-dash-space with underscores and dashes input
     *
     * @param   str: a string to be checked
     * @return  bool: a boolean value should be returned indicating it is OK or not
     */ 
    private function alphadashspace($str) {
        return ( ! preg_match("/^([\sa-z_-])+$/i", $str)) ? false : true;
    }
    
    // --------------------------------------------------------------------
    
    /**
     * Validate Alpha input
     *
     * @param   str: a string to be checked
     * @return  bool: a boolean value should be returned indicating it is OK or not
     */     
    private function alpha($str) {
        return ( ! preg_match("/^([a-z])+$/i", $str)) ? false : true;
    }
    
    // --------------------------------------------------------------------
    
    /**
     * Validate numeric with underscores and dashes input
     *
     * @param   str: a string to be checked
     * @return  bool: a boolean value should be returned indicating it is OK or not
     */ 
    private function numericdash($str) {
        return ( ! preg_match("/^([0-9_\s-])+$/i", $str)) ? false : true;
    }
    
    // --------------------------------------------------------------------
    
    /**
     * Detect Numeric format
     *
     * @param   str: a string to be checked
     * @return  bool: a boolean value should be returned indicating it is OK or not
     */ 
    private function numeric($str) {
        return (bool)preg_match( '/^[\-+]?[0-9]*\.?[0-9]+$/', $str);

    }

	// --------------------------------------------------------------------
	
    /**
     * Canadian Postal format
     *
     * @param   str: a string to be checked
     * @return  bool: a boolean value should be returned indicating it is OK or not
     */ 
    private function canadianpostal($str) {
    // Postal codes do not include the letters D, F, I, O, Q or U, and the first position also does not make use of the letters W or Z.
        return (bool)preg_match( '/^[ABCEGHJKLMNPRSTVXYabceghjklmnprstvxy]\d[A-Za-z][ -]?\d[A-Za-z]\d/', $str);

    }

	// --------------------------------------------------------------------
	
	/**
	 * Validate Email
	 *
	 * @param	str: a string to be checked
     * @return  bool: a boolean value should be returned indicating it is OK or not
	 */	
	private function validemail($str) {
		return ( ! preg_match("/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix", $str)) ? false : true;
	}

	// --------------------------------------------------------------------
	
	/**
	 * Validate Emails
	 *
	 * @param	str: a string to be checked
     * @return  bool: a boolean value should be returned indicating it is OK or not
	 */	
	private function validemails($str) {
		if (strpos($str, ',') === false) {
			return valid_email(trim($str));
		}
		
		foreach(explode(',', $str) as $email) {
			if (trim($email) != '' && valid_email(trim($email)) === false) {
				return false;
			}
		}
		
		return true;
	}


	// --------------------------------------------------------------------
	
    /**
     * Detect Phone format
     *
     * @param   str: a string to be checked
     * @return  bool: a boolean value should be returned indicating it is OK or not
     */ 
    private function phone($str) {
    /*
      These phone numbers are valid format
      123-4567
      123-456-7890
      (123) 456-7890
      0 123 456 7890
      1234567890    
    */
        if (preg_match_all( '/(\d)/', $str, $matchedtotal)) {
        // In addition to regular phone format, digits should occur from 7 to 13 times for phone number format
          $size = count($matchedtotal[0]);
          if ($size < 7 || $size > 13) {          
            return false;
          }
          
          return (bool)preg_match( '/^(\(?\+?[0-9]*\)?)?[0-9_\- \(\)]*$/', $str);
        } else {
        
          return false;
        }
    }

	// --------------------------------------------------------------------
	
    
    // 
    /**
     * Detect password format
     *
     * @param   str: a string to be checked
     * @return  bool: a boolean value should be returned indicating it is OK or not
     */ 
    private function password($str) {
    /*
        upper case letters
        lower case letters
        number
        special characters    
    */
        return (bool)preg_match( "/^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[\\!\"\#$%&'\(\)\*\+\,\-\.\/\:;<=>\?@\[\]^_`{|}~]).*$/", $str);

    }

	// --------------------------------------------------------------------
	
    


}

?>