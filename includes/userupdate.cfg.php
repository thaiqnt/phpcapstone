<?php
/**
 * User Update Configuration 
 *
 * @author      Thai Tran Ngoc Quoc
 * @copyright   Copyright (c) 2018 - All rights reserved
 */

// ------------------------------------------------------------------------

// Define all fields in the user update form
$user_update_id = 'id';
$user_update_loginname = 'loginname';
$user_update_password = 'password';
$user_update_repeatedpass = 'repeatedpass';
$user_update_firstname = 'firstname';
$user_update_lastname = 'lastname';
$user_update_street = 'street';
$user_update_city = 'city';
$user_update_postalcode = 'postalcode';
$user_update_province = 'province';
$user_update_country = 'country';
$user_update_phone = 'phone';
$user_update_email = 'email';
$user_update_deleted = 'deleted';

// There are some hidden fields
$user_update_createdat = 'createdat';
$user_update_updatedat = 'updatedat';

// Define maximum values of all fields that could be valid in database
$user_update_maxlength = 'MAX';
$user_update[$user_update_loginname][$user_update_maxlength] = 20;
$user_update[$user_update_password][$user_update_maxlength] = 60;
$user_update[$user_update_firstname][$user_update_maxlength] = 255;
$user_update[$user_update_lastname][$user_update_maxlength] = 255;
$user_update[$user_update_street][$user_update_maxlength] = 255;
$user_update[$user_update_city][$user_update_maxlength] = 50;
$user_update[$user_update_postalcode][$user_update_maxlength] = 50;
$user_update[$user_update_province][$user_update_maxlength] = 50;
$user_update[$user_update_country][$user_update_maxlength] = 50;
$user_update[$user_update_phone][$user_update_maxlength] = 50;
$user_update[$user_update_email][$user_update_maxlength] = 255;

// Define displayed names of all fields in a form
$user_update_label_name = 'LABEL';
$user_update[$user_update_loginname][$user_update_label_name] = 'Login Name';
$user_update[$user_update_password][$user_update_label_name] = 'New Password';
$user_update[$user_update_repeatedpass][$user_update_label_name] = 'Retyped Password';
$user_update[$user_update_firstname][$user_update_label_name] = 'First Name';
$user_update[$user_update_lastname][$user_update_label_name] = 'Last Name';
$user_update[$user_update_street][$user_update_label_name] = 'Street';
$user_update[$user_update_city][$user_update_label_name] = 'City';
$user_update[$user_update_postalcode][$user_update_label_name] = 'Postal Code';
$user_update[$user_update_province][$user_update_label_name] = 'Province / State';
$user_update[$user_update_country][$user_update_label_name] = 'Country Name';
$user_update[$user_update_phone][$user_update_label_name] = 'Phone Number';
$user_update[$user_update_email][$user_update_label_name] = 'Email Address';
$user_update[$user_update_deleted][$user_update_label_name] = 'Inactive Account';

$user_update[$user_update_createdat][$user_update_label_name] = 'Created at';
$user_update[$user_update_updatedat][$user_update_label_name] = 'Updated at';

// Get the inputed values that would be in sanitized format in order to prevent XSS attacks
$user_update_postedvalue = 'VALUE';

// id and create fields are never changed and permitted to modify
$user_update[$user_update_id][$user_update_postedvalue] = $userinfo[$user_update_id];
$user_update[$user_update_createdat][$user_update_postedvalue] = $userinfo[$user_update_createdat];
$user_update[$user_update_updatedat][$user_update_postedvalue] = $userinfo[$user_update_updatedat];

// Does user want to change something?
if (count($_POST)>0) {
  $user_update[$user_update_loginname][$user_update_postedvalue] = getSanitizedValue($user_update_loginname, FILTER_SANITIZE_STRING);

  $user_update[$user_update_firstname][$user_update_postedvalue] = getSanitizedValue($user_update_firstname, FILTER_SANITIZE_STRING);
  $user_update[$user_update_lastname][$user_update_postedvalue] = getSanitizedValue($user_update_lastname, FILTER_SANITIZE_STRING);
  $user_update[$user_update_street][$user_update_postedvalue] = getSanitizedValue($user_update_street, FILTER_SANITIZE_STRING);
  $user_update[$user_update_city][$user_update_postedvalue] = getSanitizedValue($user_update_city, FILTER_SANITIZE_STRING);
  $user_update[$user_update_postalcode][$user_update_postedvalue] = getSanitizedValue($user_update_postalcode, FILTER_SANITIZE_STRING);
  $user_update[$user_update_province][$user_update_postedvalue] = getSanitizedValue($user_update_province, FILTER_SANITIZE_STRING);
  $user_update[$user_update_country][$user_update_postedvalue] = getSanitizedValue($user_update_country, FILTER_SANITIZE_STRING);
  $user_update[$user_update_phone][$user_update_postedvalue] = getSanitizedValue($user_update_phone, FILTER_SANITIZE_STRING);
  $user_update[$user_update_email][$user_update_postedvalue] = getSanitizedValue($user_update_email, FILTER_SANITIZE_EMAIL);
  $user_update[$user_update_deleted][$user_update_postedvalue] = getSanitizedValue($user_update_deleted, FILTER_SANITIZE_NUMBER_INT);
} else {
  // Just show what we have right now in the session
  $user_update[$user_update_loginname][$user_update_postedvalue] = $userinfo[$user_update_loginname];
  $user_update[$user_update_firstname][$user_update_postedvalue] = $userinfo[$user_update_firstname];
  $user_update[$user_update_lastname][$user_update_postedvalue] = $userinfo[$user_update_lastname];
  $user_update[$user_update_street][$user_update_postedvalue] = $userinfo[$user_update_street];
  $user_update[$user_update_city][$user_update_postedvalue] = $userinfo[$user_update_city];
  $user_update[$user_update_postalcode][$user_update_postedvalue] = $userinfo[$user_update_postalcode];
  $user_update[$user_update_province][$user_update_postedvalue] = $userinfo[$user_update_province];
  $user_update[$user_update_country][$user_update_postedvalue] = $userinfo[$user_update_country];
  $user_update[$user_update_phone][$user_update_postedvalue] = $userinfo[$user_update_phone];
  $user_update[$user_update_email][$user_update_postedvalue] = $userinfo[$user_update_email];
  $user_update[$user_update_deleted][$user_update_postedvalue] = $userinfo[$user_update_deleted];
  
}

// Password fields are always retrieved from form
// Users have responsibilities to enter password for themselves
$user_update[$user_update_password][$user_update_postedvalue] = getSanitizedValue($user_update_password, FILTER_SANITIZE_STRING);
$user_update[$user_update_repeatedpass][$user_update_postedvalue] = getSanitizedValue($user_update_repeatedpass, FILTER_SANITIZE_STRING);


?>