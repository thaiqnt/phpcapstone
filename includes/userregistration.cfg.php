<?php
/**
 * User Registration Configuration 
 *
 * @author      Thai Tran Ngoc Quoc
 * @copyright   Copyright (c) 2018 - All rights reserved
 */

// ------------------------------------------------------------------------

// Define all fields in the user registration form
$user_reg_loginname = 'loginname';
$user_reg_password = 'password';
$user_reg_repeatedpass = 'repeatedpass';
$user_reg_firstname = 'firstname';
$user_reg_lastname = 'lastname';
$user_reg_street = 'street';
$user_reg_city = 'city';
$user_reg_postalcode = 'postalcode';
$user_reg_province = 'province';
$user_reg_country = 'country';
$user_reg_phone = 'phone';
$user_reg_email = 'email';
$user_reg_deleted = 'deleted';

// There are some hidden fields
$user_reg_createdat = 'createdat';
$user_reg_updatedat = 'updatedat';

// Define maximum values of all fields that could be valid in database
$user_reg_maxlength = 'MAX';
$user_reg[$user_reg_loginname][$user_reg_maxlength] = 20;
$user_reg[$user_reg_password][$user_reg_maxlength] = 60;
$user_reg[$user_reg_firstname][$user_reg_maxlength] = 255;
$user_reg[$user_reg_lastname][$user_reg_maxlength] = 255;
$user_reg[$user_reg_street][$user_reg_maxlength] = 255;
$user_reg[$user_reg_city][$user_reg_maxlength] = 50;
$user_reg[$user_reg_postalcode][$user_reg_maxlength] = 50;
$user_reg[$user_reg_province][$user_reg_maxlength] = 50;
$user_reg[$user_reg_country][$user_reg_maxlength] = 50;
$user_reg[$user_reg_phone][$user_reg_maxlength] = 50;
$user_reg[$user_reg_email][$user_reg_maxlength] = 255;

// Define displayed names of all fields in a form
$user_reg_label_name = 'LABEL';
$user_reg[$user_reg_loginname][$user_reg_label_name] = 'Login Name';
$user_reg[$user_reg_password][$user_reg_label_name] = 'New Password';
$user_reg[$user_reg_repeatedpass][$user_reg_label_name] = 'Retyped Password';
$user_reg[$user_reg_firstname][$user_reg_label_name] = 'First Name';
$user_reg[$user_reg_lastname][$user_reg_label_name] = 'Last Name';
$user_reg[$user_reg_street][$user_reg_label_name] = 'Street';
$user_reg[$user_reg_city][$user_reg_label_name] = 'City';
$user_reg[$user_reg_postalcode][$user_reg_label_name] = 'Postal Code';
$user_reg[$user_reg_province][$user_reg_label_name] = 'Province / State';
$user_reg[$user_reg_country][$user_reg_label_name] = 'Country Name';
$user_reg[$user_reg_phone][$user_reg_label_name] = 'Phone Number';
$user_reg[$user_reg_email][$user_reg_label_name] = 'Email Address';
$user_reg[$user_reg_deleted][$user_reg_label_name] = 'Inactive Account';

$user_reg[$user_reg_createdat][$user_reg_label_name] = 'Created at';
$user_reg[$user_reg_updatedat][$user_reg_label_name] = 'Updated at';

// Get the inputed values that would be in sanitized format in order to prevent XSS attacks
$user_reg_postedvalue = 'VALUE';
$user_reg[$user_reg_loginname][$user_reg_postedvalue] = getSanitizedValue($user_reg_loginname, FILTER_SANITIZE_STRING);

$user_reg[$user_reg_password][$user_reg_postedvalue] = getSanitizedValue($user_reg_password, FILTER_SANITIZE_STRING);
$user_reg[$user_reg_repeatedpass][$user_reg_postedvalue] = getSanitizedValue($user_reg_repeatedpass, FILTER_SANITIZE_URL);

$user_reg[$user_reg_firstname][$user_reg_postedvalue] = getSanitizedValue($user_reg_firstname, FILTER_SANITIZE_STRING);
$user_reg[$user_reg_lastname][$user_reg_postedvalue] = getSanitizedValue($user_reg_lastname, FILTER_SANITIZE_STRING);
$user_reg[$user_reg_street][$user_reg_postedvalue] = getSanitizedValue($user_reg_street, FILTER_SANITIZE_STRING);
$user_reg[$user_reg_city][$user_reg_postedvalue] = getSanitizedValue($user_reg_city, FILTER_SANITIZE_STRING);
$user_reg[$user_reg_postalcode][$user_reg_postedvalue] = getSanitizedValue($user_reg_postalcode, FILTER_SANITIZE_STRING);
$user_reg[$user_reg_province][$user_reg_postedvalue] = getSanitizedValue($user_reg_province, FILTER_SANITIZE_STRING);
$user_reg[$user_reg_country][$user_reg_postedvalue] = getSanitizedValue($user_reg_country, FILTER_SANITIZE_STRING);
$user_reg[$user_reg_phone][$user_reg_postedvalue] = getSanitizedValue($user_reg_phone, FILTER_SANITIZE_STRING);
$user_reg[$user_reg_email][$user_reg_postedvalue] = getSanitizedValue($user_reg_email, FILTER_SANITIZE_EMAIL);
$user_reg[$user_reg_deleted][$user_reg_postedvalue] = getSanitizedValue($user_reg_deleted, FILTER_SANITIZE_NUMBER_INT);

?>