<?php
/**
 * Helper Module
 *
 * @author		Thai Tran Ngoc Quoc
 * @copyright	Copyright (c) 2018 - All rights reserved
 */

    /**
     * getSanitizedValue
     *
     * This function is responsible for sanitizing field values
     *
     * @param   field : field name for sanitizing
     * @param   sanitizedflag: option for sanitizing
     * @return  string: the value after being sanitized
     */
    function getSanitizedValue($field, $sanitizedflag = FILTER_SANITIZE_STRING) {
        if (isset($_POST[$field])) {
            return filter_var($_POST[$field], $sanitizedflag);
        }
        return '';
    }

    // --------------------------------------------------------------------
    
    /**
     * getArrayForForm
     *
     * This function returns an array valid for a form
     *
     * @param   arr : an array to be valid for form
     * @return  array: an array after being valid
     */
    function getArrayForForm($arr) {
        $result = array();

        foreach ($arr as $key => $value) {
            if (is_array($value)) {
              $result[$key] = getArrayForForm($value);
            } else {
              $result[$key] = htmlspecialchars($value, ENT_QUOTES, 'UTF-8', false);            
            }
        }

        return $result;
    }

    // --------------------------------------------------------------------
    
    /**
     * getPostedBoolValue
     *
     * This function is responsible for converting posted boolean value to integer type of db
     *
     * @param   value : a value to be checked
     * @param   truevalue : true value returned after checking
     * @param   falsevalue : false value returned after checking
     * @return  string: the value after being checking
     */
    function getPostedBoolValue($value, $truevalue = 1, $falsevalue = 0) {
        return (($value === '') or ((int)$value === 0)) ? $falsevalue : $truevalue;
    }
    
    // --------------------------------------------------------------------

    /**
     * Validate Numeric input
     *
     * @param   str: a string to be checked
     * @return  bool: a boolean value should be returned indicating it is OK or not
     */ 
    function isnumeric($str) {
        return ( ! preg_match("/^([0-9])+$/", $str)) ? false : true;
    }

    // --------------------------------------------------------------------
    
    /**
     * dumpList
     *
     * This function is for internal debugger, not for regular use
     *
     * @param  objecttobeprinted: the object to be printed out
     * @return none
     */
    function dumpList($objecttobeprinted) {
        echo "<pre>";
        var_dump($objecttobeprinted);
        echo "</pre>";
    }

    // --------------------------------------------------------------------
    

    
?>