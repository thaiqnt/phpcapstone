<?php
/**
 * Tour Detail Configuration 
 *
 * @author      Thai Tran Ngoc Quoc
 * @copyright   Copyright (c) 2018 - All rights reserved
 */

// ------------------------------------------------------------------------

// Define all fields in the user update form
$tour_cmd = 'cmd';
$tour_id = 'id';
$tour_categoryid = 'cid';
$tour_title = 'title';
$tour_image = 'image';
$tour_price = 'price';
$tour_duration = 'duration';
$tour_keywords = 'keywords';
$tour_startdate = 'startdat';
$tour_maxpax = 'maxpax';
$tour_availpax = 'availpax';
$tour_desc = 'desc';

// There are some hidden fields
$tour_deleted = 'deleted';
$tour_createdat = 'createdat';
$tour_updatedat = 'updatedat';

// Define maximum values of all fields that could be valid in database
$tour_maxlength = 'MAX';
$tour[$tour_id][$tour_maxlength] = 20;
$tour[$tour_categoryid][$tour_maxlength] = 20;
$tour[$tour_title][$tour_maxlength] = 200;
$tour[$tour_image][$tour_maxlength] = 255;
$tour[$tour_price][$tour_maxlength] = 100;
$tour[$tour_duration][$tour_maxlength] = 200;
$tour[$tour_keywords][$tour_maxlength] = 255;
$tour[$tour_startdate][$tour_maxlength] = 50;
$tour[$tour_maxpax][$tour_maxlength] = 100;
$tour[$tour_availpax][$tour_maxlength] = 100;
$tour[$tour_desc][$tour_maxlength] = 1000;

// Define displayed names of all fields in a form
$tour_label_name = 'LABEL';
$tour[$tour_categoryid][$tour_label_name] = 'Category ID';
$tour[$tour_title][$tour_label_name] = 'Title';
$tour[$tour_image][$tour_label_name] = 'Image';
$tour[$tour_price][$tour_label_name] = 'Price';
$tour[$tour_duration][$tour_label_name] = 'Duration';
$tour[$tour_keywords][$tour_label_name] = 'Keywords';
$tour[$tour_startdate][$tour_label_name] = 'Start Date';
$tour[$tour_maxpax][$tour_label_name] = 'Max PAX';
$tour[$tour_availpax][$tour_label_name] = 'Avail PAX';
$tour[$tour_desc][$tour_label_name] = 'Sumary';

$tour[$tour_createdat][$tour_label_name] = 'Created at';
$tour[$tour_updatedat][$tour_label_name] = 'Updated at';

// Get the inputed values that would be in sanitized format in order to prevent XSS attacks
$tour_postedvalue = 'VALUE';

$tour[$tour_cmd][$tour_postedvalue] = getSanitizedValue($tour_cmd, FILTER_SANITIZE_STRING);
$tour[$tour_id][$tour_postedvalue] = getSanitizedValue($tour_id, FILTER_SANITIZE_NUMBER_INT);
$tour[$tour_categoryid][$tour_postedvalue] = getSanitizedValue($tour_categoryid, FILTER_SANITIZE_NUMBER_INT);
$tour[$tour_title][$tour_postedvalue] = getSanitizedValue($tour_title, FILTER_SANITIZE_STRING);
$tour[$tour_image][$tour_postedvalue] = getSanitizedValue($tour_image, FILTER_SANITIZE_STRING);
$tour[$tour_price][$tour_postedvalue] = getSanitizedValue($tour_price, FILTER_SANITIZE_NUMBER_INT);
$tour[$tour_duration][$tour_postedvalue] = getSanitizedValue($tour_duration, FILTER_SANITIZE_STRING);
$tour[$tour_keywords][$tour_postedvalue] = getSanitizedValue($tour_keywords, FILTER_SANITIZE_STRING);
$tour[$tour_startdate][$tour_postedvalue] = getSanitizedValue($tour_startdate, FILTER_SANITIZE_STRING);
$tour[$tour_maxpax][$tour_postedvalue] = getSanitizedValue($tour_maxpax, FILTER_SANITIZE_NUMBER_INT);
$tour[$tour_availpax][$tour_postedvalue] = getSanitizedValue($tour_availpax, FILTER_SANITIZE_NUMBER_INT);
$tour[$tour_desc][$tour_postedvalue] = getSanitizedValue($tour_desc, FILTER_SANITIZE_STRING);
$tour[$tour_deleted][$tour_postedvalue] = getSanitizedValue($tour_deleted, FILTER_SANITIZE_NUMBER_INT);


?>