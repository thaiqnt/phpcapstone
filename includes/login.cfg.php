<?php
/**
 * User Registration Configuration 
 *
 * @author      Thai Tran Ngoc Quoc
 * @copyright   Copyright (c) 2018 - All rights reserved
 */

// ------------------------------------------------------------------------

// Define all fields in the user registration form
$user_login_loginname = 'loginname';
$user_login_password = 'password';
$user_login_firstname = 'firstname';
$user_login_lastname = 'lastname';
$user_login_street = 'street';
$user_login_city = 'city';
$user_login_postalcode = 'postalcode';
$user_login_province = 'province';
$user_login_country = 'country';
$user_login_phone = 'phone';
$user_login_email = 'email';
$user_login_deleted = 'deleted';

// There are some hidden fields
$user_login_createdat = 'createdat';
$user_login_updatedat = 'updatedat';

// Define maximum values of all fields that could be valid in database
$user_login_maxlength = 'MAX';
$user_login[$user_login_loginname][$user_login_maxlength] = 20;
$user_login[$user_login_password][$user_login_maxlength] = 60;

// Define displayed names of all fields in a form
$user_login_label_name = 'LABEL';
$user_login[$user_login_loginname][$user_login_label_name] = 'Login Name';
$user_login[$user_login_password][$user_login_label_name] = 'Password';
$user_login[$user_login_firstname][$user_login_label_name] = 'First Name';
$user_login[$user_login_lastname][$user_login_label_name] = 'Last Name';
$user_login[$user_login_street][$user_login_label_name] = 'Street';
$user_login[$user_login_city][$user_login_label_name] = 'City';
$user_login[$user_login_postalcode][$user_login_label_name] = 'Postal Code';
$user_login[$user_login_province][$user_login_label_name] = 'Province / State';
$user_login[$user_login_country][$user_login_label_name] = 'Country Name';
$user_login[$user_login_phone][$user_login_label_name] = 'Phone Number';
$user_login[$user_login_email][$user_login_label_name] = 'Email Address';
$user_login[$user_login_deleted][$user_login_label_name] = 'Inactive Account';

$user_login[$user_login_createdat][$user_login_label_name] = 'Created at';
$user_login[$user_login_updatedat][$user_login_label_name] = 'Updated at';

// Get the inputed values that would be in sanitized format in order to prevent XSS attacks
$user_login_postedvalue = 'VALUE';
$user_login[$user_login_loginname][$user_login_postedvalue] = getSanitizedValue($user_login_loginname, FILTER_SANITIZE_STRING);

$user_login[$user_login_password][$user_login_postedvalue] = getSanitizedValue($user_login_password, FILTER_SANITIZE_STRING);

?>