<?php
/**
 * User Registration Configuration 
 *
 * @author      Thai Tran Ngoc Quoc
 * @copyright   Copyright (c) 2018 - All rights reserved
 */

// ------------------------------------------------------------------------

// Define all fields in the user registration form
$user_profile_loginname = 'loginname';
$user_profile_firstname = 'firstname';
$user_profile_lastname = 'lastname';
$user_profile_street = 'street';
$user_profile_city = 'city';
$user_profile_postalcode = 'postalcode';
$user_profile_province = 'province';
$user_profile_country = 'country';
$user_profile_phone = 'phone';
$user_profile_email = 'email';
$user_profile_deleted = 'deleted';

// There are some hidden fields
$user_profile_createdat = 'createdat';
$user_profile_updatedat = 'updatedat';

// Define displayed names of all fields in a form
$user_profile_label_name = 'LABEL';
$user_profile[$user_profile_loginname][$user_profile_label_name] = 'Login Name';
$user_profile[$user_profile_firstname][$user_profile_label_name] = 'First Name';
$user_profile[$user_profile_lastname][$user_profile_label_name] = 'Last Name';
$user_profile[$user_profile_street][$user_profile_label_name] = 'Street';
$user_profile[$user_profile_city][$user_profile_label_name] = 'City';
$user_profile[$user_profile_postalcode][$user_profile_label_name] = 'Postal Code';
$user_profile[$user_profile_province][$user_profile_label_name] = 'Province / State';
$user_profile[$user_profile_country][$user_profile_label_name] = 'Country Name';
$user_profile[$user_profile_phone][$user_profile_label_name] = 'Phone Number';
$user_profile[$user_profile_email][$user_profile_label_name] = 'Email Address';
$user_profile[$user_profile_deleted][$user_profile_label_name] = 'Inactive Account';

$user_profile[$user_profile_createdat][$user_profile_label_name] = 'Created at';
$user_profile[$user_profile_updatedat][$user_profile_label_name] = 'Updated at';

?>