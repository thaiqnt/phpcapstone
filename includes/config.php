<?php
/**
 * Configuration module
 *
 * @author      Thai Tran Ngoc Quoc
 * @copyright   Copyright (c) 2018 - All rights reserved
 */

// ------------------------------------------------------------------------

ini_set('display_errors', true);
ini_set('error_reporting', E_ALL);

define('VIETNAMTOUR_CONFIG_PATH', '../includes/');

define('USERINFO', 'userinfo');
define('USERLOGGEDIN', 'userloggedin');
define('USERLOGGEDOUT', 'userloggedout');

// All database declarations should be here
$db['default']['dbdriver'] = "mysql";
$db['default']['hostname'] = "localhost";
$db['default']['username'] = "root";
$db['default']['password'] = "mysql";
$db['default']['database'] = "vietnamtour";
$db['default']['charset'] = "utf8";

// Just a trivial user account
$isAdminUser = false;

// Get current page in either GET OR POST as possible
$currentpage = (isset($_REQUEST['p'])) ? $_REQUEST['p'] : '';

// These links are not affected by rules of checking login
$directLinks = array('index.php', 'login.php', 'logout.php', 'register.php', 'aboutus.php');

// These links are available for accessing in this site
$validLinks = array('login.php', 'logout.php', 'register.php',
                   'aboutus.php', 'flight.php', 'profile.php',
                   'seacruise.php', 'treasure.php', 'userupdate.php',
                   'checkout.php', 'checkoutcomplete.php', 'invoice.php');

$adminLinks = array('tourlist.php', 'tourdetail.php', 'adminstatistics.php');

// Declaration for the tour menu
$notactive = 'NotActive';

$tourmenu['mobiletreasureactive'] = $notactive;
$tourmenu['mobileseacruiseactive'] = $notactive;
$tourmenu['mobileflightactive'] = $notactive;
$tourmenu['mobileaboutusactive'] = $notactive;
$tourmenu['mobileadminactive'] = $notactive;

$tourmenu['desktoptreasureactive'] = $notactive;
$tourmenu['desktopseacruiseactive'] = $notactive;
$tourmenu['desktopflightactive'] = $notactive;
$tourmenu['desktopaboutusactive'] = $notactive;
$tourmenu['desktopadminactive'] = $notactive;


$tourlink['usercart'] = '<li class="mainNavArea_mb__payArea__pay_item"><a href="?p=checkout.php#checkoutheader" title="Cart"><i class="fa fa-cart"></i><span>Cart</span><b class="checkoutitems">0</b></a></li>';
$tourlink['userinvoice'] = '<li class="mainNavArea_mb__payArea__pay_item"><a href="?p=invoice.php#invoicesection" title="Customer Invoices"><i class="fa fa-creditcard"></i><span>Invoice</span></a></li>';
$tourlink['userreg'] = '<li class="mainNavArea_mb__payArea__pay_item"><a href="?p=register.php" title="User Registration"><i class="fa fa-userreg"></i><span>User Register</span></a></li>';
$tourlink['usersignin'] = '<li class="mainNavArea_mb__payArea__pay_item"><a href="?p=login.php" title="User Login Section"><i class="fa fa-usersignin"></i><span>User Login</span></a></li>';
$tourlink['usersignout'] = '<li class="mainNavArea_mb__payArea__pay_item"><a href="?p=logout.php" title="User Logout Section"><i class="fa fa-usersignout"></i><span>User Logout</span></a></li>';
$tourlink['userprofile'] = '<li class="mainNavArea_mb__payArea__pay_item"><a href="?p=profile.php#profile2" title="User Profile Section"><i class="fa fa-userprofile"></i><span>User Profile</span></a></li>';

define('APP', __DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR);
define('CONTROL_CONFIG_PATH', APP . 'control' . DIRECTORY_SEPARATOR);
define('MODEL_CONFIG_PATH', APP . 'model' . DIRECTORY_SEPARATOR);
define('VIEW_CONFIG_PATH', APP . 'view' . DIRECTORY_SEPARATOR);

function autoload($className) {
    $className = ltrim($className, '\\');
    $fileName  = '';
    $namespace = '';
    if ($lastNsPos = strrpos($className, '\\')) {
        $namespace = substr($className, 0, $lastNsPos);
        $className = substr($className, $lastNsPos + 1);
        $fileName  = str_replace('\\', DIRECTORY_SEPARATOR, $namespace) . DIRECTORY_SEPARATOR;
    }
    $fileName .= str_replace('_', DIRECTORY_SEPARATOR, $className) . '.php';

    require APP . $fileName;
}

spl_autoload_register('autoload');

session_start();




?>